#include "LibreOffice.h"
#include "LibreOfficePrivate.h"

#include <com/sun/star/frame/Desktop.hpp>

#include "Building.h"
#include "BuildingAdaptation.h"
#include "Energy.h"
#include "Scenario.h"
#include "Simulation.h"


using namespace com::sun::star;

namespace SheetGeneral{
enum LibreOffice_General
{
    FIRST_ENERGY_LINE = 5,
    ENERGY_NAME = 3,
    ENERGY_PRICE,
    ENERGY_VAT,
    ENERGY_CATEGORY,
    ENERGY_GROWTH = 8,
    ENERGY_CO2,
    ENERGY_RENEWABLE,
    ENERGY_NUCLEAR,
    ENERGY_FOSSIL,
    OTHER_GROWTH_COLUMN = 4,
    OTHER_VAT_COLUMN,
    MAINTENANCE_ROW = 28,
    PROVISION_ROW,
    DISCOUNTING_ROW,
    DAY_FIRST_COLUMN = 4,
    DAY_NAME = 45,
    DAY_FIRST_ROW,
    DAY_DHW = 70,
    WEEK_FIRST_COLUMN = 4,
    WEEK_NAME = 73,
    WEEK_FIRST_ROW,
    end
};
}  // namespace SheetGeneral

namespace SheetBuilding{
enum LibreOffice_Buildings
{
    ACTIVATED = 5,
    OWNER,
    OCCUPANT,
    SURFACE,
    VOLUME = 10,
    HEAT_LOSS_COEFF,
    SOUTH_WINDOW_SURFACE,
    INERTIA,
    ENERGY = 16,
    SYSTEM_EFFICIENCY = 21,
    INNER_EFFICIENCY,
    SYSTEM_ON,
    SYSTEM_OFF,
    ELECTRICITY = 25,
    DHW_ENERGY = 28,
    DHW_QUANTITY = 31,
    DHW_PROFILE,
    VAT_ON_PURCHASES = 35,
    RESULT_HEATING = 38,
    RESULT_DHW_WINTER,
    RESULT_DHW_SUMMER,
    RESULT_POWER = 43,
    PROFILE_FIRST_WEEK = 46,
    end
};
}  // namespace SheetBuilding

namespace SheetScenario{
enum LibreOffice_Scenario
{
    BUILDING_ENABLE = 7,
    BUILDING_USE_NETWORK = 10,
    BUILDING_HEATING_PERIOD,
    BUILDING_DHW_WINTER,
    BUILDING_DHW_SUMMER,
    BUILDING_ENERGY = 16,
    BUILDING_SYSTEM,
    BUILDING_SYSTEM_EFFICIENCY = 21,
    BUILDING_INNER_EFFICIENCY,
    BUILDING_INVESTMENT = 25,
    BUILDING_MAINTENANCE,
    BUILDING_PROVISION,
    NETWORK_SYSTEM_1 = 35,
    NETWORK_SYSTEM_2,
    NETWORK_SYSTEM_3,
    NETWORK_SYSTEM_NAME = 1,
    NETWORK_SYSTEM_MINPOWER,
    NETWORK_SYSTEM_MAXPOWER,
    NETWORK_SYSTEM_EFFICIENCY,
    NETWORK_SYSTEM_MAXENERGY,
    NETWORK_TEMPERATURE = 34,
    NETWORK_PERIOD_ROW,
    NETWORK_SUBSTATIONS_EFFICIENCY_ROW = 37,
    NETWORK_SUBSTATIONS_EFFICIENCY_COLUMN = 9,
    NETWORK_TEMP_FLOW = 9,
    NETWORK_TEMP_RETURN,
    NETWORK_PERIOD_FIRST = 9,
    NETWORK_PERIOD_LAST,
    NETWORK_FIRST_SECTION = 44,
    RESULT_FIRST_ENERGY = 61,
    PROJECT_OWNER_COLUMN = 7,
    PROJECT_OWNER_ROW = 59,
    ECONOMIC_SUMMARY_COLUMN = 11,
    ECONOMIC_INVESTMENT_TAXINCL = 62,
    ECONOMIC_SUBSIDY,
    ECONOMIC_VAT_COMPENSATION,
    ECONOMIC_VAT_ON_PURCHARSES = 66,
    ECONOMIC_MAINTENANCE_COST = 68,
    ECONOMIC_REPAIR_PROVISION,
    ECONOMIC_AMORTIZATION,
    ECONOMIC_ANNUAL_INTEREST,
    ECONOMIC_INITIAL_FUNDS = 73,
    ECONOMIC_LOAN_ANNUITY,
    end
};
}  // namespace SheetScenario

constexpr int NumberOfDayProfiles = 15;
constexpr int NumberOfWeekProfiles = 15;

void LibreOffice::loadLibreOffice()
{
    LibreOfficePrivate::loadLibreOffice();
}


void LibreOffice::closeLibreOffice()
{
    LibreOfficePrivate::closeLibreOffice();
}

void LibreOffice::cleanup()
{
    LibreOfficePrivate::closeSheet();
    LibreOfficePrivate::closeDocument();
    _current.component.reset();
}

void LibreOffice::setDocument(Document& doc)
{
    try
    {
        if (doc.component.has_value())
        {
            LibreOfficePrivate::setDocument(std::any_cast<uno::Reference<lang::XComponent>>(doc.component));
        }
        else
        {
            doc.component = LibreOfficePrivate::loadDocument(doc.url);
        }
        LibreOfficePrivate::setSheet("Signaux");  // Will throw an exception if the sheet does not exists
        auto spreadsheetVersion = LibreOfficePrivate::getValue(1, 3);
        LibreOfficePrivate::closeSheet();

        if (spreadsheetVersion < SodibeSpreadsheetRequiredVersion)
            throw Exception_BadSpreadsheetVersion {};

        _current = doc;
    }
    catch (uno::Exception& ex)
    {
        std::string err = "LibreOffice::setDocument() failed (UNO exception): ";
        err +=OUStringToOString(ex.Message, RTL_TEXTENCODING_ASCII_US).getStr();
        throw std::runtime_error(err);
    }
    catch (Exception_NoSuchSheet&)
    {
        throw std::runtime_error("Le document choisi n'est pas un tableur Sodibé valide.");
    }
    catch (Exception_BadSpreadsheetVersion&)
    {
        throw std::runtime_error("Le tableur choisi n'est pas compatible avec cette version de Sodibé.");
    }
    catch (...)
    {
        throw std::runtime_error("LibreOffice::setDocument(): unhandled exception");
    }
}

std::vector<Document> LibreOffice::documents()
{
    std::vector<Document> result;
    try
    {
        auto docs = LibreOfficePrivate::getDocumentsEnumeration();

        while(docs->hasMoreElements())
        {
            auto obj = docs->nextElement();

            auto x = obj.get<uno::Reference<frame::XModel>>();

            auto url = x->getURL();

            if (url.endsWith(".ods") || url.endsWith(".fods"))
            {
                Document doc;
                doc.url = url.toUtf8().getStr();
                doc.filename = getFilenameFromUrl(doc.url);
                doc.component = obj.get<uno::Reference<lang::XComponent>>();
                result.emplace_back(doc);
            }
        }

        if (_current.component.has_value())
        {
            if (std::find_if(result.begin(), result.end(), [&](Document& d){return d == _current;}) == result.end())
                throw Exception_DocumentClosed{};
        }
    }
    catch (uno::Exception& ex)
    {
        std::string err = "LibreOffice::listOpenFiles() failed (UNO exception): ";
        err += OUStringToOString(ex.Message, RTL_TEXTENCODING_ASCII_US).getStr();
        throw std::runtime_error(err);
    }

    return result;
}

void LibreOffice::startBuildingsListener(std::function<void(void)> callback)
{
    LibreOfficePrivate::addListener("Signaux", 1, 4, std::move(callback));
}

void LibreOffice::startProfilesListener(std::function<void(void)> callback)
{
    LibreOfficePrivate::addListener("Signaux", 1, 5, std::move(callback));
}

void LibreOffice::startScenarioListener(std::function<void(void)> callback)
{
    LibreOfficePrivate::addListener("Signaux", 1, 6, std::move(callback));
}

void LibreOffice::startGeneralListener(std::function<void(void)> callback)
{
    LibreOfficePrivate::addListener("Signaux", 1, 7, std::move(callback));
}

std::pair<int, int> LibreOffice::BuildingListener::columns()
{
    LibreOfficePrivate::setSheet("Signaux");
    auto begin = static_cast<int>(LibreOfficePrivate::getValue(3, 4));
    auto end   = static_cast<int>(LibreOfficePrivate::getValue(4, 4));
    LibreOfficePrivate::closeSheet();
    return std::make_pair(begin, end);
}

std::string LibreOffice::ScenarioListener::sheet()
{
    LibreOfficePrivate::setSheet("Signaux");
    auto sheet = LibreOfficePrivate::getFormula(3, 6);
    LibreOfficePrivate::closeSheet();
    return sheet;
}

void LibreOffice::loadSimulation(Simulation &sim)
{
}

std::vector<Building> LibreOffice::loadAllBuildings()
{
    std::vector<Building> buildings;

    for (int i = 0 ; i < 10 ; ++i)
    {
        Building b = loadBuilding(i+1);
        if (!b.disabled())
            buildings.push_back(b);
    }

    return buildings;
}

Building LibreOffice::loadBuilding(int column)
{
    using namespace SheetBuilding;

    Building b;

    LibreOfficePrivate::setSheet("Bâtiments");

    b.setColumn(column);
    b.setName  (LibreOfficePrivate::getFormula(column,  2));

    if (column < 1 || column > 10 ||
        LibreOfficePrivate::getFormula(column, ACTIVATED) == "-")
    {
        b.disable();
        LibreOfficePrivate::closeSheet();
        return b;
    }

    b.setOwner             (LibreOfficePrivate::getFormula(column, OWNER));
    b.setOccupant          (LibreOfficePrivate::getFormula(column, OCCUPANT));
    b.setSurface           (LibreOfficePrivate::getValue  (column, SURFACE));
    b.setVolume            (LibreOfficePrivate::getValue  (column, VOLUME));
    b.setHeatLossCoeff     (LibreOfficePrivate::getValue  (column, HEAT_LOSS_COEFF));
    b.setSouthWindowSurface(LibreOfficePrivate::getValue  (column, SOUTH_WINDOW_SURFACE));
    b.setInertia           (LibreOfficePrivate::getValue  (column, INERTIA));
    b.setEnergy            (LibreOfficePrivate::getFormula(column, ENERGY));
    b.setInnerEfficiency   (LibreOfficePrivate::getValue  (column, INNER_EFFICIENCY));
    b.setSystemEfficiency  (LibreOfficePrivate::getValue  (column, SYSTEM_EFFICIENCY));
    b.setSystemDateOn      (LibreOfficePrivate::getDate   (column, SYSTEM_ON));
    b.setSystemDateOff     (LibreOfficePrivate::getDate   (column, SYSTEM_OFF));
    b.setElectricity       (LibreOfficePrivate::getFormula(column, ELECTRICITY));
    b.setDHWVolume         (LibreOfficePrivate::getValue  (column, DHW_QUANTITY));
    b.setDHWProfile        (LibreOfficePrivate::getFormula(column, DHW_PROFILE));
    b.setVatOnPurchases    (LibreOfficePrivate::getValue(column, VAT_ON_PURCHASES) != 0);

    std::string dhwSystems = LibreOfficePrivate::getFormula(column, DHW_ENERGY);
    if (dhwSystems == "IdemChauffage")
        b.setDhwSystem(Building::HeatingSystem, Building::HeatingSystem);
    else if (dhwSystems == "Électricité l'été")
        b.setDhwSystem(Building::HeatingSystem, Building::Electricity);
    else if (dhwSystems == "Électricité")
        b.setDhwSystem(Building::Electricity, Building::Electricity);

    for (unsigned j = 0 ; j < 52 ; ++j)
    {
        b.setWeekProfile(j, LibreOfficePrivate::getFormula(column, PROFILE_FIRST_WEEK + j));
    }

    LibreOfficePrivate::closeSheet();

    return b;
}

std::vector<Energy> LibreOffice::loadEnergies()
{
    using namespace SheetGeneral;

    std::vector<Energy> energies;

    LibreOfficePrivate::setSheet("Données générales");

    for (int i = FIRST_ENERGY_LINE ; !LibreOfficePrivate::getFormula(ENERGY_NAME, i).empty() ; ++i)
    {
        energies.push_back({i-FIRST_ENERGY_LINE,
                            LibreOfficePrivate::getFormula(ENERGY_NAME, i),
                            Euro_per_MegawattHour(LibreOfficePrivate::getValue(ENERGY_PRICE, i)),
                            LibreOfficePrivate::getValue(ENERGY_VAT, i),
                            LibreOfficePrivate::getFormula(ENERGY_CATEGORY, i),
                            LibreOfficePrivate::getValue(ENERGY_GROWTH, i),
                            Kilogram_per_KilowattHour(LibreOfficePrivate::getValue(ENERGY_CO2, i) / 1000.0),
                            LibreOfficePrivate::getValue(ENERGY_FOSSIL, i),
                            LibreOfficePrivate::getValue(ENERGY_RENEWABLE, i),
                            LibreOfficePrivate::getValue(ENERGY_NUCLEAR, i)
                           });
    }

    Shared::maintenanceVat = LibreOfficePrivate::getValue(OTHER_VAT_COLUMN, MAINTENANCE_ROW);

    Shared::maintenanceGrowth = LibreOfficePrivate::getValue(OTHER_GROWTH_COLUMN, MAINTENANCE_ROW);
    Shared::provisionGrowth = LibreOfficePrivate::getValue(OTHER_GROWTH_COLUMN, PROVISION_ROW);

    Shared::discounting = LibreOfficePrivate::getValue(OTHER_GROWTH_COLUMN, DISCOUNTING_ROW);

    LibreOfficePrivate::closeSheet();

    return energies;
}

TemperatureProfiles LibreOffice::loadTemperatureProfiles()
{
    using namespace SheetGeneral;

    TemperatureProfiles result;

    LibreOfficePrivate::setSheet("Données générales");

    for (int i = 0 ; i < NumberOfDayProfiles ; ++i)
    {
        auto name = LibreOfficePrivate::getFormula(DAY_FIRST_COLUMN + i, DAY_NAME);
        std::array<double, 24> temperatures {};
        for (int j = 0 ; j < 24 ; ++j)
        {
            auto x = static_cast<decltype(temperatures)::size_type>(j);
            temperatures[x] = LibreOfficePrivate::getValue(DAY_FIRST_COLUMN + i, DAY_FIRST_ROW + j);
        }
        result.days.emplace(name, temperatures);

        auto water = LibreOfficePrivate::getFormula(DAY_FIRST_COLUMN + i, DAY_DHW);
        result.dhw.emplace(name, (water == "Oui"));
    }

    for (int i = 0 ; i < NumberOfWeekProfiles ; ++i)
    {
        std::string name = LibreOfficePrivate::getFormula(WEEK_FIRST_COLUMN + i, WEEK_NAME);
        std::array<std::string, 7> pr {};
        for (int j = 0 ; j < 7 ; ++j)
        {
            auto x = static_cast<decltype(pr)::size_type>(j);
            pr[x] = LibreOfficePrivate::getFormula(WEEK_FIRST_COLUMN + i, WEEK_FIRST_ROW + j);
        }
        result.weeks.emplace(name, pr);
    }

    LibreOfficePrivate::closeSheet();

    return result;
}

void LibreOffice::updateBuildingsConsumption(const Building& b)
{
    using namespace SheetBuilding;

    if (b.invalid()) return;

    LibreOfficePrivate::setSheet("Bâtiments");

    LibreOfficePrivate::setValue(b.column(), RESULT_HEATING,    std::round(b.currentHeatConsumption().get()));
    LibreOfficePrivate::setValue(b.column(), RESULT_DHW_WINTER, std::round(b.currentWinterDhwConsumption().get()));
    LibreOfficePrivate::setValue(b.column(), RESULT_DHW_SUMMER, std::round(b.currentSummerDhwConsumption().get()));
    LibreOfficePrivate::setValue(b.column(), RESULT_POWER,      std::round(b.estimatedPower().get()));

    LibreOfficePrivate::closeSheet();
}

void LibreOffice::clearBuildingsCalculations(std::vector<int> exceptions)
{
    LibreOfficePrivate::setSheet("Bâtiments");

    for (int i = 1 ; i < 11 ; ++i) if (std::find(exceptions.begin(), exceptions.end(), i) == exceptions.end())
    {
        LibreOfficePrivate::clearCell(i, 35);
        LibreOfficePrivate::clearCell(i, 36);
        LibreOfficePrivate::clearCell(i, 37);
        LibreOfficePrivate::clearCell(i, 40);
    }

    LibreOfficePrivate::closeSheet();
}

std::vector<std::string> LibreOffice::scenarios()
{
    auto scenarios = LibreOfficePrivate::sheets();

    scenarios.erase(std::remove_if(scenarios.begin(), scenarios.end(), [](const std::string& sc){
        LibreOfficePrivate::setSheet(sc);

        bool result = (LibreOfficePrivate::getFormula(0, 4) != "Nom du scénario") ||
                      (LibreOfficePrivate::getFormula(5, 4) != "Oui");

        LibreOfficePrivate::closeSheet();

        return result;
    }), scenarios.end());

    return scenarios;
}

void LibreOffice::loadScenario(Scenario& sc)
{
    using namespace SheetScenario;

    LibreOfficePrivate::setSheet(sc.sheet());

    sc.setName(LibreOfficePrivate::getFormula(1, 4));

    sc.enable(LibreOfficePrivate::getFormula(5, 4) == "Oui");

    for (int i = 1 ; i < 11 ; ++i)
    {
        BuildingAdaptation ch;
        ch.enabled = (LibreOfficePrivate::getFormula(i, BUILDING_ENABLE) == "Oui");

        ch.setHeatingSystem  (LibreOfficePrivate::getFormula(i, BUILDING_USE_NETWORK));
        ch.setWinterDhwSystem(LibreOfficePrivate::getFormula(i, BUILDING_DHW_WINTER));
        ch.setSummerDhwSystem(LibreOfficePrivate::getFormula(i, BUILDING_DHW_SUMMER));

        ch.setNewSystem(LibreOfficePrivate::getFormula(i, BUILDING_ENERGY),
                        LibreOfficePrivate::getValue(i, BUILDING_INNER_EFFICIENCY),
                        LibreOfficePrivate::getValue(i, BUILDING_SYSTEM_EFFICIENCY));

        ch.useNetworkHeatingPeriod = (LibreOfficePrivate::getFormula(i, BUILDING_HEATING_PERIOD) == "Selon réseau");

        ch.investment  = Euro(LibreOfficePrivate::getValue(i, BUILDING_INVESTMENT));
        ch.maintenance = Euro(LibreOfficePrivate::getValue(i, BUILDING_MAINTENANCE));
        ch.provision   = Euro(LibreOfficePrivate::getValue(i, BUILDING_PROVISION));

        sc.adaptBuilding(i, ch);
    }

    sc.setFirstSystem (LibreOfficePrivate::getFormula(NETWORK_SYSTEM_NAME,       NETWORK_SYSTEM_1),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_MAXPOWER,   NETWORK_SYSTEM_1),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_MINPOWER,   NETWORK_SYSTEM_1),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_EFFICIENCY, NETWORK_SYSTEM_1),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_MAXENERGY,  NETWORK_SYSTEM_1));

    sc.setSecondSystem(LibreOfficePrivate::getFormula(NETWORK_SYSTEM_NAME,       NETWORK_SYSTEM_2),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_MAXPOWER,   NETWORK_SYSTEM_2),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_MINPOWER,   NETWORK_SYSTEM_2),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_EFFICIENCY, NETWORK_SYSTEM_2),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_MAXENERGY,  NETWORK_SYSTEM_2));

    sc.setThirdSystem (LibreOfficePrivate::getFormula(NETWORK_SYSTEM_NAME,       NETWORK_SYSTEM_3),
                       LibreOfficePrivate::getValue  (NETWORK_SYSTEM_EFFICIENCY, NETWORK_SYSTEM_3));

    sc.clearNetwork();

    for (int i = 0 ; i < 10 ; ++i)
    {
        sc.addNetworkSection(LibreOfficePrivate::getValue(1, NETWORK_FIRST_SECTION+i),
                             LibreOfficePrivate::getValue(4, NETWORK_FIRST_SECTION+i));
    }

    sc.setNetworkTemperature(LibreOfficePrivate::getValue(NETWORK_TEMP_FLOW, NETWORK_TEMPERATURE),
                             LibreOfficePrivate::getValue(NETWORK_TEMP_RETURN, NETWORK_TEMPERATURE));
    sc.setNetworkPeriodOfUse(LibreOfficePrivate::getDate(NETWORK_PERIOD_FIRST, NETWORK_PERIOD_ROW),
                             LibreOfficePrivate::getDate(NETWORK_PERIOD_LAST,  NETWORK_PERIOD_ROW));
    sc.setSubstationsEfficiency(LibreOfficePrivate::getValue(NETWORK_SUBSTATIONS_EFFICIENCY_COLUMN,
                                                             NETWORK_SUBSTATIONS_EFFICIENCY_ROW));

    sc.setProjectOwner(LibreOfficePrivate::getFormula(PROJECT_OWNER_COLUMN, PROJECT_OWNER_ROW));

    sc.setInvestmentTaxIncl(LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_INVESTMENT_TAXINCL));
    sc.setSubsidy          (LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_SUBSIDY));
    sc.setVatCompensation  (LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_VAT_COMPENSATION));

    sc.setMaintenanceCostTaxIncl(LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_MAINTENANCE_COST));
    sc.setRepairProvisionTaxIncl(LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_REPAIR_PROVISION));

    sc.setAmortization  (LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_AMORTIZATION));
    sc.setAnnualInterest(LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_ANNUAL_INTEREST));

    sc.setInitialFunds(LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_INITIAL_FUNDS));
    sc.setLoanAnnuity (LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_LOAN_ANNUITY));

    sc.setVatOnPurchases(LibreOfficePrivate::getValue(ECONOMIC_SUMMARY_COLUMN, ECONOMIC_VAT_ON_PURCHARSES) != 0);

    LibreOfficePrivate::closeSheet();
}

void LibreOffice::updateScenarioCalculations(const Scenario& scenario)
{
    using namespace SheetScenario;

    auto consumptions = scenario.globalConsumptions();

    LibreOfficePrivate::setSheet(scenario.sheet());

    std::vector<int> rowsCompleted;

    for (auto& c : consumptions)
    {
        int row = Shared::energy(c.first).row;
        LibreOfficePrivate::setValue(1, RESULT_FIRST_ENERGY + row, c.second.get());
        rowsCompleted.push_back(row);
    }

    for (int i = 0 ; i < 20 ; ++i)
    {
        if (std::find(rowsCompleted.begin(), rowsCompleted.end(), i) == rowsCompleted.end())
            LibreOfficePrivate::setValue(1, RESULT_FIRST_ENERGY + i, 0);
    }

    LibreOfficePrivate::closeSheet();
}

std::string LibreOffice::getFilenameFromUrl(std::string url)
{
    url = url.substr(url.find_last_of('/')+1);

    std::string ret;

    for (unsigned i = 0 ; i < url.length() ; i++)
    {
        if (url[i] != '%')
        {
            ret += url[i];
        }
        else
        {
            unsigned long ii = std::strtoul(url.substr(i + 1, 2).c_str(), nullptr, 16);
            char ch = static_cast<char>(ii);
            ret += ch;
            i = i + 2;
        }
    }

    return ret;
}
