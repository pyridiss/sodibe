#include "Scenario.h"

#include <random>

#include "Building.h"

Scenario::Scenario(std::string sheet, std::vector<Building>* buildings)
  : _sheet     {std::move(sheet)},
    _buildings {buildings}
{
}

void Scenario::enable(bool b)
{
    _enabled = b;
}

void Scenario::setName(const std::string& name)
{
    _name = name;
}

std::string  Scenario::name() const
{
    return _name;
}

std::string  Scenario::sheet() const
{
    return _sheet;
}

void Scenario::setFirstSystem(const std::string &name, double nominal, double min, double efficiency, double maxEnergy)
{
    _systems[0] = {name, nominal, min, efficiency, maxEnergy};
}

void Scenario::setSecondSystem(const std::string &name, double nominal, double min, double efficiency, double maxEnergy)
{
    _systems[1] = {name, nominal, min, efficiency, maxEnergy};
}

void Scenario::setThirdSystem(const std::string &name, double efficiency)
{
    _systems[2] = {name, 500000, 0, efficiency, 0};
}

void Scenario::adaptBuilding(int column, const BuildingAdaptation& c)
{
    auto b = std::find_if(_buildings->begin(), _buildings->end(), [&](const Building& bb) {
                          return bb.column() == column;});

    if (b != _buildings->end())
    {
        auto a = std::find_if(_buildingsAdaptations.begin(), _buildingsAdaptations.end(), [&](const BuildingAdaptation& aa) {
            return aa.building() == &*b;});
        if (a == _buildingsAdaptations.end())
        {
            _buildingsAdaptations.push_back(c);
            _buildingsAdaptations.back().setScenario(this);
            _buildingsAdaptations.back().setBuilding(&*b);
        }
        else
        {
            *a = c;
            a->setScenario(this);
            a->setBuilding(&*b);
        }
    }
}

void Scenario::clearNetwork()
{
    _networkSections.clear();
}

void Scenario::addNetworkSection(double size, double coeff)
{
    _networkSections.emplace_back(Scenario::NetWorkSection{Meter(size), Watt_per_KelvinMeter(coeff)});
}

void Scenario::setNetworkTemperature(double flowTemp, double returnTemp)
{
    _networkTemperature = Kelvin((flowTemp + returnTemp) / 2);
}

void Scenario::setSubstationsEfficiency(double eff)
{
    _substationsEfficiency = eff;
}

void Scenario::setNetworkPeriodOfUse(std::time_t start, std::time_t stop)
{
    _startNetwork = start;
    _stopNetwork  = stop;
}

void Scenario::setProjectOwner(const std::string& value)
{
    _projectOwner = value;
}

void Scenario::setInvestmentTaxIncl(double value)
{
    _investment = Euro(value);
}

void Scenario::setSubsidy(double value)
{
    _subsidy = Euro(value);
}

void Scenario::setInitialFunds(double value)
{
    _initialFunds = Euro(value);
}

void Scenario::setLoanAnnuity(double value)
{
    _loanAnnuity = Euro(value);
}

void Scenario::setVatCompensation(double value)
{
    _vatCompensation = Euro(value);
}

void Scenario::setMaintenanceCostTaxIncl(double value)
{
    _maintenance = Euro(value);
}

void Scenario::setRepairProvisionTaxIncl(double value)
{
    _provision = Euro(value);
}

void Scenario::setAmortization(double value)
{
    _amortization = Euro(value);
}

void Scenario::setAnnualInterest(double value)
{
    _interest = Euro(value);
}

void Scenario::setVatOnPurchases(bool value)
{
    _vatOnPurchases = value;
}

Sim_TableOfHours<KilowattHour> Scenario::_heatLossOfPipes() const
{
    Sim_TableOfHours<KilowattHour> result;

    for (unsigned hour = 0 ; hour < result.size() ; ++hour)
    {
        if (!Shared::dateBetween(hour, networkHeatingPeriod()))
        {
            result[hour] = KilowattHour(0);
            continue;
        }

        Kelvin soilTemperature = Shared::soilTemperature(hour);

        for (auto& s : _networkSections)
            result[hour] += Kilowatt(s.linearHeatTransfertCoeff * (_networkTemperature - soilTemperature) * s.size) * Hour(1);
    }
    return result;
}

void Scenario::updateConsumptions()
{
    const std::lock_guard lock(_mutex);

    _totalHeatFromNetwork.fill(KilowattHour{0});

    for (auto& a : _buildingsAdaptations)
    {
        a.updateConsumption();
        _totalHeatFromNetwork = addTable(_totalHeatFromNetwork, a.heatFromNetwork);
    }

    for (auto& i : _totalHeatFromNetwork)
        i = i / (_substationsEfficiency ? _substationsEfficiency : 1);

    _totalHeatFromNetwork = addTable(_totalHeatFromNetwork, _heatLossOfPipes());

    for (auto& a : _buildingsAdaptations)
        a.updateBill();
}

auto Scenario::_fixedCharges(unsigned year) const -> std::unordered_map<std::string, Euro>
{
    std::unordered_map<std::string, Euro> result;

    result.emplace("Provisions pour réparations", _provision * std::pow(1 + Shared::provisionGrowth, year));
    result.emplace("Amortissement / Capital emprunté", _amortization);
    result.emplace("Intérêts d'emprunt", _interest);

    return result;
}

auto Scenario::_variableCharges(unsigned year) const -> std::unordered_map<std::string, Euro>
{
    auto energies = _loadDurationCurve();
    auto energy1  = Shared::energy(_systems[0].energy);
    auto energy2  = Shared::energy(_systems[1].energy);
    auto energy3  = Shared::energy(_systems[2].energy);

    KilowattHour qty1, qty2, qty3;

    for (auto& e : energies)
    {
        qty1 += KilowattHour(e[0]);
        qty2 += KilowattHour(e[1]);
        qty3 += KilowattHour(e[2]);
    }

    qty1 = qty1 / Shared::years / _systems[0].efficiency;
    qty2 = qty2 / Shared::years / _systems[1].efficiency;
    qty3 = qty3 / Shared::years / _systems[2].efficiency;

    auto charges1 = Euro(energy1.price * qty1 * (1 + (_vatOnPurchases ? energy1.vat : 0)));
    auto charges2 = Euro(energy2.price * qty2 * (1 + (_vatOnPurchases ? energy2.vat : 0)));
    auto charges3 = Euro(energy3.price * qty3 * (1 + (_vatOnPurchases ? energy3.vat : 0)));

    charges1 = charges1 * pow(1 + energy1.priceGrowth, year);
    charges2 = charges2 * pow(1 + energy2.priceGrowth, year);
    charges3 = charges3 * pow(1 + energy3.priceGrowth, year);

    // TODO: electricity

    auto maintenance = _maintenance * std::pow(1 + Shared::maintenanceGrowth, year)
                                    * (1 + (_vatOnPurchases ? Shared::maintenanceVat : 0));

    std::unordered_map<std::string, Euro> result;

    result.emplace(energy1.name, charges1);
    result.emplace(energy2.name, charges2);
    result.emplace(energy3.name, charges3);
    result.emplace("Entretien", maintenance);

    return result;
}

Euro_per_MegawattHour Scenario::energyCost(unsigned year) const
{
    MegawattHour energy;

    for (auto& a : _buildingsAdaptations)
        energy += a.consumptions()["Réseau"];

    Euro sum = sumMap(_variableCharges(year));

    if (energy < MegawattHour(1)) return Euro_per_MegawattHour(0);

    return Euro_per_MegawattHour(sum / energy);
}

auto Scenario::detailedEnergyCost(unsigned year) const -> std::unordered_map<std::string, Euro_per_MegawattHour>
{
    std::unordered_map<std::string, Euro_per_MegawattHour> result;

    MegawattHour energy;

    for (auto& a : _buildingsAdaptations)
        energy += a.consumptions()["Réseau"];

    if (energy < MegawattHour(1)) return result;

    for (auto& i : _variableCharges(year))
        result.emplace(i.first, i.second / energy);

    return result;
}

Euro_per_Kilowatt Scenario::subscriptionCost(unsigned year) const
{
    Kilowatt totalSubscriptions;

    for (auto& a : _buildingsAdaptations)
        totalSubscriptions += a.networkSubscription();

    if (totalSubscriptions < Kilowatt(1)) totalSubscriptions = Kilowatt(1);

    Euro sum = sumMap(_fixedCharges(year));

    return Euro_per_Kilowatt(sum / totalSubscriptions);
}

auto Scenario::detailedSubscriptionCost(unsigned year) const -> std::unordered_map<std::string, Euro_per_Kilowatt>
{
    std::unordered_map<std::string, Euro_per_Kilowatt> result;

    Kilowatt totalSubscriptions;

    for (auto& a : _buildingsAdaptations)
        totalSubscriptions += a.networkSubscription();

    if (totalSubscriptions < Kilowatt(1)) return result;

    for (auto& i : _fixedCharges(year))
        result.emplace(i.first, i.second / totalSubscriptions);

    return result;
}

auto Scenario::detailedChargesByBuilding(unsigned year) const -> std::map<Building*, std::unordered_map<std::string, Euro>>
{
    // Using a std::map with key type 'Building*' ensures buildings are sorted as in the spreadsheet
    // (pointers to the elements of a std::vector)
    std::map<Building*, std::unordered_map<std::string, Euro>> result;

    for (auto a : _buildingsAdaptations) if (a.enabled)
    {
        auto& charges = result[a.building()];

        charges = a.occupantBills[year];
        if (a.building()->owner() == a.building()->occupant())
            charges.merge(a.ownerBills[year]);
    }

    return result;
}

auto Scenario::globalCostByBuilding(unsigned year) const -> std::map<Building*, Euro_per_MegawattHour>
{
    std::map<Building*, Euro_per_MegawattHour> result;

    auto subs = detailedSubscriptionCost(year);

    for (auto a : _buildingsAdaptations) if (a.enabled)
    {
        if (a.building()->owner() == _projectOwner && a.building()->occupant() == _projectOwner)
        {
            auto cost = sumMap(a.occupantBills[year]) + sumMap(a.ownerBills[year]);
            cost += subs["Amortissement / Capital emprunté"] * a.networkSubscription();
            cost += subs["Intérêts d'emprunt"] * a.networkSubscription();

            cost += a.investment / 20;  // TODO: VAT in investment
            result[a.building()] = cost / a.needs();
        }
    }

    return result;
}

auto Scenario::cumulativeCosts() const -> std::vector<Euro>
{
    std::vector<Euro> result;
    result.resize(21);

    result[0] = _initialFunds;
    for (auto& b : _buildingsAdaptations)
        result[0] += b.investment;

    for (unsigned u = 1 ; u < 21 ; ++u)
    {
        for (auto& b : _buildingsAdaptations)
        {
            result[u] += sumMap(b.occupantBills[u-1]);
            result[u] += sumMap(b.ownerBills[u-1]);
        }
        result[u] = result[u] / (std::pow(1 + Shared::discounting, u));

        result[u] += result[u-1];
    }

    return result;
}

Sim_TableOfHours<std::array<double, 3>> Scenario::_loadDurationCurve() const
{
    using Energies = std::array<double, 3>;
    Sim_TableOfHours<Energies> energies {};

    for (unsigned i = 0 ; i < energies.size() ; ++i)
    {
        auto h = _totalHeatFromNetwork[i].get();
        if (h < _systems[0].minimumPower)
        {
            if (h < _systems[1].minimumPower)
            {
                energies[i][2] = h;
            }
            else
            {
                if (h <= _systems[1].nominalPower)
                {
                    energies[i][1] = h;
                }
                else
                {
                    energies[i][1] = _systems[1].nominalPower;
                    energies[i][2] = h - _systems[1].nominalPower;
                }
            }
        }
        else if (h <= _systems[0].nominalPower)
        {
            energies[i][0] = h;
        }
        else
        {
            energies[i][0] = _systems[0].nominalPower;
            h -= _systems[0].nominalPower;

            if (h < _systems[1].minimumPower)
            {
                energies[i][2] = h;
            }
            else if (h <= _systems[1].nominalPower)
            {
                energies[i][1] = h;
            }
            else
            {
                energies[i][1] = _systems[1].nominalPower;
                energies[i][2] = h - _systems[1].nominalPower;
            }
        }
    }

    if (_systems[0].maxEnergy || _systems[1].maxEnergy)
    {
        KilowattHour qty1, qty2, qty3;

        for (auto& e : energies)
        {
            qty1 += KilowattHour(e[0]);
            qty2 += KilowattHour(e[1]);
            qty3 += KilowattHour(e[2]);
        }

        qty1 = qty1 / Shared::years / (_systems[0].efficiency ? _systems[0].efficiency : 1);
        qty2 = qty2 / Shared::years / (_systems[1].efficiency ? _systems[1].efficiency : 1);
        qty3 = qty3 / Shared::years / (_systems[2].efficiency ? _systems[2].efficiency : 1);

        std::mt19937 rng;
        std::uniform_int_distribution<> dist(0, energies.size());

        if (_systems[0].maxEnergy > 1 && qty1 > KilowattHour(_systems[0].maxEnergy*1.01))
        {
            auto diff = qty1 - KilowattHour(_systems[0].maxEnergy);
            while (diff > KilowattHour(0))
            {
                int i = dist(rng);
                if (energies[i][1] + energies[i][0] < _systems[1].nominalPower)
                {
                    energies[i][1] += energies[i][0];
                    qty2 += KilowattHour(energies[i][0]) / Shared::years / _systems[1].efficiency;
                }
                else
                {
                    auto added1 = _systems[1].nominalPower - energies[i][1];
                    auto added2 = energies[i][0] - added1;
                    energies[i][2] += added2;
                    qty3 += KilowattHour(added2) / Shared::years / _systems[2].efficiency;
                    energies[i][1] = _systems[1].nominalPower;
                    qty2 += KilowattHour(added1) / Shared::years / _systems[1].efficiency;
                }
                diff += KilowattHour(-energies[i][0] / Shared::years / _systems[0].efficiency);
                energies[i][0] = 0;
            }
        }
        if (_systems[1].maxEnergy > 1 && qty2 > KilowattHour(_systems[1].maxEnergy*1.01))
        {
            auto diff = qty2 - KilowattHour(_systems[1].maxEnergy);
            while (diff > KilowattHour(0))
            {
                int i = dist(rng);
                energies[i][2] += energies[i][1];
                qty3 += KilowattHour(energies[i][1]) / Shared::years / _systems[2].efficiency;

                diff += KilowattHour(-energies[i][1] / Shared::years / _systems[1].efficiency);
                energies[i][1] = 0;
            }
        }
    }
    std::sort(energies.begin(), energies.end(),
              [](Energies a, Energies b) { return a[0] + a[1] + a[2] > b[0] + b[1] + b[2]; });

    return energies;
}

auto Scenario::consumptionPerSourcePerBuilding() const -> ConsumptionPerSourcePerBuilding
{
    const std::lock_guard lock(_mutex);

    ConsumptionPerSourcePerBuilding result {};

    for (auto& ch : _buildingsAdaptations)
    {
        if (!ch.enabled) continue;

        auto energyFromNetwork = yearlyMeanSum(ch.heatFromNetwork).get();
        auto localEnergy = yearlyMeanSum(ch.localConsumptionForHeating).get()
                         + yearlyMeanSum(ch.localConsumptionForDhw).get();

        if (energyFromNetwork > 1)
            result.networkItems.emplace_back(ch.building()->name(), energyFromNetwork);
        if (localEnergy > 1)
            result.localItems.emplace_back(ch.building()->name(), localEnergy);
    }

    auto networkCon = _heatLossOfPipes();
    double networkSum = yearlyMeanSum(networkCon).get();

    result.losses.emplace_back("Pertes réseau", networkSum);


    auto ldc = _loadDurationCurve();

    double losses {};

    for (auto& h : ldc) for (int i = 0 ; i < 3 ; ++i)
    {
        if (_systems[i].efficiency < 1)
            losses += h[i] * (1 - _systems[i].efficiency) / _systems[i].efficiency;
    }

    losses /= Shared::years;

    result.losses.emplace_back("Pertes de production", losses);

    return result;
}

Sim_TableOfHours<std::array<double, 3>> Scenario::loadDurationCurve() const
{
    const std::lock_guard lock(_mutex);

    return _loadDurationCurve();
}

Sim_TableOfDays<std::array<double, 3>> Scenario::dailyMaximums() const
{
    const std::lock_guard lock(_mutex);

    Sim_TableOfDays<std::array<double, 3>> values {};

    for (unsigned i = 0 ; i < 3*365 ; ++i)
    {
        double min {100000}, max {0}, moy {0};
        for (unsigned j = 0 ; j < 24 ; ++j)
        {
            min = std::min(min, _totalHeatFromNetwork[24*i+j].get());
            max = std::max(max, _totalHeatFromNetwork[24*i+j].get());
            moy += _totalHeatFromNetwork[24*i+j].get();
        }
        moy /= 24;

        values[i][0] = min;
        values[i][1] = moy;
        values[i][2] = max;
    }

    return values;
}

auto Scenario::globalConsumptions() const -> std::unordered_map<std::string, KilowattHour>
{
    const std::lock_guard lock(_mutex);

    std::unordered_map<std::string, KilowattHour> result;

    auto energies = _loadDurationCurve();

    std::array<KilowattHour, 3> qty;

    for (auto& e : energies) for (int i = 0 ; i < 3 ; ++i)
    {
        qty[i] += KilowattHour(e[i]);
    }

    for (int i = 0 ; i < 3 ; ++i)
    {
        qty[i] = qty[i] / Shared::years / (_systems[i].efficiency ? _systems[i].efficiency : 1);
        result[_systems[i].energy] += qty[i];

        if (_systems[i].efficiency > 1.5)  // Assuming Heat Pump
            result["gaia"] += qty[i] * (_systems[i].efficiency-1);
    }

    for (auto& a : _buildingsAdaptations)
    {
        for (auto& b : a.consumptions()) if (b.first != "Réseau")
            result[b.first] += b.second;
    }

    return result;
}

auto Scenario::localInvestments() const -> std::unordered_map<Building*, Euro>
{
    std::unordered_map<Building*, Euro> result;

    for (auto a : _buildingsAdaptations)
    {
        result[a.building()] = a.investment;
    }

    return result;
}

auto Scenario::co2Emissions() const -> Ton_per_Year
{
    auto cons = globalConsumptions();
    Ton_per_Year emissions;

    for (auto& [energy, consumption] : cons)
        emissions += Ton_per_Year(Shared::energy(energy).co2Factor * consumption / Year(1));

    return emissions;
}

auto Scenario::carTravellingEquivalence() const -> Kilometer_per_Year
{
    auto co2 = co2Emissions();

    return Kilometer_per_Year(co2 / Shared::co2EmissionPerKilometer);
}

auto Scenario::nonRenewablesFraction() const -> double
{
    auto cons = globalConsumptions();

    KilowattHour total, nonRenewable;

    for (auto& [energy, consumption] : cons)
    {
        total += consumption;
        nonRenewable += consumption * (Shared::energy(energy).fossilFactor + Shared::energy(energy).nuclearFactor);
    }

    return (nonRenewable / total).get();
}

auto Scenario::nuclearWasteProduced() const -> Kilogram_per_Year
{
    auto cons = globalConsumptions();

    Kilogram_per_Year waste;

    for (auto& [energy, consumption] : cons)
    {
        waste += Kilogram_per_Year(consumption * Shared::energy(energy).nuclearFactor * Shared::nuclearWasteFactor / Year(1));
    }

    return waste;
}
