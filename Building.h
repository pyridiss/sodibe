#ifndef BUILDING_H
#define BUILDING_H

#include <ctime>

#include "Shared.h"
#include "units.h"


class Building
{
public:
    enum DhwSystem
    {
        HeatingSystem,
        Electricity
    };

    void setName(const std::string & value) {
        _name = value;
    }
    void disable() {
        _disabled = true;
    }
    void invalidate(const std::string& status) {
        _invalid = true;
        _status = status;
    }
    void setColumn(int c) {
        _column = c;
    }

    std::string name() const {
        return _name;
    }
    bool disabled() const {
        return _disabled;
    }
    bool invalid() const {
        return _invalid;
    }
    const std::string& status() const {
        return _status;
    }
    int column() const {
        return _column;
    }
    const auto& owner        () const {
        return _owner;
    }
    const auto& occupant     () const {
        return _occupant;
    }
    Meter2 surface() const {
        return _surface;
    }
    std::string heatingEnergy() const {
        return _energy;
    }
    const auto& energy() const {
        return _energy;
    }
    const auto& electricity() const {
        return _electricity;
    }
    auto systemEfficiency() const {
        return _systemEfficiency;
    }
    const std::string& profile(unsigned week) const {
        return _profiles[week];
    }
    std::pair<std::time_t, std::time_t> heatingPeriod() const {
        return std::make_pair(_systemDateOn, _systemDateOff);
    }
    auto vatOnPurchases() const {
        return _vatOnPurchases;
    }

    void setOwner              (const std::string& value);
    void setOccupant           (const std::string& value);
    void setSurface            (double value);
    void setVolume             (double value);
    void setHeatLossCoeff      (double value);
    void setSouthWindowSurface (double value);
    void setInertia            (double value);
    void setEnergy             (const std::string& value);
    void setInnerEfficiency    (double value);
    void setSystemEfficiency   (double value);
    void setSystemDateOn       (std::time_t value);
    void setSystemDateOff      (std::time_t value);
    void setElectricity        (const std::string& value);
    void setDhwSystem          (DhwSystem winter, DhwSystem summer);
    void setDHWVolume          (double value);
    void setDHWProfile         (const std::string &value);
    void setWeekProfile        (unsigned week, std::string profile);
    void setTargetedTemperature(unsigned hour, double value);
    void setUseOfDHW           (unsigned day, bool value);
    void setVatOnPurchases     (bool value);


    Sim_TableOfHours<KilowattHour> heatNeeds(Period period = Shared::wholeYear) const;
    Sim_TableOfHours<KilowattHour> dhwNeeds (Period period                  = Shared::wholeYear) const;

    KilowattHour currentHeatConsumption()      const;
    KilowattHour currentWinterDhwConsumption() const;
    KilowattHour currentSummerDhwConsumption() const;
    Kilowatt     estimatedPower()              const;

    bool compareSystems(const Building& other) const;

private:
    std::string                _name             {};
    bool                       _disabled         {};
    bool                       _invalid          {};
    std::string                _status           {};
    int                        _column           {};
    std::string                _owner            {};
    std::string                _occupant         {};
    Meter2                     _surface          {};
    Meter3                _volume        {};
    Watt_per_KelvinMeter3 _heatLossCoeff {};
    Meter2                     _southWindow      {};
    Coefficient           _inertia       {};
    std::string                _energy           {};
    double                     _innerEfficiency  {};
    double                     _systemEfficiency {};
    std::time_t                _systemDateOn     {};
    std::time_t                _systemDateOff    {};
    std::string                _electricity      {};
    DhwSystem                  _winterDhwSystem  {};
    DhwSystem                  _summerDhwSystem  {};
    Liter_per_Day         _dhwQuantity   {};
    DHWProfile            _dhwProfile    {};
    std::array<std::string, 52>_profiles         {};
    TableOfHours<Kelvin>  _temperatures;
    TableOfDays<bool>     _hotWaterUsed  {};
    bool                       _vatOnPurchases   {};
};

#endif // BUILDING_H
