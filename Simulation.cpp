#include "Simulation.h"

#include "LibreOffice.h"

void Simulation::load()
{
    LibreOffice::loadSimulation(*this);

    _profiles = LibreOffice::loadTemperatureProfiles();
    buildings = LibreOffice::loadAllBuildings();
    Shared::setEnergies(LibreOffice::loadEnergies());

    auto scenarios = LibreOffice::scenarios();
    for (auto& sc : scenarios)
        addScenario(sc);

    for (auto& b : buildings)
        updateBuildingProfiles(b);

    if (auto i = std::find_if(buildings.begin(), buildings.end(), [](Building& b) { return b.invalid(); }) ;
        i != buildings.end())
    {
        emit loadingError(QString(i->name().c_str()) + " : " + QString(i->status().c_str()));
    }
    else
    {
        emit loadingSuccess();
    }

    clearSpreadsheet();

    for (auto& b : buildings)
        LibreOffice::updateBuildingsConsumption(b);

    update();
}

void Simulation::update()
{
    for (auto& sc : _scenarios)
    {
        try
        {
            sc.updateConsumptions();
            LibreOffice::updateScenarioCalculations(sc);
        }
        catch(Exception_NoSuchEnergy& ex)
        {
            sc.enable(false);
            emit loadingError(QString(sc.name().c_str()) + " : l'énergie '" + QString(ex.name.c_str()) + "' est invalide");
        }
    }

    emit updated();
}

void Simulation::startListeners()
{
    LibreOffice::startBuildingsListener([&]()
    {
        emit loadingSuccess();

        auto columns = LibreOffice::BuildingListener::columns();

        for (int column = columns.first ; column <= columns.second ; ++column)
        {
            Building newBuilding = LibreOffice::loadBuilding(column);
            auto old = std::find_if(buildings.begin(), buildings.end(), [&](Building& _b) { return _b.column() == column; });

            if (newBuilding.disabled())
            {
                if (old != buildings.end())
                {
                    buildings.erase(old);
                    clearSpreadsheet();
                }
                continue;
            }

            updateBuildingProfiles(newBuilding);

            if (old != buildings.end()) *old = newBuilding;
            else                        buildings.push_back(newBuilding);

            if (newBuilding.invalid())
            {
                clearSpreadsheet();
                emit loadingError(QString(newBuilding.name().c_str()) + " : " + QString(newBuilding.status().c_str()));
            }

            LibreOffice::updateBuildingsConsumption(newBuilding);
        }
        update();
    });

    LibreOffice::startProfilesListener([&]()
    {
        emit loadingSuccess();

        _profiles = LibreOffice::loadTemperatureProfiles();

        for (auto& b : buildings)
        {
            if (b.invalid()) b = LibreOffice::loadBuilding(b.column());  // New try
            updateBuildingProfiles(b);
            if (b.invalid())
            {
                clearSpreadsheet();
                emit loadingError(QString(b.name().c_str()) + " : " + QString(b.status().c_str()));
            }
        }

        update();
    });

    LibreOffice::startScenarioListener([&]()
    {
        emit loadingSuccess();

        auto sheet = LibreOffice::ScenarioListener::sheet();
        addScenario(sheet);
        update();
    });

    LibreOffice::startGeneralListener([&]()
    {
        emit loadingSuccess();

        Shared::setEnergies(LibreOffice::loadEnergies());
        update();
    });
}

void Simulation::updateBuildingProfiles(Building& b)
{
    if (b.invalid()) return;

    try
    {
        for (unsigned j = 0 ; j < 8760 ; ++j)
        {
            unsigned week_num = std::min(51u, j/168);
            const std::string& w = b.profile(week_num);
            const std::string& d = _profiles.weeks.at(w)[(j/24)%7];

            b.setTargetedTemperature(j, _profiles.days.at(d)[j%24]);
            b.setUseOfDHW(j/24, _profiles.dhw.at(d));
        }
    }
    catch (std::out_of_range&)
    {
        b.invalidate("Profils de températures incorrects.");
    }
}

void Simulation::clearSpreadsheet()
{
    std::vector<int> columns;

    for (auto& b : buildings) if (!b.invalid())
        columns.push_back(b.column());

    LibreOffice::clearBuildingsCalculations(columns);
}

void Simulation::addScenario(const std::string& sheet)
{
    if (auto sc = std::find_if(_scenarios.begin(), _scenarios.end(), [&](Scenario& s){ return s.sheet() == sheet;}) ;
        sc != _scenarios.end())
    {
        LibreOffice::loadScenario(*sc);
    }
    else
    {
        LibreOffice::loadScenario(_scenarios.emplace_back(sheet, &buildings));
    }
}

Scenario& Simulation::getScenario(const std::string& name)
{
    for (auto& sc : _scenarios)
    {
        if (sc.name() == name) return sc;
    }

    throw std::runtime_error("No scenario named " + name);
}

std::vector<std::string> Simulation::listScenarios()
{
    std::vector<std::string> values {};

    for (auto& sc : _scenarios)
    {
        if (sc.enabled()) values.push_back(sc.name());
    }

    return values;
}

void Simulation::clear()
{
    buildings.clear();
    _scenarios.clear();
}
