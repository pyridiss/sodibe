#ifndef LIBREOFFICEPRIVATE_H
#define LIBREOFFICEPRIVATE_H

#include <atomic>
#include <functional>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <cppu/unotype.hxx>

#include <com/sun/star/frame/Desktop.hpp>
#include <com/sun/star/lang/XComponent.hpp>
#include <com/sun/star/sheet/XSpreadsheet.hpp>
#include <com/sun/star/sheet/XSpreadsheetDocument.hpp>

using namespace com::sun::star;


struct Exception_NoSuchSheet
{
    std::string sheetName;
};

class LibreOfficePrivate
{
private:
    static void loadLibreOffice();
    static void closeLibreOffice();

    static uno::Reference<lang::XComponent> loadDocument (const std::string& url);
    static void setDocument  (uno::Reference<lang::XComponent> c);
    static void setSheet     (const std::string& s);
    static std::vector<std::string> sheets();
    static void addListener  (const std::string& sheetName, int x, int y, std::function<void(void)> callback);
    static void closeSheet   ();
    static void closeDocument();

    static uno::Reference<container::XEnumeration> getDocumentsEnumeration();

    static double        getValue  (int x, int y);
    static std::string   getFormula(int x, int y);
    static std::time_t   getDate   (int x, int y);

    static void setValue  (int x, int y, double value);
    static void setFormula(int x, int y, const std::string& value);
    static void clearCell (int x, int y);

private:
    inline static uno::Reference<uno::XComponentContext>       _xContext         {};
    inline static uno::Reference<lang::XMultiComponentFactory> _xServiceManager  {};
    inline static uno::Reference<frame::XDesktop2>             _xComponentLoader {};
    inline static uno::Reference<sheet::XSpreadsheetDocument>  _document         {};
    inline static uno::Reference<sheet::XSpreadsheet>          _sheet            {};
    inline static std::vector<std::thread>                     _listener         {};
    inline static std::atomic_bool                             _close            {};
    inline static std::mutex                                   _mutex            {};

    friend class LibreOffice;
};

#endif // LIBREOFFICEPRIVATE_H
