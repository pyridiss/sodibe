#include "BuildingAdaptation.h"

#include "Building.h"
#include "Scenario.h"

void BuildingAdaptation::setBuilding(Building* b)
{
    _building = b;
}

void BuildingAdaptation::setScenario(Scenario* sc)
{
    _scenario = sc;
}

void BuildingAdaptation::setHeatingSystem(const std::string& str)
{
    if (str == "Système local")
        heatingSystem = LocalSystem;
    else if (str == "Réseau")
        heatingSystem = Network;
    else
        heatingSystem = None;
}

void BuildingAdaptation::setWinterDhwSystem(const std::string& str)
{
    if (str == "Système local")
        winterDhwSystem = LocalSystem;
    else if (str == "Réseau")
        winterDhwSystem = Network;
    else if (str == "Ballon électrique")
        winterDhwSystem = ElectricHeater;
    else
        winterDhwSystem = None;
}

void BuildingAdaptation::setSummerDhwSystem(const std::string& str)
{
    if (str == "Système local")
        summerDhwSystem = LocalSystem;
    else if (str == "Ballon électrique")
        summerDhwSystem = ElectricHeater;
    else
        summerDhwSystem = None;
}

void BuildingAdaptation::setNewSystem(const std::string& energy, double inner, double system)
{
    newSystem_energy           = energy;
    newSystem_innerEfficiency  = inner;
    newSystem_systemEfficiency = system;
}

Building BuildingAdaptation::createNewBuilding() const
{
    Building newBuilding = *_building;
    if (!newSystem_energy.empty())  newBuilding.setEnergy(newSystem_energy);
    if (newSystem_innerEfficiency)  newBuilding.setInnerEfficiency (newSystem_innerEfficiency);
    if (newSystem_systemEfficiency) newBuilding.setSystemEfficiency(newSystem_systemEfficiency);

    return newBuilding;
}

KilowattHour BuildingAdaptation::needs() const
{
    if (!enabled || _building->invalid()) return KilowattHour(0);

    auto b = createNewBuilding();

    auto networkPeriod = _scenario->networkHeatingPeriod();
    auto heatingPeriod = (heatingSystem == Network && useNetworkHeatingPeriod) ? networkPeriod : b.heatingPeriod();

    auto heatNeeds = b.heatNeeds(heatingPeriod);
    auto dhwNeeds  = b.dhwNeeds();

    return KilowattHour(yearlyMeanSum(heatNeeds) + yearlyMeanSum(dhwNeeds));
}

void BuildingAdaptation::updateConsumption()
{
    if (!enabled || _building->invalid()) return;

    heatFromNetwork.fill(KilowattHour{0});
    localConsumptionForHeating.fill(KilowattHour{0});
    localConsumptionForDhw.fill(KilowattHour{0});
    localEnergyConsumption.fill(KilowattHour{0});
    electricityConsumption.fill(KilowattHour{0});

    auto b = createNewBuilding();

    auto heatNeeds = b.heatNeeds(Shared::wholeYear);
    auto dhwNeeds  = b.dhwNeeds();

    auto networkPeriod = _scenario->networkHeatingPeriod();
    auto heatingPeriod = (heatingSystem == Network && useNetworkHeatingPeriod) ? networkPeriod : b.heatingPeriod();

    for (unsigned i = 0 ; i < heatNeeds.size() ; ++i)
    {
        if (Shared::dateBetween(i, heatingPeriod))
        {
            if (heatingSystem == Network && Shared::dateBetween(i, networkPeriod))
                heatFromNetwork[i] = heatNeeds[i];
            else
                localEnergyConsumption[i] = localConsumptionForHeating[i] = heatNeeds[i] / b.systemEfficiency();
        }

        if (Shared::dateBetween(i, heatingPeriod))
        {
            switch(winterDhwSystem)
            {
            case Network:
                    heatFromNetwork[i] += dhwNeeds[i];
                break;
            case LocalSystem:
                localEnergyConsumption[i] += localConsumptionForDhw[i] = dhwNeeds[i] / b.systemEfficiency();
                break;
            case ElectricHeater:
                electricityConsumption[i] += localConsumptionForDhw[i] = dhwNeeds[i] / 0.95;
                break;
            default:
                break;
            }
        }
        else
        {
            switch(summerDhwSystem)
            {
            case LocalSystem:
                localEnergyConsumption[i] += localConsumptionForDhw[i] = dhwNeeds[i] / b.systemEfficiency();
                break;
            case ElectricHeater:
                electricityConsumption[i] += localConsumptionForDhw[i] = dhwNeeds[i] / 0.95;
                break;
            default:
                break;
            }
        }
    }
}

bool BuildingAdaptation::hasNewSystems() const
{
    return createNewBuilding().compareSystems(*building());
}

auto BuildingAdaptation::consumptions() const -> std::unordered_map<std::string, KilowattHour>
{
    std::unordered_map<std::string, KilowattHour> result;

    auto b = createNewBuilding();

    if (auto sum = yearlyMeanSum(localEnergyConsumption) ; sum > KilowattHour(1))
        result[b.energy()] += sum;

    if (auto sum = yearlyMeanSum(electricityConsumption) ; sum > KilowattHour(1))
        result[_building->electricity()] += sum;

    if (auto sum = yearlyMeanSum(heatFromNetwork) ; sum > KilowattHour(1))
        result["Réseau"] += sum;

    return result;
}

auto BuildingAdaptation::networkSubscription() const -> Kilowatt
{
    auto sorted = heatFromNetwork;
    std::partial_sort(sorted.begin(), sorted.begin() + 5 * Shared::years + 1, sorted.end(), std::greater());

    return Kilowatt(std::ceil(sorted[5 * Shared::years].get()));
}

void BuildingAdaptation::updateBill()
{
    for (auto& b : occupantBills)
        b.clear();
    for (auto& b : ownerBills)
        b.clear();

    auto networkConsumption = yearlyMeanSum(heatFromNetwork);

    auto b = createNewBuilding();

    // Local costs

    auto localEnergy      = Shared::energy(b.energy());
    auto localEnergyTotal = yearlyMeanSum(localEnergyConsumption);
    auto localEnergyPrice = localEnergy.price * (1 + (b.vatOnPurchases() ? localEnergy.vat : 0));

    for (int i = 0 ; i < 20 ; ++i)
        occupantBills[i].emplace(b.energy(), Euro(localEnergyTotal * localEnergyPrice * std::pow(1 + localEnergy.priceGrowth, i)));

    auto electricity      = Shared::energy(_building->electricity());
    auto electricityTotal = yearlyMeanSum(electricityConsumption);
    auto electricityPrice = electricity.price * (1 + (_building->vatOnPurchases() ? electricity.vat : 0));

    if (electricityTotal > KilowattHour(1))
        for (int i = 0 ; i < 20 ; ++i)
            occupantBills[i].emplace(_building->electricity(), electricityTotal * electricityPrice * std::pow(1 + electricity.priceGrowth,  i));

    for (int i = 0 ; i < 20 ; ++i)
        occupantBills[i].emplace("Entretien", maintenance * std::pow(1 + Shared::maintenanceGrowth, i)
                                      * (1 + (_building->vatOnPurchases() ? Shared::maintenanceVat : 0)));

    if (building()->owner() == building()->occupant())
    {
        for (int i = 0 ; i < 20 ; ++i)
            occupantBills[i].emplace("Provisions pour réparations", provision * std::pow(1 + Shared::provisionGrowth, i));
    }
    else
    {
        for (int i = 0 ; i < 20 ; ++i)
            ownerBills[i].emplace("À charge du propriétaire (provisions)", provision * std::pow(1 + Shared::provisionGrowth, i));
    }

    // Network

    if (building()->owner() == _scenario->projectOwner() && building()->occupant() == _scenario->projectOwner())
    {
        for (int i = 0 ; i < 20 ; ++i)
        {
            for (auto c : _scenario->detailedEnergyCost(i))
                occupantBills[i][c.first] += c.second * networkConsumption;
            occupantBills[i]["Provisions pour réparations"] += _scenario->detailedSubscriptionCost(i)["Provisions pour réparations"] * networkSubscription();
        }
    }
    else if (building()->owner() == _scenario->projectOwner() && building()->occupant() != _scenario->projectOwner())
    {
        for (int i = 0 ; i < 20 ; ++i)
        {
            occupantBills[i]["Facture de chauffage du locataire"] = _scenario->energyCost(i) * networkConsumption;
            ownerBills[i]["À charge du propriétaire (provisions)"] += _scenario->detailedSubscriptionCost(i)["Provisions pour réparations"] * networkSubscription();
        }
    }
    else
    {
        for (int i = 0 ; i < 20 ; ++i)
        {
            occupantBills[i]["Facture de chauffage / Abonnement"] = _scenario->subscriptionCost(i) * networkSubscription();
            occupantBills[i]["Facture de chauffage / Consommation"] = _scenario->energyCost(i) * networkConsumption;
        }
    }
}
