#include "GraphicTable.h"
#include "ui_GraphicTable.h"

#include <QAbstractAxis>
#include <QAbstractBarSeries>
#include <QBarCategoryAxis>
#include <QBarSet>
#include <QChartView>
#include <QFileDialog>
#include <QGraphicsProxyWidget>
#include <QPieSeries>
#include <QPieSlice>
#include <QPointer>
#include <QValueAxis>
#include <QXYSeries>
#include <QtSvg/QSvgGenerator>

#include <Shared.h>

GraphicTable::GraphicTable(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GraphicTable)
{
    ui->setupUi(this);
    _chartView = new QChartView();
    _chartView->setRenderHint(QPainter::Antialiasing);
    auto* scene = new QGraphicsScene();
    scene->addWidget(_chartView);

    ui->graphicsView->setScene(scene);

    auto* zoomIcon  = new QLabel();
    auto* zoomLabel = new QLabel("  Zoom :  ");
    auto* zoomValue = new QLabel("  100 %");

    zoomIcon->setPixmap(QPixmap(":/icons/icons/edit-find.png"));

    _slider = new QSlider();
    _slider->setOrientation(Qt::Horizontal);
    _slider->setRange(-100, 100);
    _slider->setMaximumSize(300, 50);

    ui->toolBar->addSeparator();
    ui->toolBar->addWidget(zoomIcon);
    ui->toolBar->addWidget(zoomLabel);
    ui->toolBar->addWidget(_slider);
    ui->toolBar->addWidget(zoomValue);

    connect(_slider, &QSlider::valueChanged, [this, zoomValue](int arg){
        double newZoom = std::pow(10, arg/100.0);
        _zoom = newZoom;
        resizeGraphic();
        zoomValue->setText(QString("  %L1 %").arg((int)(newZoom*100)));
    });

    for (auto* picker : ui->dockWidget->findChildren<ColorPicker*>())
    {
        Shared::startColorEnum();

        picker->insertColor(Qt::black,        "Noir");
        picker->insertColor(Qt::white,        "Blanc");

        for (auto c = Shared::nextColor() ; c.color.isValid() ; c = Shared::nextColor())
        {
            picker->insertColor(c.color, c.useName.c_str());
        }
    }
}

GraphicTable::~GraphicTable()
{
    delete ui;
}

void GraphicTable::setGraphic(Reports::Report_Chart* newGraphic)
{
    QChart* chart = _chartView->chart();

    if (newGraphic) _chartView->setChart(newGraphic);
    else            _chartView->setChart(new QChart);

    newGraphic->setAnimationOptions(QChart::AllAnimations);

    delete chart;

    ui->width->setValue(1500);
    ui->height->setValue(1000);
    _zoom = 1;
    _slider->setValue(0);
    resizeGraphic();

    ui->action_exportPNG->setEnabled(newGraphic != nullptr);
    ui->action_exportSVG->setEnabled(newGraphic != nullptr);

    if (newGraphic) resetDock();
    _report = newGraphic;
}

void GraphicTable::on_action_exportPNG_triggered()
{
    QString path = QFileDialog::getSaveFileName(this, "Exporter au format PNG", QString(), "Fichier PNG (*.png)");
    if (path.isEmpty()) return;
    if (!path.endsWith(".png", Qt::CaseInsensitive)) path += ".png";

    QPixmap pixMap = _chartView->grab();
    pixMap.save(path);
}

void GraphicTable::on_action_exportSVG_triggered()
{
    QString path = QFileDialog::getSaveFileName(this, "Exporter au format SVG", QString(), "Fichier SVG (*.svg)");
    if (path.isEmpty()) return;
    if (!path.endsWith(".svg", Qt::CaseInsensitive)) path += ".svg";

    auto currentScene = _chartView->scene();
    auto currentSize  = _chartView->size();

    QGraphicsScene scene(this);
    _chartView->setScene(&scene);
    scene.addItem(_chartView->chart());

    QSvgGenerator svg;
    svg.setFileName(path);
    svg.setSize(_chartView->size());

    QPainter painter(&svg);
    scene.render(&painter);

    _chartView->setScene(currentScene);
    _chartView->resize(currentSize);
    currentScene->addItem(_chartView->chart());
}

void GraphicTable::on_action_saveSettings_triggered()
{
    _report->saveSettings();
}

void GraphicTable::resetDock()
{
    auto* chart = _chartView->chart();

    ui->tabWidget->setCurrentIndex(0);

    // Tab General

    QSignalBlocker block01(ui->titleFont);
    QSignalBlocker block02(ui->titleFontSize);
    QSignalBlocker block03(ui->titleFontBold);
    QSignalBlocker block04(ui->titleFontItalic);
    QSignalBlocker block11(ui->legendFont);
    QSignalBlocker block12(ui->legendFontSize);
    QSignalBlocker block13(ui->legendFontBold);
    QSignalBlocker block14(ui->legendFontItalic);

    ui->titleText->setPlainText(chart->title());
    ui->titleFont->setCurrentFont(chart->titleFont());
    ui->titleFontSize->setValue(chart->titleFont().pointSize());
    ui->titleFontBold->setChecked(chart->titleFont().bold());
    ui->titleFontItalic->setChecked(chart->titleFont().italic());
    ui->titleFontColor->setCurrentColor(chart->titleBrush().color());

    ui->legendShow->setChecked(chart->legend()->isVisible());
    switch(chart->legend()->alignment())
    {
    case Qt::AlignTop:
        ui->legendPosition->setCurrentText("Haut");
        break;
    case Qt::AlignBottom:
        ui->legendPosition->setCurrentText("Bas");
        break;
    case Qt::AlignLeft:
        ui->legendPosition->setCurrentText("Gauche");
        break;
    case Qt::AlignRight:
        ui->legendPosition->setCurrentText("Droite");
        break;
    }
    ui->legendReverseMarkers->setChecked(chart->legend()->reverseMarkers());
    ui->legendFont->setCurrentFont(chart->legend()->font());
    ui->legendFontSize->setValue(chart->legend()->font().pointSize());
    ui->legendFontBold->setChecked(chart->legend()->font().bold());
    ui->legendFontItalic->setChecked(chart->legend()->font().italic());
    ui->legendFontColor->setCurrentColor(chart->legend()->labelColor());

    ui->miscLocalize->setChecked(chart->localizeNumbers());
    ui->miscBackgroundColor->setCurrentColor(chart->backgroundBrush().color());
    ui->miscPlotAreaColor->setCurrentColor(chart->plotAreaBackgroundBrush().color());

    // Tab Axes

    ui->axes->clear();

    for (auto* axis : chart->axes(Qt::Horizontal))
        ui->axes->addItem(axis->titleText(), QVariant::fromValue(QPointer(axis)));

    ui->axes->insertSeparator(ui->axes->count());

    for (auto* axis : chart->axes(Qt::Vertical))
        ui->axes->addItem(axis->titleText(), QVariant::fromValue(QPointer(axis)));

    if (ui->axes->count() == 1)  // Only the separator
    {
        ui->tabAxes->setEnabled(false);
    }
    else
    {
        ui->tabAxes->setEnabled(true);
        ui->axes->setCurrentIndex(0);
        on_axes_currentIndexChanged(0);
    }

    // Tab Data

    auto allCheckboxes = ui->series_scrollArea->findChildren<QCheckBox*>();
    for (auto c : allCheckboxes)
    {
        ui->series_scrollArea->layout()->removeWidget(c);
        delete c;
    }

    auto allGroupboxes = ui->series_scrollArea->findChildren<QGroupBox*>();
    for (auto c : allGroupboxes)
    {
        ui->series_scrollArea->layout()->removeWidget(c);
        delete c;
    }

    ui->series->clear();

    _series = chart->series();

    for (auto* series : _series)
    {
        if (auto* barSeries = dynamic_cast<QAbstractBarSeries*>(series) ; barSeries)
        {
            ui->series->addItem(series->name(), QVariant::fromValue(series));

            auto* group = new QGroupBox(series->name());
            group->setCheckable(true);
            group->setChecked(true);
            group->setProperty("series", QVariant::fromValue(series));
            group->setProperty("axes",   QVariant::fromValue(series->attachedAxes()));
            connect(group, &QGroupBox::toggled, [chart, group]()
            {
                if (group->isChecked())
                {
                    chart->addSeries(group->property("series").value<QAbstractSeries*>());
                    for (auto* axis : group->property("axes").value<QList<QAbstractAxis*>>())
                        group->property("series").value<QAbstractSeries*>()->attachAxis(axis);
                }
                else
                    chart->removeSeries(group->property("series").value<QAbstractSeries*>());
            });

            auto* layout = new QVBoxLayout;

            for (auto* set : barSeries->barSets())
            {
                ui->series->addItem("  ♦  " + set->label(), QVariant::fromValue(set));

                auto check = new QCheckBox(set->label());
                check->setChecked(true);
                check->setProperty("set", QVariant::fromValue(set));
                connect(check, &QCheckBox::stateChanged, [barSeries, check]()
                {
                    if (check->isChecked())
                        barSeries->append(check->property("set").value<QBarSet*>());
                    else
                        barSeries->take(check->property("set").value<QBarSet*>());
                });

                layout->addWidget(check);
            }

            group->setLayout(layout);

            ui->series_scrollArea->layout()->addWidget(group);
        }
        else if (auto* pieSeries = dynamic_cast<QPieSeries*>(series) ; pieSeries)
        {
            ui->series->addItem(series->name(), QVariant::fromValue(series));

            auto* group = new QGroupBox(series->name());
            group->setCheckable(true);
            group->setChecked(true);
            group->setProperty("series", QVariant::fromValue(series));
            connect(group, &QGroupBox::toggled, [chart, group]()
            {
                if (group->isChecked())
                {
                    chart->addSeries(group->property("series").value<QAbstractSeries*>());
                }
                else
                    chart->removeSeries(group->property("series").value<QAbstractSeries*>());
            });

            auto* layout = new QVBoxLayout;

            for (auto* slice : pieSeries->slices())
            {
                ui->series->addItem("  ♦  " + slice->label(), QVariant::fromValue(slice));

                auto check = new QCheckBox(slice->label());
                check->setChecked(true);
                check->setProperty("slice", QVariant::fromValue(slice));
                connect(check, &QCheckBox::stateChanged, [pieSeries, check]()
                {
                    if (check->isChecked())
                        pieSeries->append(check->property("slice").value<QPieSlice*>());
                    else
                        pieSeries->take(check->property("slice").value<QPieSlice*>());
                });

                layout->addWidget(check);
            }

            group->setLayout(layout);

            ui->series_scrollArea->layout()->addWidget(group);
        }
        else
        {
            ui->series->addItem(series->name(), QVariant::fromValue(series));

            auto* check = new QCheckBox(series->name());
            check->setChecked(true);
            check->setProperty("series", QVariant::fromValue(series));
            check->setProperty("axes",   QVariant::fromValue(series->attachedAxes()));
            connect(check, &QCheckBox::stateChanged, [chart, check]()
            {
                if (check->isChecked())
                {
                    chart->addSeries(check->property("series").value<QAbstractSeries*>());
                    for (auto* axis : check->property("axes").value<QList<QAbstractAxis*>>())
                        check->property("series").value<QAbstractSeries*>()->attachAxis(axis);
                }
                else
                    chart->removeSeries(check->property("series").value<QAbstractSeries*>());
            });

            ui->series_scrollArea->layout()->addWidget(check);
        }
    }

    ui->series->setCurrentIndex(0);
}

void GraphicTable::resizeGraphic()
{
    ui->scrollAreaWidgetContents->resize(15 + ui->width->value() * _zoom, 15 + ui->height->value() * _zoom);

    ui->graphicsView->scale(_zoom / _oldZoom, _zoom / _oldZoom);
    ui->graphicsView->scene()->setSceneRect(0, 0, ui->width->value(), ui->height->value());
    _oldZoom = _zoom;

    _chartView->resize(ui->width->value(), ui->height->value());
}

// Tab General

void GraphicTable::on_width_valueChanged([[maybe_unused]] int arg1)
{
    resizeGraphic();
}

void GraphicTable::on_height_valueChanged([[maybe_unused]] int arg1)
{
    resizeGraphic();
}

void GraphicTable::on_titleText_textChanged()
{
    _chartView->chart()->setTitle(ui->titleText->toPlainText());
}

void GraphicTable::titleFont_changed()
{
    QFont newFont = ui->titleFont->currentFont();
    newFont.setBold(ui->titleFontBold->isChecked());
    newFont.setItalic(ui->titleFontItalic->isChecked());
    newFont.setPointSize(ui->titleFontSize->value());

    _chartView->chart()->setTitleFont(newFont);
}

void GraphicTable::on_titleFontColor_colorChanged(const QColor& color)
{
    if (!color.isValid()) return;

    _chartView->chart()->setTitleBrush(color);
}

void GraphicTable::on_legendShow_stateChanged(int arg1)
{
    _chartView->chart()->legend()->setVisible(arg1);
}

void GraphicTable::on_legendPosition_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Haut")
        _chartView->chart()->legend()->setAlignment(Qt::AlignTop);
    else if (arg1 == "Bas")
        _chartView->chart()->legend()->setAlignment(Qt::AlignBottom);
    else if (arg1 == "Gauche")
        _chartView->chart()->legend()->setAlignment(Qt::AlignLeft);
    else if (arg1 == "Droite")
        _chartView->chart()->legend()->setAlignment(Qt::AlignRight);
}

void GraphicTable::on_legendReverseMarkers_stateChanged(int arg1)
{
    _chartView->chart()->legend()->setReverseMarkers(arg1);
}

void GraphicTable::legendFont_changed()
{
    QFont newFont = ui->legendFont->currentFont();
    newFont.setBold(ui->legendFontBold->isChecked());
    newFont.setItalic(ui->legendFontItalic->isChecked());
    newFont.setPointSize(ui->legendFontSize->value());

    _chartView->chart()->legend()->setFont(newFont);
}

void GraphicTable::on_legendFontColor_colorChanged(const QColor& color)
{
    if (!color.isValid()) return;

    _chartView->chart()->legend()->setLabelColor(color);
}

void GraphicTable::on_miscLocalize_clicked(bool checked)
{
    _chartView->chart()->setLocalizeNumbers(checked);
}

void GraphicTable::on_miscBackgroundColor_colorChanged(const QColor& color)
{
    _chartView->chart()->setBackgroundBrush(color);
}

void GraphicTable::on_miscPlotAreaColor_colorChanged(const QColor& color)
{
    _chartView->chart()->setPlotAreaBackgroundVisible(true);
    _chartView->chart()->setPlotAreaBackgroundBrush(color);
}

// Tab Axes

void GraphicTable::on_axes_currentIndexChanged(int index)
{
    if (index == -1 || ui->axes->currentData().isNull()) return;

    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();

    auto valueAxis = dynamic_cast<QValueAxis*>      (currentAxis.data());
    auto barAxis   = dynamic_cast<QBarCategoryAxis*>(currentAxis.data());

    // Tab General

    ui->axisVisible->setChecked(currentAxis->isVisible());
    ui->groupBox_axisValues->setEnabled(valueAxis);
    ui->groupBox_axisCategories->setEnabled(barAxis);
    ui->axisTicks->setEnabled(valueAxis);
    ui->axisLineColor->setCurrentColor(currentAxis->linePenColor());

    if (valueAxis)
    {
        ui->axisMinimum->setValue(valueAxis->min());
        ui->axisMaximum->setValue(valueAxis->max());
        ui->axisTicks->setValue(valueAxis->tickCount());
        ui->axisCategories->clear();
    }
    if (barAxis)
    {
        ui->axisCategories->clear();
        for (auto& cat : barAxis->categories())
        {
            auto* item = new QListWidgetItem(cat);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            item->setData(Qt::UserRole, cat);
            ui->axisCategories->addItem(item);
        }
    }

    ui->axisReverse->setChecked(currentAxis->isReverse());

    QSignalBlocker block(ui->axisAlignment);

    ui->axisAlignment->clear();
    if (currentAxis->orientation() == Qt::Horizontal)
    {
        ui->axisAlignment->addItem("Haut");
        ui->axisAlignment->addItem("Bas");
        if (currentAxis->alignment() == Qt::AlignTop)
            ui->axisAlignment->setCurrentText("Haut");
        if (currentAxis->alignment() == Qt::AlignBottom)
            ui->axisAlignment->setCurrentText("Bas");
    }
    else if (currentAxis->orientation() == Qt::Vertical)
    {
        ui->axisAlignment->addItem("Gauche");
        ui->axisAlignment->addItem("Droite");
        if (currentAxis->alignment() == Qt::AlignLeft)
            ui->axisAlignment->setCurrentText("Gauche");
        if (currentAxis->alignment() == Qt::AlignRight)
            ui->axisAlignment->setCurrentText("Droite");
    }

    ui->axisLineWidth->setValue(currentAxis->linePen().width());

    // Tab Grid

    ui->groupBox_mainGrid->setChecked(currentAxis->isGridLineVisible());
    ui->mainGridStyle->setCurrentIndex(currentAxis->gridLinePen().style()-1);
    ui->mainGridWidth->setValue(currentAxis->gridLinePen().width());
    ui->mainGridColor->setCurrentColor(currentAxis->gridLineColor());

    ui->groupBox_minorGrid->setEnabled(valueAxis);
    ui->minorGridColor->setCurrentColor(currentAxis->minorGridLineColor());

    if (valueAxis)
    {
        ui->groupBox_minorGrid->setChecked(currentAxis->isMinorGridLineVisible());
        ui->minorGridTicks->setValue(valueAxis->minorTickCount());
        ui->minorGridStyle->setCurrentIndex(currentAxis->minorGridLinePen().style()-1);
        ui->minorGridWidth->setValue(currentAxis->minorGridLinePen().width());
    }

    // Tab Title

    ui->axisTitleText->setText(currentAxis->titleText());
    ui->axisTitleVisible->setChecked(currentAxis->isTitleVisible());
    QSignalBlocker block1(ui->axisFont);
    QSignalBlocker block2(ui->axisFontSize);
    QSignalBlocker block3(ui->axisFontBold);
    QSignalBlocker block4(ui->axisFontItalic);
    QSignalBlocker block5(ui->axisFontColor);
    ui->axisFont->setCurrentFont(currentAxis->titleFont());
    ui->axisFontSize->setValue(currentAxis->titleFont().pointSize());
    ui->axisFontBold->setChecked(currentAxis->titleFont().bold());
    ui->axisFontItalic->setChecked(currentAxis->titleFont().italic());
    ui->axisFontColor->setCurrentColor(currentAxis->titleBrush().color());

    // Tab Labels

    QSignalBlocker block11(ui->labelsFont);
    QSignalBlocker block12(ui->labelsFontSize);
    QSignalBlocker block13(ui->labelsFontBold);
    QSignalBlocker block14(ui->labelsFontItalic);
    QSignalBlocker block15(ui->labelsColor);
    ui->labelsVisible->setChecked(currentAxis->labelsVisible());
    ui->labelsFont->setCurrentFont(currentAxis->labelsFont());
    ui->labelsFontSize->setValue(currentAxis->labelsFont().pointSize());
    ui->labelsFontBold->setChecked(currentAxis->labelsFont().bold());
    ui->labelsFontItalic->setChecked(currentAxis->labelsFont().italic());
    ui->labelsColor->setCurrentColor(currentAxis->labelsColor());
    ui->labelsFormat->setEnabled(valueAxis);

    if (valueAxis)
    {
        ui->labelsFormat->setText(valueAxis->labelFormat());
    }
}

void GraphicTable::on_axisVisible_stateChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setVisible(arg1);
}

void GraphicTable::on_axisMinimum_valueChanged(double arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setMin(arg1);
}

void GraphicTable::on_axisMaximum_valueChanged(double arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setMax(arg1);
}

void GraphicTable::on_axisReverse_stateChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setReverse(arg1);
}

void GraphicTable::on_axisCategories_itemChanged(QListWidgetItem* item)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto* barAxis = dynamic_cast<QBarCategoryAxis*>(currentAxis.data());

    barAxis->replace(item->data(Qt::UserRole).toString(), item->text());
    item->setData(Qt::UserRole, item->text());
}

void GraphicTable::on_axisAlignment_currentIndexChanged(const QString &arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();

    QList<QAbstractSeries*> attachedSeries;

    for (auto* series : _chartView->chart()->series())
        if (series->attachedAxes().contains(currentAxis))
            attachedSeries.push_back(series);

    _chartView->chart()->removeAxis(currentAxis);

    if (arg1 == "Haut")
        _chartView->chart()->addAxis(currentAxis, Qt::AlignTop);
    else if (arg1 == "Bas")
        _chartView->chart()->addAxis(currentAxis, Qt::AlignBottom);
    else if (arg1 == "Gauche")
        _chartView->chart()->addAxis(currentAxis, Qt::AlignLeft);
    else if (arg1 == "Droite")
        _chartView->chart()->addAxis(currentAxis, Qt::AlignRight);

    for (auto* series : attachedSeries)
        series->attachAxis(currentAxis);
}

void GraphicTable::on_axisLineWidth_valueChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto pen = currentAxis->linePen();
    pen.setWidth(arg1);
    currentAxis->setLinePen(pen);
}

void GraphicTable::on_axisTicks_valueChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto valueAxis = dynamic_cast<QValueAxis*>(currentAxis.data());
    valueAxis->setTickCount(arg1);
}

void GraphicTable::on_axisLineColor_colorChanged(const QColor& color)
{
    if (!color.isValid()) return;

    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setLinePenColor(color);
}

void GraphicTable::on_groupBox_mainGrid_toggled(bool checked)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setGridLineVisible(checked);
}

void GraphicTable::on_mainGridStyle_currentIndexChanged(int index)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto pen = currentAxis->gridLinePen();
    pen.setStyle(static_cast<Qt::PenStyle>(index+1));
    currentAxis->setGridLinePen(pen);
}

void GraphicTable::on_mainGridWidth_valueChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto pen = currentAxis->gridLinePen();
    pen.setWidth(arg1);
    currentAxis->setGridLinePen(pen);
}

void GraphicTable::on_mainGridColor_colorChanged(const QColor& color)
{
    if (!color.isValid()) return;

    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setGridLineColor(color);
}

void GraphicTable::on_groupBox_minorGrid_toggled(bool checked)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setMinorGridLineVisible(checked);
}

void GraphicTable::on_minorGridTicks_valueChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto valueAxis = dynamic_cast<QValueAxis*>(currentAxis.data());

    if (valueAxis) valueAxis->setMinorTickCount(arg1);
}

void GraphicTable::on_minorGridStyle_currentIndexChanged(int index)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto pen = currentAxis->minorGridLinePen();
    pen.setStyle(static_cast<Qt::PenStyle>(index+1));
    currentAxis->setMinorGridLinePen(pen);
}

void GraphicTable::on_minorGridWidth_valueChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto pen = currentAxis->minorGridLinePen();
    pen.setWidth(arg1);
    currentAxis->setMinorGridLinePen(pen);
}

void GraphicTable::on_minorGridColor_colorChanged(const QColor& color)
{
    if (!color.isValid()) return;

    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setMinorGridLineColor(color);
}

void GraphicTable::on_axisTitleText_textEdited(const QString &arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setTitleText(arg1);
    auto cursor = ui->axisTitleText->cursorPosition();
    ui->axes->insertItem(ui->axes->currentIndex(), arg1, ui->axes->currentData());
    ui->axes->removeItem(ui->axes->currentIndex());
    ui->axes->setCurrentText(arg1);
    ui->axisTitleText->setCursorPosition(cursor);
}

void GraphicTable::on_axisTitleVisible_stateChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setTitleVisible(arg1);
}

void GraphicTable::axisFont_changed()
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();

    QFont newFont = ui->axisFont->currentFont();
    newFont.setBold(ui->axisFontBold->isChecked());
    newFont.setItalic(ui->axisFontItalic->isChecked());
    newFont.setPointSize(ui->axisFontSize->value());

    currentAxis->setTitleFont(newFont);
}

void GraphicTable::on_axisFontColor_colorChanged(const QColor& color)
{
    if (!color.isValid()) return;

    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setTitleBrush(color);
}

void GraphicTable::on_labelsVisible_stateChanged(int arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setLabelsVisible(arg1);
}

void GraphicTable::labelsFont_changed()
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();

    QFont newFont = ui->labelsFont->currentFont();
    newFont.setBold(ui->labelsFontBold->isChecked());
    newFont.setItalic(ui->labelsFontItalic->isChecked());
    newFont.setPointSize(ui->labelsFontSize->value());

    currentAxis->setLabelsFont(newFont);
}

void GraphicTable::on_labelsColor_colorChanged(const QColor& color)
{
    if (!color.isValid()) return;

    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    currentAxis->setLabelsColor(color);
}

void GraphicTable::on_labelsFormat_textEdited(const QString &arg1)
{
    auto currentAxis = ui->axes->currentData().value<QPointer<QAbstractAxis>>();
    auto valueAxis = dynamic_cast<QValueAxis*>(currentAxis.data());
    if (valueAxis)
        valueAxis->setLabelFormat(arg1);
}

// Tab Data

void GraphicTable::on_series_currentIndexChanged(int index)
{
    if (index == -1 || ui->series->currentData().isNull()) return;

    auto currentSeries = ui->series->currentData().value<QObject*>();

    if (auto* barSeries = dynamic_cast<QAbstractBarSeries*>(currentSeries))
    {
        ui->stackedDataWidgets->setEnabled(true);
        ui->stackedDataWidgets->setCurrentIndex(0);
        ui->QBarSeries_barWidth->setValue(barSeries->barWidth()*100);
        ui->QBarSeries_labelsVisible->setChecked(barSeries->isLabelsVisible());
        switch(barSeries->labelsPosition())
        {
        case QAbstractBarSeries::LabelsCenter:
            ui->QBarSeries_labelsPosition->setCurrentText("Centré");
            break;
        case QAbstractBarSeries::LabelsInsideEnd:
            ui->QBarSeries_labelsPosition->setCurrentText("En haut");
            break;
        case QAbstractBarSeries::LabelsInsideBase:
            ui->QBarSeries_labelsPosition->setCurrentText("En bas");
            break;
        case QAbstractBarSeries::LabelsOutsideEnd:
            ui->QBarSeries_labelsPosition->setCurrentText("À l'extérieur");
            break;
        }

        ui->QBarSeries_labelsFormat->setText(barSeries->labelsFormat());
        ui->QBarSeries_labelsAngle->setValue(barSeries->labelsAngle());
        ui->QBarSeries_labelsPrecision->setValue(barSeries->labelsPrecision());
    }
    else if (auto* barSet = dynamic_cast<QBarSet*>(currentSeries))
    {
        ui->stackedDataWidgets->setEnabled(true);
        ui->stackedDataWidgets->setCurrentIndex(1);

        QSignalBlocker block1(ui->QBarSet_LabelFont);
        QSignalBlocker block2(ui->QBarSet_LabelFontSize);
        QSignalBlocker block3(ui->QBarSet_LabelFontBold);
        QSignalBlocker block4(ui->QBarSet_LabelFontItalic);
        ui->QBarSet_Legend->setText(barSet->label());
        ui->QBarSet_Color->setCurrentColor(barSet->color());
        ui->QBarSet_BorderWidth->setValue(barSet->pen().width());
        ui->QBarSet_BorderColor->setCurrentColor(barSet->borderColor());
        ui->QBarSet_LabelFont->setCurrentFont(barSet->labelFont());
        ui->QBarSet_LabelFontSize->setValue(barSet->labelFont().pointSize());
        ui->QBarSet_LabelFontBold->setChecked(barSet->labelFont().bold());
        ui->QBarSet_LabelFontItalic->setChecked(barSet->labelFont().italic());
        ui->QBarSet_LabelColor->setCurrentColor(barSet->labelColor());
    }
    else if (auto* xySeries = dynamic_cast<QXYSeries*>(currentSeries))
    {
        ui->stackedDataWidgets->setEnabled(true);
        ui->stackedDataWidgets->setCurrentIndex(2);

        ui->QXYSeries_Color->setCurrentColor(xySeries->color());
        ui->QXYSeries_Width->setValue(xySeries->pen().width());
        ui->QXYSeries_Style->setCurrentIndex(xySeries->pen().style()-1);
    }
    else if (auto* pieSeries = dynamic_cast<QPieSeries*>(currentSeries))
    {
        ui->stackedDataWidgets->setEnabled(true);
        ui->stackedDataWidgets->setCurrentIndex(3);

        ui->QPieSeries_Horizontal->setValue(pieSeries->horizontalPosition()*100);
        ui->QPieSeries_Vertical->setValue(pieSeries->verticalPosition()*100);
        ui->QPieSeries_Size->setValue(pieSeries->pieSize()*100);
        ui->QPieSeries_Hole->setValue(pieSeries->holeSize()*100);
        ui->QPieSeries_StartAngle->setValue(pieSeries->pieStartAngle());
        ui->QPieSeries_EndAngle->setValue(pieSeries->pieEndAngle());
    }
    else if (auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries))
    {
        ui->stackedDataWidgets->setEnabled(true);
        ui->stackedDataWidgets->setCurrentIndex(4);

        QSignalBlocker block1(ui->QPieSlice_LabelFont);
        QSignalBlocker block2(ui->QPieSlice_LabelFontBold);
        QSignalBlocker block3(ui->QPieSlice_LabelFontItalic);
        QSignalBlocker block4(ui->QPieSlice_LabelFontSize);
        ui->QPieSlice_Color->setCurrentColor(pieSlice->color());
        ui->QPieSlice_Explode->setValue(pieSlice->isExploded() * pieSlice->explodeDistanceFactor() * 100);
        ui->QPieSlice_LabelVisible->setChecked(pieSlice->isLabelVisible());
        ui->QPieSlice_Label->setText(pieSlice->label());
        ui->QPieSlice_LabelFont->setCurrentFont(pieSlice->labelFont());
        ui->QPieSlice_LabelFontBold->setChecked(pieSlice->labelFont().bold());
        ui->QPieSlice_LabelFontItalic->setChecked(pieSlice->labelFont().italic());
        ui->QPieSlice_LabelFontSize->setValue(pieSlice->labelFont().pointSize());
        ui->QPieSlice_LabelColor->setCurrentColor(pieSlice->labelColor());
        ui->QPieSlice_LabelPosition->setCurrentIndex(pieSlice->labelPosition());
        ui->QPieSlice_LabelArm->setValue(pieSlice->labelArmLengthFactor()*100);
        ui->QPieSlice_BorderWidth->setValue(pieSlice->borderWidth());
        ui->QPieSlice_BorderColor->setCurrentColor(pieSlice->borderColor());
    }
    else
    {
        ui->stackedDataWidgets->setEnabled(false);
    }
}

void GraphicTable::on_QBarSeries_barWidth_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSeries = dynamic_cast<QAbstractBarSeries*>(currentSeries);

    barSeries->setBarWidth(arg1/100.0);
}

void GraphicTable::on_QBarSeries_labelsVisible_stateChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSeries = dynamic_cast<QAbstractBarSeries*>(currentSeries);

    barSeries->setLabelsVisible(arg1);
}

void GraphicTable::on_QBarSeries_labelsPosition_currentIndexChanged(const QString &arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSeries = dynamic_cast<QAbstractBarSeries*>(currentSeries);

    if (arg1 == "Centré")
        barSeries->setLabelsPosition(QAbstractBarSeries::LabelsCenter);
    else if (arg1 == "En haut")
        barSeries->setLabelsPosition(QAbstractBarSeries::LabelsInsideEnd);
    else if (arg1 == "En bas")
        barSeries->setLabelsPosition(QAbstractBarSeries::LabelsInsideBase);
    else if (arg1 == "À l'extérieur")
        barSeries->setLabelsPosition(QAbstractBarSeries::LabelsOutsideEnd);
}

void GraphicTable::on_QBarSeries_labelsFormat_textEdited(const QString &arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSeries = dynamic_cast<QAbstractBarSeries*>(currentSeries);

    barSeries->setLabelsFormat(arg1);
}

void GraphicTable::on_QBarSeries_labelsAngle_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSeries = dynamic_cast<QAbstractBarSeries*>(currentSeries);

    if (arg1 == 0) arg1 = 360;
    barSeries->setLabelsAngle(arg1);
}

void GraphicTable::on_QBarSeries_labelsPrecision_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSeries = dynamic_cast<QAbstractBarSeries*>(currentSeries);

    barSeries->setLabelsPrecision(arg1);
}

void GraphicTable::on_QBarSet_Legend_textEdited(const QString &arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSet = dynamic_cast<QBarSet*>(currentSeries);

    barSet->setLabel(arg1);
}

void GraphicTable::on_QBarSet_Color_colorChanged(const QColor& color)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSet = dynamic_cast<QBarSet*>(currentSeries);

    barSet->setColor(color);
}

void GraphicTable::on_QBarSet_BorderWidth_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSet = dynamic_cast<QBarSet*>(currentSeries);

    auto pen = barSet->pen();
    pen.setWidth(arg1);
    barSet->setPen(pen);
}

void GraphicTable::on_QBarSet_BorderColor_colorChanged(const QColor& color)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSet = dynamic_cast<QBarSet*>(currentSeries);

    barSet->setBorderColor(color);
}

void GraphicTable::QBarSet_LabelFont_changed()
{
    QFont newFont = ui->QBarSet_LabelFont->currentFont();
    newFont.setBold(ui->QBarSet_LabelFontBold->isChecked());
    newFont.setItalic(ui->QBarSet_LabelFontItalic->isChecked());
    newFont.setPointSize(ui->QBarSet_LabelFontSize->value());

    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSet = dynamic_cast<QBarSet*>(currentSeries);

    barSet->setLabelFont(newFont);
}

void GraphicTable::on_QBarSet_LabelColor_colorChanged(const QColor& color)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* barSet = dynamic_cast<QBarSet*>(currentSeries);

    barSet->setLabelColor(color);
}

void GraphicTable::on_QXYSeries_Color_colorChanged(const QColor& color)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* xySeries = dynamic_cast<QXYSeries*>(currentSeries);

    xySeries->setColor(color);
}

void GraphicTable::on_QXYSeries_Width_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* xySeries = dynamic_cast<QXYSeries*>(currentSeries);

    auto pen = xySeries->pen();
    pen.setWidth(arg1);
    xySeries->setPen(pen);
}

void GraphicTable::on_QXYSeries_Style_currentIndexChanged(int index)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* xySeries = dynamic_cast<QXYSeries*>(currentSeries);

    auto pen = xySeries->pen();
    pen.setStyle(static_cast<Qt::PenStyle>(index+1));
    xySeries->setPen(pen);
}

void GraphicTable::on_QPieSeries_Horizontal_valueChanged(int position)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSeries = dynamic_cast<QPieSeries*>(currentSeries);

    pieSeries->setHorizontalPosition((double)position/100.0);
}

void GraphicTable::on_QPieSeries_Vertical_valueChanged(int position)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSeries = dynamic_cast<QPieSeries*>(currentSeries);

    pieSeries->setVerticalPosition((double)position/100.0);
}

void GraphicTable::on_QPieSeries_Size_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSeries = dynamic_cast<QPieSeries*>(currentSeries);

    pieSeries->setPieSize((double)arg1/100.0);
}

void GraphicTable::on_QPieSeries_Hole_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSeries = dynamic_cast<QPieSeries*>(currentSeries);

    pieSeries->setHoleSize((double)arg1/100.0);
}

void GraphicTable::on_QPieSeries_StartAngle_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSeries = dynamic_cast<QPieSeries*>(currentSeries);

    pieSeries->setPieStartAngle(arg1);
}

void GraphicTable::on_QPieSeries_EndAngle_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSeries = dynamic_cast<QPieSeries*>(currentSeries);

    pieSeries->setPieEndAngle(arg1);
}

void GraphicTable::on_QPieSlice_Color_colorChanged(const QColor& color)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setColor(color);
}

void GraphicTable::on_QPieSlice_Explode_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setExploded(arg1);
    pieSlice->setExplodeDistanceFactor((double)arg1/100.0);
}

void GraphicTable::on_QPieSlice_LabelVisible_clicked(bool checked)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setLabelVisible(checked);
}

void GraphicTable::on_QPieSlice_Label_textEdited(const QString &arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setLabel(arg1);
}

void GraphicTable::QPieSlice_LabelFont_changed()
{
    QFont newFont = ui->QPieSlice_LabelFont->currentFont();
    newFont.setBold(ui->QPieSlice_LabelFontBold->isChecked());
    newFont.setItalic(ui->QPieSlice_LabelFontItalic->isChecked());
    newFont.setPointSize(ui->QPieSlice_LabelFontSize->value());

    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setLabelFont(newFont);
}

void GraphicTable::on_QPieSlice_LabelColor_colorChanged(const QColor& color)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setLabelColor(color);
}

void GraphicTable::on_QPieSlice_LabelPosition_currentIndexChanged(int index)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setLabelPosition(static_cast<QPieSlice::LabelPosition>(index));
}

void GraphicTable::on_QPieSlice_LabelArm_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setLabelArmLengthFactor((double)arg1/100.0);
}

void GraphicTable::on_QPieSlice_BorderWidth_valueChanged(int arg1)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setBorderWidth(arg1);
}

void GraphicTable::on_QPieSlice_BorderColor_colorChanged(const QColor& color)
{
    if (ui->series->currentData().isNull()) return;
    auto currentSeries = ui->series->currentData().value<QObject*>();
    auto* pieSlice = dynamic_cast<QPieSlice*>(currentSeries);

    pieSlice->setBorderColor(color);
}
