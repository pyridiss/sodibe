#ifndef GRAPHICTABLE_H
#define GRAPHICTABLE_H

#include <QChart>
#include <QChartView>
#include <QListWidgetItem>
#include <QMainWindow>
#include <QSlider>

#include "reports/Report.h"

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class GraphicTable;
}

class GraphicTable : public QMainWindow
{
    Q_OBJECT

public:
    explicit GraphicTable(QWidget *parent = nullptr);
    ~GraphicTable();

    void setGraphic(Reports::Report_Chart* newGraphic);

private slots:
    void on_action_exportPNG_triggered();
    void on_action_exportSVG_triggered();

    // Tab General

    void on_width_valueChanged(int arg1);
    void on_height_valueChanged(int arg1);

    void on_titleText_textChanged();
    void titleFont_changed();
    void on_titleFont_currentFontChanged(const QFont&) {titleFont_changed();}
    void on_titleFontSize_valueChanged  (int)          {titleFont_changed();}
    void on_titleFontBold_clicked       (bool)         {titleFont_changed();}
    void on_titleFontItalic_clicked     (bool)         {titleFont_changed();}
    void on_titleFontColor_colorChanged(const QColor& color);

    void on_legendShow_stateChanged(int arg1);
    void on_legendPosition_currentIndexChanged(const QString &arg1);
    void on_legendReverseMarkers_stateChanged(int arg1);
    void legendFont_changed();
    void on_legendFont_currentFontChanged(const QFont&) {legendFont_changed();}
    void on_legendFontSize_valueChanged  (int)          {legendFont_changed();}
    void on_legendFontBold_clicked       (bool)         {legendFont_changed();}
    void on_legendFontItalic_clicked     (bool)         {legendFont_changed();}
    void on_legendFontColor_colorChanged(const QColor& color);

    void on_miscLocalize_clicked(bool checked);
    void on_miscBackgroundColor_colorChanged(const QColor& color);
    void on_miscPlotAreaColor_colorChanged(const QColor& color);

    // Tab Axes

    void on_axes_currentIndexChanged(int index);
    void on_axisVisible_stateChanged(int arg1);
    void on_axisMinimum_valueChanged(double arg1);
    void on_axisMaximum_valueChanged(double arg1);
    void on_axisReverse_stateChanged(int arg1);
    void on_axisCategories_itemChanged(QListWidgetItem* item);

    void on_axisAlignment_currentIndexChanged(const QString &arg1);
    void on_axisLineWidth_valueChanged(int arg1);
    void on_axisTicks_valueChanged(int arg1);
    void on_axisLineColor_colorChanged(const QColor& color);

    void on_groupBox_mainGrid_toggled(bool checked);
    void on_mainGridStyle_currentIndexChanged(int index);
    void on_mainGridWidth_valueChanged(int arg1);
    void on_mainGridColor_colorChanged(const QColor& color);
    void on_groupBox_minorGrid_toggled(bool checked);
    void on_minorGridTicks_valueChanged(int arg1);
    void on_minorGridStyle_currentIndexChanged(int index);
    void on_minorGridWidth_valueChanged(int arg1);
    void on_minorGridColor_colorChanged(const QColor& color);

    void on_axisTitleText_textEdited(const QString &arg1);
    void on_axisTitleVisible_stateChanged(int arg1);
    void axisFont_changed();
    void on_axisFont_currentFontChanged(const QFont&) {axisFont_changed();}
    void on_axisFontSize_valueChanged  (int)          {axisFont_changed();}
    void on_axisFontBold_clicked       (bool)         {axisFont_changed();}
    void on_axisFontItalic_clicked     (bool)         {axisFont_changed();}
    void on_axisFontColor_colorChanged(const QColor& color);

    void on_labelsVisible_stateChanged(int arg1);
    void labelsFont_changed();
    void on_labelsFont_currentFontChanged(const QFont&) {labelsFont_changed();}
    void on_labelsFontSize_valueChanged  (int)          {labelsFont_changed();}
    void on_labelsFontBold_clicked       (bool)         {labelsFont_changed();}
    void on_labelsFontItalic_clicked     (bool)         {labelsFont_changed();}
    void on_labelsColor_colorChanged(const QColor& color);
    void on_labelsFormat_textEdited(const QString &arg1);


    void on_series_currentIndexChanged(int index);

    void on_QBarSeries_barWidth_valueChanged(int arg1);
    void on_QBarSeries_labelsVisible_stateChanged(int arg1);
    void on_QBarSeries_labelsPosition_currentIndexChanged(const QString &arg1);
    void on_QBarSeries_labelsFormat_textEdited(const QString &arg1);
    void on_QBarSeries_labelsAngle_valueChanged(int arg1);
    void on_QBarSeries_labelsPrecision_valueChanged(int arg1);

    void on_QBarSet_Legend_textEdited(const QString &arg1);
    void on_QBarSet_Color_colorChanged(const QColor& color);
    void on_QBarSet_BorderWidth_valueChanged(int arg1);
    void on_QBarSet_BorderColor_colorChanged(const QColor& color);
    void QBarSet_LabelFont_changed();
    void on_QBarSet_LabelFont_currentFontChanged(const QFont&) {QBarSet_LabelFont_changed();}
    void on_QBarSet_LabelFontSize_valueChanged  (int)          {QBarSet_LabelFont_changed();}
    void on_QBarSet_LabelFontBold_clicked       (bool)         {QBarSet_LabelFont_changed();}
    void on_QBarSet_LabelFontItalic_clicked     (bool)         {QBarSet_LabelFont_changed();}
    void on_QBarSet_LabelColor_colorChanged(const QColor& color);

    void on_QXYSeries_Color_colorChanged(const QColor& color);
    void on_QXYSeries_Width_valueChanged(int arg1);
    void on_QXYSeries_Style_currentIndexChanged(int index);

    void on_QPieSeries_Horizontal_valueChanged(int position);
    void on_QPieSeries_Vertical_valueChanged(int position);
    void on_QPieSeries_Size_valueChanged(int arg1);
    void on_QPieSeries_Hole_valueChanged(int arg1);
    void on_QPieSeries_StartAngle_valueChanged(int arg1);
    void on_QPieSeries_EndAngle_valueChanged(int arg1);

    void on_QPieSlice_Color_colorChanged(const QColor& color);
    void on_QPieSlice_Explode_valueChanged(int arg1);
    void on_QPieSlice_LabelVisible_clicked(bool checked);
    void on_QPieSlice_Label_textEdited(const QString &arg1);
    void QPieSlice_LabelFont_changed();
    void on_QPieSlice_LabelFont_currentFontChanged(const QFont&) {QPieSlice_LabelFont_changed();}
    void on_QPieSlice_LabelFontSize_valueChanged  (int)          {QPieSlice_LabelFont_changed();}
    void on_QPieSlice_LabelFontBold_clicked       (bool)         {QPieSlice_LabelFont_changed();}
    void on_QPieSlice_LabelFontItalic_clicked     (bool)         {QPieSlice_LabelFont_changed();}
    void on_QPieSlice_LabelColor_colorChanged(const QColor& color);
    void on_QPieSlice_LabelPosition_currentIndexChanged(int index);
    void on_QPieSlice_LabelArm_valueChanged(int arg1);
    void on_QPieSlice_BorderWidth_valueChanged(int arg1);
    void on_QPieSlice_BorderColor_colorChanged(const QColor& color);

    void on_action_saveSettings_triggered();

private:
    void resetDock();
    void resizeGraphic();

private:
    Ui::GraphicTable *ui;
    Reports::Report_Chart* _report;
    QList<QAbstractSeries*> _series;
    QChart* _chart;
    QChartView* _chartView;
    QSlider* _slider;
    double _zoom {1};
    double _oldZoom {1};
};

#endif // GRAPHICTABLE_H
