#include "Building.h"

#include "Shared.h"


void Building::setOwner(const std::string& value)
{
    _owner = value;
}

void Building::setOccupant(const std::string& value)
{
    _occupant = value;
}

void Building::setSurface(double value)
{
    _surface = Meter2(value);
}

void Building::setVolume(double value)
{
    _volume = Meter3(value);
}

void Building::setHeatLossCoeff(double value)
{
    _heatLossCoeff = Watt_per_KelvinMeter3(value);
}

void Building::setSouthWindowSurface(double value)
{
    _southWindow = Meter2(value);
}

void Building::setInertia(double value)
{
    _inertia = Coefficient(value);
}

void Building::setEnergy(const std::string& value)
{
    _energy = value;
}

void Building::setInnerEfficiency(double value)
{
    _innerEfficiency = value;
    if (value == 0) invalidate("Rendement intérieur non valide.");
}

void Building::setSystemEfficiency(double value)
{
    _systemEfficiency = value;
    if (value == 0) invalidate("Rendement de production non valide");
}

void Building::setSystemDateOn(std::time_t value)
{
    _systemDateOn = value;
}

void Building::setSystemDateOff(std::time_t value)
{
    _systemDateOff = value;
}

void Building::setElectricity(const std::string& value)
{
    _electricity = value;
}

void Building::setDhwSystem(DhwSystem winter, DhwSystem summer)
{
    _winterDhwSystem = winter;
    _summerDhwSystem = summer;
}

void Building::setDHWVolume(double value)
{
    _dhwQuantity = Liter_per_Day(value);
}

void Building::setDHWProfile(const std::string& value)
{
    if (value == "Individuel – Accumulation")    _dhwProfile = StorageTankHeater;
    else if (value == "Individuel – Instantané") _dhwProfile = TanklessHeater;
    else if (value == "Collectif")               _dhwProfile = CollectiveBuilding;
}

void Building::setWeekProfile(unsigned int week, std::string profile)
{
    _profiles[week] = std::move(profile);
}

void Building::setTargetedTemperature(unsigned hour, double value)
{
    _temperatures[hour] = Kelvin(value);
}

void Building::setUseOfDHW(unsigned day, bool value)
{
    _hotWaterUsed[day] = value;
}

void Building::setVatOnPurchases(bool value)
{
    _vatOnPurchases = value;
}

Sim_TableOfHours<KilowattHour> Building::heatNeeds(Period period) const
{
    Sim_TableOfHours<KilowattHour> result;

    auto meanTemp = std::accumulate(_temperatures.begin(), _temperatures.end(), Kelvin(0)) / 8760;

    for (unsigned i = 0 ; i < result.size() ; ++i)
    {
        if (!Shared::dateBetween(i, period)) continue;

        unsigned hour = i % 8760;
        Kelvin outside = (hour == 0) ? Shared::temperature(i)
                                  : Shared::temperature(i) * (1 - _inertia.get()) + _temperatures[hour-1] * _inertia;

        auto envelope = _volume * _heatLossCoeff * (_temperatures[hour] - outside);

        auto solar = (Shared::solarElevation(i) == 0)
                   ? Watt(0)
                   : Watt(Shared::solarRadiation(i) * _southWindow / tan(Shared::solarElevation(i)));

        auto internal = Shared::internalHeatGains(_temperatures[hour].get() >= meanTemp.get()) * _surface;

        Kilowatt power {(envelope - solar - internal) / _innerEfficiency};
        if (power.get() < 0) power = Kilowatt(0);

        result[i] = power * Hour(1);
    }

    return result;
}

Sim_TableOfHours<KilowattHour> Building::dhwNeeds(Period period) const
{
    Sim_TableOfHours<KilowattHour> result;

    for (unsigned hour = 0 ; hour < result.size() ; ++hour)
    {
        if (!Shared::dateBetween(hour, period)) continue;

        if (_hotWaterUsed[(hour/24)%365])
            result[hour] = Kilowatt(Shared::dhw(_dhwProfile, hour) * _dhwQuantity) * Hour(1);
        else
            result[hour] = KilowattHour(0);
    }

    return result;
}

KilowattHour Building::currentHeatConsumption() const
{
    auto needs = heatNeeds(heatingPeriod());

    auto result = yearlyMeanSum(needs);

    result = result / _systemEfficiency;

    return result;
}

KilowattHour Building::currentWinterDhwConsumption() const
{
    auto needs = dhwNeeds(heatingPeriod());

    auto result = yearlyMeanSum(needs);

    double eff = (_winterDhwSystem == HeatingSystem) ? _systemEfficiency : 0.95;
    result = result / eff;

    return result;
}

KilowattHour Building::currentSummerDhwConsumption() const
{
    auto needs = dhwNeeds(std::make_pair(_systemDateOff, _systemDateOn));

    auto result = yearlyMeanSum(needs);

    double eff = (_summerDhwSystem == HeatingSystem) ? _systemEfficiency : 0.95;
    result = result / eff;

    return result;
}

Kilowatt Building::estimatedPower() const
{
    auto needs = heatNeeds();

    std::partial_sort(needs.begin(), needs.begin() + 5 * Shared::years + 1, needs.end(), std::greater());

    auto dhw = dhwNeeds();

    auto dhwMax = std::max_element(dhw.begin(), dhw.end());

    return Kilowatt((needs[5 * Shared::years] + *dhwMax) / Hour(1));
}

bool Building::compareSystems(const Building& other) const
{
    return _energy           != other._energy           ||
           _innerEfficiency  != other._innerEfficiency  ||
           _systemEfficiency != other._systemEfficiency;
}
