#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <atomic>
#include <thread>

#include <QChart>
#include <QLabel>
#include <QMainWindow>
#include <QPixmap>
#include <QStandardItemModel>


#include "GraphicTable.h"
#include "LibreOffice.h"
#include "Simulation.h"

namespace Ui {
class MainWindow;
}

namespace Reports {
class Report_Chart;
class Report_Model;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void addMdiChart(std::string title, Reports::Report_Chart* report);
    void addMdiTable(std::string title, Reports::Report_Model* report);

    void documentsWatcher();

signals:
    void documentClosed();
    void documentsModified();

public slots:
    void cleanup();

private slots:
    void updateDocumentsList();

    void informLoadingError(QString message);
    void informLoadingSuccess();

    void openDocument(Document doc);
    void closeDocument();

    void redrawGraphic();

    void on_action_openLOFile_triggered();
    void on_action_reloadLOFile_triggered();
    void on_action_clearMdiArea_triggered();
    void on_action_toGraphicTable_triggered();
    void on_action_copyToClipboard_triggered();

    void on_initial_consumptions_clicked();
    void on_initial_energies_clicked();

    void on_scenario_summaryperbuilding_clicked();
    void on_scenario_consumptionpersourceperbuilding_clicked();
    void on_scenario_consumptionperenergy_clicked();
    void on_scn_dailymaximums_clicked();
    void on_scn_detailedcharges_clicked();
    void on_scn_globalcost_clicked();
    void on_scenario_ldc_clicked();
    void on_scenario_localinvestments_clicked();

    void on_cmp_chargesbybuilding_clicked();
    void on_cmp_environmentalimpacts_clicked();
    void on_cmp_investments_clicked();
    void on_cmp_cumulativecosts_clicked();
    void on_cmp_detailedcharges_clicked();


private:
    Ui::MainWindow*  ui;
    GraphicTable*    _graphicTable     {};
    Simulation       _simulation       {};
    std::atomic_bool _close            {};
    std::thread      _documentsWatcher {};
    QLabel*          _statusIcon       {};
    QLabel*          _statusMessage    {};
    QPixmap          _statusError      {};
    QPixmap          _statusInfo       {};
};

#endif // MAINWINDOW_H
