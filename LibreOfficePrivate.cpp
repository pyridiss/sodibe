#include "LibreOfficePrivate.h"

#include <chrono>

#include <cppuhelper/bootstrap.hxx>

using namespace com::sun::star;

void LibreOfficePrivate::loadLibreOffice()
{
    try
    {
        _xContext.set(::cppu::bootstrap());
        if (!_xContext.is()) throw std::runtime_error("LibreOffice: No context");

        _xServiceManager.set(LibreOfficePrivate::_xContext->getServiceManager());
        if (!_xServiceManager.is()) throw std::runtime_error("LibreOffice: No service manager");

        _xComponentLoader.set(frame::Desktop::create(LibreOfficePrivate::_xContext));
    }
    catch (uno::Exception& ex)
    {
        std::string err = "LibreOfficePrivate::loadLibreOffice() failed (UNO exception): ";
        err += OUStringToOString(ex.Message, RTL_TEXTENCODING_ASCII_US).getStr();
        throw std::runtime_error(err);
    }
    catch (std::exception& ex)
    {
        throw std::runtime_error(ex.what());
    }
    catch (...)
    {
        throw std::runtime_error("LibreOfficePrivate::loadLibreOffice(): unhandled exception");
    }
}

void LibreOfficePrivate::closeLibreOffice()
{
    _xContext.clear();
    _xServiceManager.clear();
    _xComponentLoader.clear();
}

uno::Reference<lang::XComponent> LibreOfficePrivate::loadDocument(const std::string& url)
{
    rtl::OUString uurl = rtl::OUString::fromUtf8(url.c_str());
    uno::Sequence<beans::PropertyValue> loadProps;
    auto xComponent = LibreOfficePrivate::_xComponentLoader->loadComponentFromURL(uurl, "_default", 0, loadProps);
    if (!xComponent.is()) throw std::runtime_error("LibreOfficePrivate: Cannot load component");
    LibreOfficePrivate::setDocument(xComponent);
    return xComponent;
}

void LibreOfficePrivate::setDocument(uno::Reference<lang::XComponent> c)
{
    _document.set(c, uno::UNO_QUERY);
}

std::vector<std::string> LibreOfficePrivate::sheets()
{
    uno::Reference <sheet::XSpreadsheets> xSheets = _document->getSheets();
    auto names = xSheets->getElementNames();
    std::vector<std::string> sheets;
    for (auto& n : names)
    {
        sheets.emplace_back(n.toUtf8().getStr());
    }
    return sheets;
}

void LibreOfficePrivate::setSheet(const std::string &s)
{
    try
    {
        _mutex.lock();
        uno::Reference <sheet::XSpreadsheets> xSheets = _document->getSheets();
        uno::Any rSheet = xSheets->getByName(rtl::OUString::fromUtf8(s.c_str()));
        _sheet.set(rSheet, uno::UNO_QUERY);
    }
    catch (container::NoSuchElementException)
    {
        _mutex.unlock();
        throw Exception_NoSuchSheet {s};
    }
}

void LibreOfficePrivate::addListener(const std::string& sheetName, int x, int y, std::function<void(void)> callback)
{
    _listener.emplace_back(std::thread([=]()
    {
        try
        {
            uno::Reference <sheet::XSpreadsheets> xSheets = _document->getSheets();
            uno::Any rSheet = xSheets->getByName(rtl::OUString::fromUtf8(sheetName.c_str()));
            uno::Reference <sheet::XSpreadsheet> sheet(rSheet, uno::UNO_QUERY);

            rtl::OUString last = sheet->getCellByPosition(x, y)->getFormula();

            while(true)
            {
                using namespace std::chrono_literals;

                std::this_thread::sleep_for(1s);

                rtl::OUString current = sheet->getCellByPosition(x, y)->getFormula();

                if (current != last)
                {
                    last = current;
                    callback();
                }

                if (_close.load()) return;
            }
        }
        catch (...)
        {
        }
    }));
}

void LibreOfficePrivate::closeSheet()
{
    _sheet.clear();
    _mutex.unlock();
}

void LibreOfficePrivate::closeDocument()
{
    _close.store(true);
    _document.clear();

    for (auto& l : _listener) if (l.joinable())
        l.join();
}

uno::Reference<container::XEnumeration> LibreOfficePrivate::getDocumentsEnumeration()
{
    return _xComponentLoader->getComponents()->createEnumeration();
}

double LibreOfficePrivate::getValue(int x, int y)
{
    return _sheet->getCellByPosition(x, y)->getValue();
}

std::string LibreOfficePrivate::getFormula(int x, int y)
{
    return _sheet->getCellByPosition(x, y)->getFormula().toUtf8().getStr();
}

std::time_t LibreOfficePrivate::getDate(int x, int y)
{
    // 1 is 02/01/1904, 24107 is 01/01/1970

    return (static_cast<long>(getValue(x, y)) - 24107)*24*3600;
}

void LibreOfficePrivate::setValue(int x, int y, double value)
{
    _sheet->getCellByPosition(x, y)->setValue(value);
}

void LibreOfficePrivate::setFormula(int x, int y, const std::string& value)
{
    _sheet->getCellByPosition(x, y)->setFormula(rtl::OUString::createFromAscii(value.c_str()));
}

void LibreOfficePrivate::clearCell(int x, int y)
{
    _sheet->getCellByPosition(x, y)->setFormula("");
}
