#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QChart>
#include <QChartView>
#include <QCheckBox>
#include <QClipboard>
#include <QFileDialog>
#include <QHeaderView>
#include <QMessageBox>
#include <QMimeData>
#include <QMdiSubWindow>
#include <QTableView>

#include "htmlCreator.h"
#include "LibreOffice.h"

#include "reports/Initial_Consumptions.h"
#include "reports/Initial_Energies.h"
#include "reports/Scenario_ConsumptionPerEnergy.h"
#include "reports/Scenario_ConsumptionPerSourcePerBuilding.h"
#include "reports/Scn_DailyMaximums.h"
#include "reports/Scn_DetailedCharges.h"
#include "reports/Scenario_LDC.h"
#include "reports/Scenario_LocalInvestments.h"
#include "reports/Scenario_SummaryPerBuilding.h"
#include "reports/Cmp_ChargesByBuilding.h"
#include "reports/Cmp_CumulativeCosts.h"
#include "reports/Cmp_DetailedChargesByScenario.h"
#include "reports/Cmp_EnvironmentalImpacts.h"
#include "reports/Cmp_Investments.h"


enum MdiWindowType
{
    MDI_CHART = 100,
    MDI_TABLE = 200
};

/////////////////////////////
/// Main functions
/////////////////////////////

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _graphicTable = new GraphicTable;
    ui->dock_tools->setEnabled(false);

    ui->dailymaximums_year->addItem("2011", unsigned(0));
    ui->dailymaximums_year->addItem("2012", unsigned(1));
    ui->dailymaximums_year->addItem("2013", unsigned(2));

    _statusIcon = new QLabel();
    _statusMessage = new QLabel();
    statusBar()->addWidget(_statusIcon);
    statusBar()->addWidget(_statusMessage);
    _statusError.load(":/icons/icons/dialog-error.png");
    _statusInfo.load(":/icons/icons/dialog-information.png");

    LibreOffice::loadLibreOffice();

    _documentsWatcher = std::thread(&MainWindow::documentsWatcher, this);

    connect(this, &MainWindow::documentClosed,    this, &MainWindow::closeDocument,       Qt::QueuedConnection);
    connect(this, &MainWindow::documentsModified, this ,&MainWindow::updateDocumentsList, Qt::QueuedConnection);

    connect(&_simulation, &Simulation::updated,        this, &MainWindow::redrawGraphic,        Qt::QueuedConnection);
    connect(&_simulation, &Simulation::loadingError,   this, &MainWindow::informLoadingError,   Qt::QueuedConnection);
    connect(&_simulation, &Simulation::loadingSuccess, this, &MainWindow::informLoadingSuccess, Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete _graphicTable;
}

void MainWindow::documentsWatcher()
{
    using namespace std::chrono_literals;

    std::vector<Document> oldFiles;

    while (!_close.load())
    {
        try
        {
            auto files = LibreOffice::documents();
            if (files != oldFiles)
            {
                emit documentsModified();
                oldFiles = files;
            }
        }
        catch (Exception_DocumentClosed&)
        {
            emit documentClosed();
        }
        catch (...)
        {
        }
        std::this_thread::sleep_for(3s);
    }
}

void MainWindow::updateDocumentsList()
{
    auto files = LibreOffice::documents();
    auto* menu = new QMenu(this);

    for (auto& doc : files)
    {
        menu->addAction(QString(doc.filename.c_str()), [=](){openDocument(doc);});
    }

    if (!menu->isEmpty())
        ui->action_openLOFile->setMenu(menu);
    else ui->action_openLOFile->setMenu(nullptr);
}

void MainWindow::openDocument(Document doc)
{
    if (doc.url.empty()) return;

    try
    {
        LibreOffice::setDocument(doc);

        _simulation.load();
        _simulation.startListeners();

        ui->selectedFile->setText(doc.filename.c_str());
        ui->action_reloadLOFile->setEnabled(true);

        ui->dock_tools->setEnabled(true);

        on_action_clearMdiArea_triggered();
        redrawGraphic();
    }
    catch (std::runtime_error& ex)
    {
        QMessageBox msgBox(this);
        msgBox.setText(ex.what());
        msgBox.exec();
    }
}

void MainWindow::closeDocument()
{
    LibreOffice::cleanup();
    on_action_clearMdiArea_triggered();
    _simulation.clear();
    ui->selectedFile->setText("<i>Pas de fichier sélectionné</i>");
    ui->action_reloadLOFile->setEnabled(false);
    ui->dock_tools->setEnabled(false);
}

void MainWindow::informLoadingError(QString message)
{
    _statusIcon->setPixmap(_statusError);
    _statusMessage->setText(message);
}

void MainWindow::informLoadingSuccess()
{
    _statusIcon->setPixmap(_statusInfo);
    _statusMessage->setText("Chargement terminé avec succès.");
}

void MainWindow::cleanup()
{
    _close.store(true);
    _documentsWatcher.join();
    LibreOffice::cleanup();
    LibreOffice::closeLibreOffice();
    on_action_clearMdiArea_triggered();
    _simulation.clear();
}

void MainWindow::addMdiChart(std::string title, Reports::Report_Chart* report)
{
    auto* chart = new QChartView();
    chart->setChart(report);

    auto* mdiChart = ui->mdiArea->addSubWindow(chart);
    mdiChart->setProperty("type", MDI_CHART);
    mdiChart->setWindowTitle(title.c_str());
    mdiChart->show();
}

void MainWindow::addMdiTable(std::string title, Reports::Report_Model* report)
{
    auto* table = new QTableView();
    table->setModel(report);
    table->resizeColumnsToContents();
    table->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);

    auto* mdiTable = ui->mdiArea->addSubWindow(table);
    mdiTable->setProperty("type", MDI_TABLE);
    mdiTable->setWindowTitle(title.c_str());

    int width {0}, height {0};

    width += table->verticalHeader()->width();
    for (int i = 0 ; i < report->columnCount() ; ++i)
        width += table->columnWidth(i);

    height += table->horizontalHeader()->height();
    for (int i = 0 ; i < report->rowCount(); ++i)
        height += table->rowHeight(i);

    mdiTable->resize(width + 10, height + 32);

    mdiTable->show();
}

void MainWindow::redrawGraphic()
{
    const auto& sc = _simulation.listScenarios();

    ui->tab_scenarios->setEnabled(!sc.empty());
    ui->tab_comparisons->setEnabled(!sc.empty());

    if (sc.empty())
    {
        on_action_clearMdiArea_triggered();
    }
    else if (sc.size() != static_cast<unsigned>(ui->tab_scenarios_scenario->count()))
    {
        auto current = ui->tab_scenarios_scenario->currentText();
        ui->tab_scenarios_scenario->clear();

        auto allCheckboxes = ui->groupBox_scenarios->findChildren<QCheckBox*>();
        for (auto c : allCheckboxes)
        {
            ui->groupBox_scenarios->layout()->removeWidget(c);
            delete c;
        }


        for (auto& s : sc)
        {
            ui->tab_scenarios_scenario->addItem(s.c_str());
            ui->groupBox_scenarios->layout()->addWidget(new QCheckBox(s.c_str()));
        }

        if (std::find_if(sc.begin(), sc.end(), [&](const std::string& s) {return s == current.toStdString();}) == sc.end())
            on_action_clearMdiArea_triggered();
        else
            ui->tab_scenarios_scenario->setCurrentText(current);
    }

    for (auto* w : ui->mdiArea->subWindowList())
    {
        switch (w->property("type").toInt())
        {
        case MDI_CHART:
        {
            auto* view  = dynamic_cast<QChartView*>(w->widget());
            auto* chart = dynamic_cast<Reports::Report_Chart*>(view->chart());
            chart->redraw();
            break;
        }
        case MDI_TABLE:
        {
            auto* view  = dynamic_cast<QTableView*>(w->widget());
            auto* table = dynamic_cast<Reports::Report_Model*>(view->model());
            table->update();
            view->resizeColumnsToContents();
            break;
        }
        }
    }
}

/////////////////////////////
/// Tool bar actions
/////////////////////////////

void MainWindow::on_action_openLOFile_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    "Sélectionner un fichier LibreOffice",
                                                    QDir::homePath(),
                                                    "Tableurs LibreOffice (*.ods, *.fods)");

    if (fileName.isEmpty()) return;

    fileName.insert(0, "file://");

    Document doc;
    doc.url = fileName.toStdString();
    doc.filename = LibreOffice::getFilenameFromUrl(doc.url);

    openDocument(doc);
}

void MainWindow::on_action_reloadLOFile_triggered()
{
    _simulation.clear();
    _simulation.load();

    redrawGraphic();
}

void MainWindow::on_action_clearMdiArea_triggered()
{
    for (auto w : ui->mdiArea->subWindowList())
        ui->mdiArea->removeSubWindow(w);
}

void MainWindow::on_action_toGraphicTable_triggered()
{
    auto* w = ui->mdiArea->currentSubWindow();

    if (w && w->property("type").toInt() == MDI_CHART)
    {
        auto* view  = dynamic_cast<QChartView*>(w->widget());
        auto* chart = dynamic_cast<Reports::Report_Chart*>(view->chart());
        _graphicTable->setGraphic(chart->duplicate());
        _graphicTable->showMaximized();
    }
}

void MainWindow::on_action_copyToClipboard_triggered()
{
    auto* w = ui->mdiArea->currentSubWindow();

    if (w && w->property("type").toInt() == MDI_TABLE)
    {
        auto* view  = dynamic_cast<QTableView*>(w->widget());
        auto* table = dynamic_cast<QStandardItemModel*>(view->model());
        auto html = htmlCreator(table);

        QClipboard *clipboard = QApplication::clipboard();
        auto* data = new QMimeData;
        data->setHtml(html);
        clipboard->setMimeData(data);
    }
}


/////////////////////////////
/// Current situation
/////////////////////////////

void MainWindow::on_initial_consumptions_clicked()
{
    addMdiTable("Consommations initiales",
                new Reports::Initial_Consumptions_Model(_simulation));
}

void MainWindow::on_initial_energies_clicked()
{
    addMdiTable("Énergies",
                new Reports::Initial_Energies_Model());
}


/////////////////////////////
/// Scenarios
/////////////////////////////

void MainWindow::on_scenario_summaryperbuilding_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());

    addMdiTable(currentScenario.name() + " / Résumé par bâtiment",
                new Reports::Scenario_SummaryPerBuilding_Model(currentScenario));
}

void MainWindow::on_scenario_consumptionpersourceperbuilding_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());

    addMdiChart(currentScenario.name() + " / Conso. par source et par bâtiment",
                new Reports::Scenario_ConsumptionPerSourcePerBuilding_Chart(currentScenario));

    addMdiTable(currentScenario.name() + " / Conso. par source et par bâtiment",
                new Reports::Scenario_ConsumptionPerSourcePerBuilding_Model(currentScenario));
}

void MainWindow::on_scenario_consumptionperenergy_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());

    addMdiTable(currentScenario.name() + " / Consommation par énergie",
                new Reports::Scenario_ConsumptionPerEnergy_Model(currentScenario));
}

void MainWindow::on_scn_dailymaximums_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());
    unsigned currentYear = ui->dailymaximums_year->currentData().toUInt();
    auto year = ui->dailymaximums_year->currentText().toStdString();

    addMdiChart(currentScenario.name() + " / Appels de puissance journaliers (" + year + ")",
                new Reports::Scn_DailyMaximums_Chart(currentScenario, currentYear));
}

void MainWindow::on_scenario_ldc_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());

    addMdiChart(currentScenario.name() + " / Monotone de puissance",
                new Reports::Scenario_LDC_Chart(currentScenario, ui->ldc_precision->value()));

    addMdiTable(currentScenario.name() + " / Énergies en entrée réseau",
                new Reports::Scenario_LDC_Model(currentScenario));
}

void MainWindow::on_scenario_localinvestments_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());

    addMdiTable(currentScenario.name() + " / Investissements locaux",
                new Reports::Scenario_LocalInvestments_Model(currentScenario));
}

void MainWindow::on_scn_detailedcharges_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());
    unsigned currentYear = ui->detailedchargesbybuilding_year->value() - 1;
    auto year = intToChars(currentYear + 1);

    addMdiChart(currentScenario.name() + " / Détail des charges (année " + year + ")",
                new Reports::Scn_DetailedCharges_Chart(currentScenario, currentYear));

    addMdiTable(currentScenario.name() + " / Détail des charges (année " + year + ")",
                new Reports::Scn_DetailedCharges_Model(currentScenario, currentYear));
}

void MainWindow::on_scn_globalcost_clicked()
{
    auto& currentScenario = _simulation.getScenario(ui->tab_scenarios_scenario->currentText().toStdString());
    unsigned currentYear = ui->detailedchargesbybuilding_year->value() - 1;
    auto year = intToChars(currentYear + 1);

    addMdiTable(currentScenario.name() + " / Coût global (année" + year + ")",
                new Reports::Scn_GlobalCost_Model(currentScenario, currentYear));
}


/////////////////////////////
/// Comparisons
/////////////////////////////

void MainWindow::on_cmp_environmentalimpacts_clicked()
{
    std::vector<Scenario*> scenarios;

    auto allCheckboxes = ui->groupBox_scenarios->findChildren<QCheckBox*>();
    for (auto c : allCheckboxes)
        if (c->isChecked())
            scenarios.push_back(&_simulation.getScenario(c->text().toStdString()));

    addMdiChart("Impacts environnementaux",
                new Reports::Cmp_EnvironmentalImpacts_Chart(scenarios));

    addMdiTable("Impacts environnementaux",
                new Reports::Cmp_EnvironmentalImpacts_Model(scenarios));
}

void MainWindow::on_cmp_chargesbybuilding_clicked()
{
    std::vector<Scenario*> scenarios;

    auto allCheckboxes = ui->groupBox_scenarios->findChildren<QCheckBox*>();
    for (auto c : allCheckboxes)
        if (c->isChecked())
            scenarios.push_back(&_simulation.getScenario(c->text().toStdString()));

    addMdiChart("Charges par bâtiment",
                new Reports::Cmp_ChargesByBuilding_Chart(scenarios, 0));
}

void MainWindow::on_cmp_investments_clicked()
{
    std::vector<Scenario*> scenarios;

    auto allCheckboxes = ui->groupBox_scenarios->findChildren<QCheckBox*>();
    for (auto c : allCheckboxes)
        if (c->isChecked())
            scenarios.push_back(&_simulation.getScenario(c->text().toStdString()));

    addMdiChart("Investissements",
                new Reports::Cmp_Investments_Chart(scenarios));
}

void MainWindow::on_cmp_cumulativecosts_clicked()
{
    std::vector<Scenario*> scenarios;

    auto allCheckboxes = ui->groupBox_scenarios->findChildren<QCheckBox*>();
    for (auto c : allCheckboxes)
        if (c->isChecked())
            scenarios.push_back(&_simulation.getScenario(c->text().toStdString()));

    addMdiChart("Cumul des dépenses actualisées",
                new Reports::Cmp_CumulativeCosts_Chart(scenarios));
}

void MainWindow::on_cmp_detailedcharges_clicked()
{
    std::vector<Scenario*> scenarios;

    auto allCheckboxes = ui->groupBox_scenarios->findChildren<QCheckBox*>();
    for (auto c : allCheckboxes)
        if (c->isChecked())
            scenarios.push_back(&_simulation.getScenario(c->text().toStdString()));

    addMdiTable("Détails des coûts par scénario",
                new Reports::Cmp_DetailedChargesByScenario_Model(scenarios));
}
