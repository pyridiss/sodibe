#ifndef UNITS_H
#define UNITS_H

#include <cmath>
#include <ratio>

using UnitType = double;

template<int Exp = 0, long Rank = 1>
struct Var
{
    static constexpr int    exp  = Exp;
    static constexpr double rank = Rank;
};

template <typename Distance = Var<>,
          typename Weight = Var<>,
          typename Temperature = Var<>,
          typename Time = Var<>,
          typename Money = Var<>,
          typename Factor = std::ratio<1,1>
         >
class Unit
{
public:
    using Type = Unit<Distance, Weight, Temperature, Time, Money, Factor>;

    template<long Distance2, long Weight2, long Temperature2, long Time2, long Money2>
    using Ranked = Unit<Var<Distance::exp, Distance2>,
                        Var<Weight::exp, Weight2>,
                        Var<Temperature::exp, Temperature2>,
                        Var<Time::exp, Time2>,
                        Var<Money::exp, Money2>,
                        std::ratio<1,1>>;

    using Canonical = Ranked<1, 1, 1, 1, 1>;

public:
    explicit constexpr Unit() = default;
    explicit constexpr Unit(UnitType v) : value(v) {}

    UnitType get() const { return value; }


    /** CONVERSION OPERATORS **/

    template<typename... Args>
    Unit& operator=(const Unit<Args...>& right)
    {
        *this = Type(right);
        return *this;
    }

    template<long Distance2, long Weight2, long Temperature2, long Time2, long Money2>
    explicit operator Ranked<Distance2, Weight2, Temperature2, Time2, Money2>() const
    {
        auto c = toCanonical();
        return Ranked<Distance2, Weight2, Temperature2, Time2, Money2>(
                   c.get() / std::pow(Distance2, Distance::exp)
                           / std::pow(Weight2, Weight::exp)
                           / std::pow(Temperature2, Temperature::exp)
                           / std::pow(Time2, Time::exp)
                           / std::pow(Money2, Money::exp)
        );
    }

    Canonical toCanonical() const
    {
        return Canonical(value * std::pow(Distance::rank, Distance::exp)
                               * std::pow(Weight::rank, Weight::exp)
                               * std::pow(Temperature::rank, Temperature::exp)
                               * std::pow(Time::rank, Time::exp)
                               * std::pow(Money::rank, Money::exp)
                               * Factor::num / Factor::den
        );
    }


    /** USUAL OPERATIONS **/

    template<typename... Args>
    auto operator+(const Unit<Args...>& right) const
    {
        static_assert(are_addible<Type, std::decay_t<decltype(right)>>());

        auto l = toCanonical();
        auto r = right.toCanonical();
        return Canonical(l.get() + r.get());
    }

    template<typename... Args>
    Unit& operator+=(const Unit<Args...>& right)
    {
        *this = Type(*this + right);
        return *this;
    }

    template<typename... Args>
    auto operator-(const Unit<Args...>& right) const
    {
        return operator+(Unit<Args...>(-right.get()));
    }

    template<typename Distance2, typename Weight2, typename Temperature2, typename Time2, typename Money2, typename Factor2>
    auto operator*(const Unit<Distance2, Weight2, Temperature2, Time2, Money2, Factor2>& right) const
    {
        auto l = toCanonical();
        auto r = right.toCanonical();

        return Unit<Var<Distance::exp + Distance2::exp, 1>,
                    Var<Weight::exp + Weight2::exp, 1>,
                    Var<Temperature::exp + Temperature2::exp, 1>,
                    Var<Time::exp + Time2::exp, 1>,
                    Var<Money::exp + Money2::exp, 1>
                   >(l.get() * r.get());
    }

    template<typename Distance2, typename Weight2, typename Temperature2, typename Time2, typename Money2, typename Factor2>
    auto operator/(const Unit<Distance2, Weight2, Temperature2, Time2, Money2, Factor2>& right) const
    {
        auto l = toCanonical();
        auto r = right.toCanonical();

        return Unit<Var<Distance::exp - Distance2::exp, 1>,
                    Var<Weight::exp - Weight2::exp, 1>,
                    Var<Temperature::exp - Temperature2::exp, 1>,
                    Var<Time::exp - Time2::exp, 1>,
                    Var<Money::exp - Money2::exp, 1>
                   >(l.get() / r.get());
    }

    auto operator*(UnitType coeff) const
    {
        return Type(value * coeff);
    }

    auto operator/(UnitType coeff) const
    {
        return Type(value / coeff);
    }


    /**  COMPARISONS **/

    template<typename... Args>
    bool operator<(const Unit<Args...>& right) const
    {
        static_assert(are_addible<Type, std::decay_t<decltype(right)>>());

        return toCanonical().get() < right.toCanonical().get();
    }

    template<typename... Args>
    bool operator>(const Unit<Args...>& right) const
    {
        return right < *this;
    }

private:
    UnitType value {};

private:
    template<typename T, typename U>
    static constexpr bool are_addible()
    {
        return std::is_same_v<typename T::Canonical, typename U::Canonical>;
    }
};

//                                     ---Distance-----  ----Weight------  ---Temperature--  -------Time------  -------Euro------
//                                       (decimeter)       (kilogram)          (kelvin)           (second)            (cent)

using Coefficient               = Unit<Var<>        , Var<>        , Var<>        , Var<>         >;

using Meter                     = Unit<Var< 1,   10>, Var<>        , Var<>        , Var<>         >;
using Meter2                    = Unit<Var< 2,   10>, Var<>        , Var<>        , Var<>         >;
using Meter3                    = Unit<Var< 3,   10>, Var<>        , Var<>        , Var<>         >;
using Liter                     = Unit<Var< 3,    1>, Var<>        , Var<>        , Var<>         >;

using Kilogram                  = Unit<Var<>           , Var< 1,       1>>;

using Watt                      = Unit<Var< 2,      10>, Var< 1,       1>, Var<>           , Var<-3,       1>>;
using Kilowatt                  = Unit<Var< 2,      10>, Var< 1,    1000>, Var<>           , Var<-3,       1>>;

using Kelvin                    = Unit<Var<>        , Var<>        , Var< 1,    1>, Var<>         >;

using Second                    = Unit<Var<>        , Var<>        , Var<>        , Var< 1,     1>>;
using Hour                      = Unit<Var<>        , Var<>        , Var<>        , Var< 1,  3600>>;
using Year                      = Unit<Var<>           , Var<>           , Var<>           , Var< 1, 31536000>>;
using per_Hour                  = Unit<Var<>        , Var<>        , Var<>        , Var<-1,  3600>>;

using KilowattHour              = Unit<Var< 2,      10>, Var< 1, 3600000>, Var<>           , Var<-2,       1>>;
using MegawattHour              = Unit<Var< 2,      10>, Var< 1, 3600000000>, Var<>        , Var<-2,       1>>;
using Liter_per_Day             = Unit<Var< 3,    1>, Var<>        , Var<>        , Var<-1, 86400>>;
using Watt_per_Meter2           = Unit<Var<>           , Var< 1,       1>, Var<>           , Var<-3,       1>>;
using Watt_per_KelvinMeter      = Unit<Var< 1,      10>, Var< 1,       1>, Var<-1,       1>, Var<-3,       1>>;
using Watt_per_KelvinMeter3     = Unit<Var<-1,      10>, Var< 1,       1>, Var<-1,       1>, Var<-3,       1>>;

using Cent                      = Unit<Var<>        , Var<>        , Var<>        , Var<>         , Var< 1, 1>  >;
using Euro                      = Unit<Var<>        , Var<>        , Var<>        , Var<>         , Var< 1, 100>>;
using Euro_per_Kilowatt         = Unit<Var<-2,      10>, Var<-1,    1000>, Var<>           , Var< 3,       1>, Var< 1,     100>>;
using Euro_per_MegawattHour     = Unit<Var<-2,      10>, Var<-1, 3600000000>, Var<>        , Var< 2,       1>, Var< 1,     100>>;

using Kilogram_per_Year         = Unit<Var<>           , Var< 1,       1>, Var<>           , Var<-1, 31536000>>;
using Ton_per_Year              = Unit<Var<>           , Var< 1,    1000>, Var<>           , Var<-1, 31536000>>;
using Kilogram_per_Kilometer    = Unit<Var<-1,   10000>, Var< 1,       1>>;
using Kilogram_per_KilowattHour = Unit<Var<-2,      10>, Var<>           , Var<>           , Var< 2,       1>, Var<>, std::ratio<1,3600000>>;
using Kilogram_per_MegawattHour = Unit<Var<-2,      10>, Var<>           , Var<>           , Var< 2,       1>, Var<>, std::ratio<1,3600000000>>;
using Kilometer_per_Year        = Unit<Var< 1,   10000>, Var<>           , Var<>           , Var<-1, 31536000>>;

#endif // UNITS_H
