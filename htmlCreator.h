#ifndef HTMLCREATOR_H
#define HTMLCREATOR_H

#include <QStandardItemModel>
#include <QString>

enum TableProperties
{
    TABLE_HAS_TOTAL_LINE,   // bool, default 'true'
    TABLE_COLSPAN,          // int,  default '1'
    TABLE_NONE
};

inline QString htmlCreator(QStandardItemModel* table)
{
    const QString borderColor             = "#37A42C";
    const QString evenLineBackgroundColor = "#EEEEEE";
    const QString headerBackgroundColor   = "#77b753";
    const QString headerTextColor         = "#FFFFFF";
    const QString sumBackgroundColor      = "#D8E8C2";

    bool even = false;

    auto totalLine_v = table->data(table->index(0, 0), Qt::UserRole + TABLE_HAS_TOTAL_LINE);
    bool totalLine = (totalLine_v.isValid()) ? totalLine_v.toBool() : true;

    QString html = R"(<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">)"
                   R"(<html>)"
                   R"(    <head>)"
                   R"(        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>)"
                   R"(        <style type="text/css">)"
                   R"(            td {border: none; padding: 0.1cm;})"
                   R"(            table {border: 2pt solid )" + borderColor + R"(;})"
                   R"(        </style>)"
                   R"(    </head>)"
                   R"(    <body>)"
                   R"(        <table width="100%" "cellpadding="4" cellspacing="0" border="2" bordercolor=")" + borderColor + R"(">)";

    for (int i = 0 ; i < table->columnCount() ; ++i)
        html += R"(<col width="1*"/>)";

    html += R"(<tr>)";
    for (int j = 0 ; j < table->columnCount() ; ++j)
    {
        html += R"(<td style="background-color:)" + headerBackgroundColor + R"(;color:)" + headerTextColor
              + R"(;font-weight:bold;text-align:center">)";
        html += table->horizontalHeaderItem(j)->text().toHtmlEscaped();
        html += R"(</td>)";
    }
    html += R"(</tr>)";

    for (int i = 0 ; i < table->rowCount() ; ++i)
    {
        html += R"(<tr style="background-color:)";
        if (i == table->rowCount()-1 && totalLine)
            html += sumBackgroundColor;
        else
            html += (even) ? evenLineBackgroundColor : "#FFFFFF";
        html += R"(">)";

        for (int j = 0 ; j < table->columnCount() ; ++j)
        {
            html += R"(<td )";

            auto colspan_v = table->data(table->index(i, j), Qt::UserRole + TABLE_COLSPAN);
            int colspan = (colspan_v.isValid()) ? colspan_v.toInt() : 1;
            if (colspan > 1)
                html += QString(R"( colspan="%L1" )").arg(colspan);

            html += R"(style=")";

            if (table->item(i, j)->font().bold())   html += R"(font-weight:bold;)";
            if (table->item(i, j)->font().italic()) html += R"(font-style:italic;)";

            if (table->item(i, j)->textAlignment() & Qt::AlignRight)
                html += R"(text-align:right;)";

            if (table->item(i, j)->textAlignment() & Qt::AlignHCenter)
                html += R"(text-align:center;)";

            if (table->item(i, j)->foreground().color() != Qt::black)
                html += R"(color:)" + table->item(i, j)->foreground().color().name() + R"(;)";

            html += R"(">)";

            html += table->item(i, j)->text().toHtmlEscaped();

            html += R"(</td>)";

            if (colspan > 1) j += colspan - 1;
        }
        html += R"(</tr>)";
        even = !even;
    }

    html += R"(</table></body></html>)";

    return html;
}

#endif // HTMLCREATOR_H
