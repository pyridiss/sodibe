#ifndef SIMULATION_H
#define SIMULATION_H

#include <array>
#include <string>
#include <vector>

#include <QObject>

#include "Building.h"
#include "Energy.h"
#include "Scenario.h"


struct TemperatureProfiles
{
    std::unordered_map<std::string, std::array<double, 24>>     days;
    std::unordered_map<std::string, bool>                       dhw;
    std::unordered_map<std::string, std::array<std::string, 7>> weeks;
};


class Simulation : public QObject
{
    Q_OBJECT

public:
    void load();
    void startListeners();

    void addScenario(const std::string& sheet);
    Scenario&                  getScenario  (const std::string& name);
    std::vector<std::string>   listScenarios();
    void                       clear        ();

    const auto& getBuildings() const {
        return buildings;
    }

signals:
    void updated();
    void loadingError(QString);
    void loadingSuccess();

private:
    void update();
    void updateBuildingProfiles(Building& b);
    void clearSpreadsheet();

private:
    std::list<Scenario>   _scenarios {};
    std::vector<Building>    buildings;
    TemperatureProfiles      _profiles    {};
};

#endif // SIMULATION_H
