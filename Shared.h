#ifndef SHARED_H
#define SHARED_H

#include <array>
#include <ctime>
#include <set>
#include <vector>

#include <QColor>

#include "Energy.h"
#include "units.h"


constexpr int SodibeSpreadsheetRequiredVersion = 20200804;

template<typename T, int years = 1>
using TableOfHours = std::array<T, years*8760>;

template<typename T, int years = 1>
using TableOfDays  = std::array<T, years*365>;

using Period = std::pair<std::time_t, std::time_t>;

enum DHWProfile
{
    CollectiveBuilding,
    StorageTankHeater,
    TanklessHeater
};

struct Exception_NoSuchEnergy {std::string name;};

struct Color
{
    QColor color;
    std::string useName;
    std::string realName;
};

class Shared
{
public:
    using DHW_coeff_type = Unit<Var<-1, 10>, Var<1, 86400000>, Var<>, Var<-2, 1>>;

    inline static const Kilogram_per_Kilometer co2EmissionPerKilometer {0.253};
    inline static const Kilogram_per_MegawattHour nuclearWasteFactor   {0.13949};

    inline static const auto symbol_AUTOMOBILE       = QString::fromStdWString(L"\U0001F697");
    inline static const auto symbol_CLOUD            = QString::fromStdWString(L"\U00002601");
    inline static const auto symbol_OIL_DRUM         = QString::fromStdWString(L"\U0001F6E2");
    inline static const auto symbol_RADIOACTIVE_SIGN = QString::fromStdWString(L"\U00002622");

    inline static QString symbolaFont;

    constexpr static unsigned years = 3;
    constexpr static auto     wholeYear = Period();

    static void           load           ();

    static Kelvin          temperature      (unsigned sim_hour);
    static DHW_coeff_type dhw            (DHWProfile use, unsigned hour);
    static Kelvin         soilTemperature(unsigned hour);
    static Watt_per_Meter2 solarRadiation   (unsigned sim_hour);
    static double          solarElevation   (unsigned sim_hour);
    static Watt_per_Meter2 internalHeatGains(bool presence);
    static bool            dateBetween      (unsigned hour, Period period);

    static void setEnergies(std::vector<Energy> energies) {
        _energies = std::move(energies);
        Energy gaia;
        gaia.name = "gaia";
        gaia.category = "Électricité";
        gaia.renewableFactor = 1;
        _energies.push_back(gaia);
    }
    static const Energy& energy(const std::string& name) {
        auto e = std::find_if(_energies.begin(), _energies.end(), [&](const Energy& ee){return ee.name == name;});
        if (e != _energies.end()) return *e;
        throw Exception_NoSuchEnergy{name};
    }
    static const std::vector<Energy>& getEnergies() {
        return _energies;
    }

    static auto        energyCategories()
         -> std::set<std::string>;

    inline static double maintenanceVat    {};
    inline static double maintenanceGrowth {};
    inline static double provisionGrowth   {};
    inline static double discounting       {};

    static void startColorEnum();
    static auto nextColor() -> Color;
    static auto color(const std::string& name) -> QColor;

private:
    inline static std::array<TableOfHours<Kelvin>, years>     _temperatures;
    inline static TableOfHours<Watt_per_Meter2>               _solarRadiation;
    inline static std::array<TableOfHours<DHW_coeff_type>, 3> _hotwaterProfiles;
    inline static TableOfDays<Kelvin>                         _soilTemperature;
    inline static std::vector<Energy>                         _energies;
    inline static unsigned                                    _currentColor {0};
};


template<typename T>
using Sim_TableOfHours = TableOfHours<T, Shared::years>;

template<typename T>
using Sim_TableOfDays  = TableOfDays<T, Shared::years>;

template<typename T>
[[nodiscard]] T addTable(T a, T b)
{
    T sum;
    for(unsigned i = 0 ; i < a.size() ; ++i)
        sum[i] = a[i] + b[i];
    return sum;
}

template<typename T>
[[nodiscard]] auto sumMap(T map)
{
    typename T::mapped_type sum;
    for (auto& i : map)
        sum += i.second;
    return sum;
}

template<typename T>
[[nodiscard]] typename T::value_type yearlyMeanSum(T container)
{
    return std::accumulate(container.begin(), container.end(), typename T::value_type {0}) / Shared::years;
}

std::string intToChars(int value, int stringLength = 0, bool sign = false, bool digitSeparator = false);

#endif // SHARED_H
