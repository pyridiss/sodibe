#ifndef SCENARIO_H
#define SCENARIO_H

#include <array>
#include <map>
#include <mutex>
#include <string>
#include <vector>

#include "Shared.h"
#include "units.h"

#include "BuildingAdaptation.h"


class Building;

class Scenario
{
public:

    struct HeatingSystem
    {
        std::string energy       {};
        double      nominalPower {};
        double      minimumPower {};
        double      efficiency   {};
        double      maxEnergy    {};
    };

    struct ConsumptionPerSourcePerBuilding
    {
        using Item = std::pair<std::string, double>;

        std::vector<Item> networkItems;
        std::vector<Item> losses;
        std::vector<Item> localItems;
    };

    Scenario(std::string sheet, std::vector<Building>* buildings);

    bool enabled() const {
        return _enabled;
    }
    void enable(bool b);

    std::string                         name                    () const;
    std::string                         sheet                   () const;
    void setName(const std::string& name);
    void                                setFirstSystem          (const std::string& name, double nominal, double min, double efficiency, double maxEnergy);
    void                                setSecondSystem         (const std::string& name, double nominal, double min, double efficiency, double maxEnergy);
    void                                setThirdSystem          (const std::string& name, double efficiency);
    void adaptBuilding(int column, const BuildingAdaptation& c);
    void                                clearNetwork            ();
    void                                addNetworkSection       (double size, double coeff);
    void                                setNetworkTemperature   (double flowTemp, double returnTemp);
    void                                setSubstationsEfficiency(double eff);
    void                                setNetworkPeriodOfUse   (std::time_t start, std::time_t stop);

    void setProjectOwner          (const std::string& value);
    void setInvestmentTaxIncl     (double value);
    void setSubsidy               (double value);
    void setVatCompensation       (double value);
    void setMaintenanceCostTaxIncl(double value);
    void setRepairProvisionTaxIncl(double value);
    void setAmortization          (double value);
    void setAnnualInterest        (double value);
    void setInitialFunds          (double value);
    void setLoanAnnuity           (double value);
    void setVatOnPurchases        (bool value);

    void                                updateConsumptions      ();

    auto        networkHeatingPeriod()             const {
        return std::make_pair(_startNetwork, _stopNetwork);
    }
    const auto& buildingsAdaptations()             const {
        return _buildingsAdaptations;
    }
    const auto& projectOwner()                     const {
        return _projectOwner;
    }
    const auto& systems()                          const {
        return _systems;
    }
    auto        investment()           const {
        return _investment;
    }
    auto        subsidy()              const {
        return _subsidy;
    }
    auto        vatCompensation()      const {
        return _vatCompensation;
    }


    /* COSTS */

    auto energyCost      (unsigned year) const
         -> Euro_per_MegawattHour;

    auto detailedEnergyCost(unsigned year) const
         -> std::unordered_map<std::string, Euro_per_MegawattHour>;

    auto subscriptionCost(unsigned year) const
         -> Euro_per_Kilowatt;

    auto detailedSubscriptionCost(unsigned year)const
         -> std::unordered_map<std::string, Euro_per_Kilowatt>;


    /* ANALYZES */

    auto consumptionPerSourcePerBuilding() const
         -> ConsumptionPerSourcePerBuilding;

    auto loadDurationCurve              () const
         -> Sim_TableOfHours<std::array<double, 3>>;

    auto globalConsumptions             () const
         -> std::unordered_map<std::string, KilowattHour>;

    auto dailyMaximums                  () const
         -> Sim_TableOfDays<std::array<double, 3>>;

    auto localInvestments               () const
         -> std::unordered_map<Building*, Euro>;

    auto detailedChargesByBuilding      (unsigned year) const
         -> std::map<Building*, std::unordered_map<std::string, Euro>>;

    auto globalCostByBuilding           (unsigned year) const
         -> std::map<Building*, Euro_per_MegawattHour>;

    auto cumulativeCosts                () const
         -> std::vector<Euro>;

    auto co2Emissions                   () const
         -> Ton_per_Year;

    auto carTravellingEquivalence       () const
         -> Kilometer_per_Year;

    auto nonRenewablesFraction          () const
         -> double;

    auto nuclearWasteProduced           () const
         -> Kilogram_per_Year;

private:
    auto _loadDurationCurve() const
         -> Sim_TableOfHours<std::array<double, 3>>;

    auto _heatLossOfPipes  () const
         -> Sim_TableOfHours<KilowattHour>;

    auto _fixedCharges     (unsigned year) const
         -> std::unordered_map<std::string, Euro>;

    auto _variableCharges  (unsigned year) const
         -> std::unordered_map<std::string, Euro>;

private:

    struct NetWorkSection
    {
        Meter                size                     {};
        Watt_per_KelvinMeter linearHeatTransfertCoeff {};
    };

    bool                         _enabled               {};
    std::string                  _name                  {};
    std::string                  _sheet                 {};
    std::vector<Building>*                   _buildings             {};
    std::vector<NetWorkSection>  _networkSections       {};
    Kelvin                       _networkTemperature    {};
    double                       _substationsEfficiency {};
    std::time_t                  _startNetwork          {};
    std::time_t                  _stopNetwork           {};
    std::array<HeatingSystem, 3> _systems               {};
    Sim_TableOfHours<KilowattHour>           _totalHeatFromNetwork;
    std::vector<BuildingAdaptation> _buildingsAdaptations {};

    std::string                     _projectOwner         {};
    Euro                            _investment           {};
    Euro                            _subsidy              {};
    Euro                            _vatCompensation      {};
    Euro                            _maintenance          {};
    Euro                            _provision            {};
    Euro                            _amortization         {};
    Euro                            _interest             {};
    Euro                            _initialFunds         {};
    Euro                            _loanAnnuity          {};
    bool                            _vatOnPurchases       {};

    mutable std::mutex _mutex {};

};

#endif // SCENARIO_H
