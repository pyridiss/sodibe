#include "Shared.h"

#include <QFile>
#include <QFontDatabase>
#include <QTextStream>

void SolarAzEl(time_t utc_time_point, double Lat, double Lon, double Alt, double* Az, double* El);

void Shared::load()
{
    auto loadFile = [](const char* path, auto& container)
    {
        QFile qfile(path);
        if (!qfile.open(QIODevice::ReadOnly | QIODevice::Text))
            throw std::runtime_error(std::string(path) + " cannot be loaded.");
        QTextStream stream(&qfile);

        for (auto& e : container)
        {
            double d;
            stream >> d;
            e = decltype(e)(d);
        }
    };

    loadFile(":/data/OutsideTemperature_2011", _temperatures[0]);
    loadFile(":/data/OutsideTemperature_2012", _temperatures[1]);
    loadFile(":/data/OutsideTemperature_2013", _temperatures[2]);

    loadFile(":/data/DHW_CollectiveBuilding",  _hotwaterProfiles[CollectiveBuilding]);
    loadFile(":/data/DHW_StorageTankHeater",   _hotwaterProfiles[StorageTankHeater]);
    loadFile(":/data/DHW_TanklessHeater",      _hotwaterProfiles[TanklessHeater]);

    loadFile(":/data/SoilTemperature",         _soilTemperature);
    loadFile(":/data/SolarRadiation_2011",     _solarRadiation);

    int id = QFontDatabase::addApplicationFont(":/fonts/fonts/Symbola.ttf");
    symbolaFont = QFontDatabase::applicationFontFamilies(id).at(0);
}

Kelvin Shared::temperature(unsigned sim_hour)
{
    unsigned year = sim_hour / 8760;
    unsigned hour = sim_hour % 8760;
    return _temperatures[year][hour];
}

Watt_per_Meter2 Shared::solarRadiation(unsigned sim_hour)
{
    unsigned hour = sim_hour % 8760;
    return _solarRadiation[hour];
}

double Shared::solarElevation(unsigned sim_hour)
{
    time_t unixTime = 1293836400;  // January 1st, 2011
    unixTime += (sim_hour%8760)*3600;

    double Az, El;

    SolarAzEl(unixTime, 49.773538, 4.72083, 0.148, &Az, &El);  // Place Ducale, Charleville-Mézières

    return std::max(0.0, El / 360.0 * 2.0 * M_PI);
}

Watt_per_Meter2 Shared::internalHeatGains(bool presence)
{
    if (presence) return Watt_per_Meter2(5);
    return Watt_per_Meter2(1);
}

Shared::DHW_coeff_type Shared::dhw(DHWProfile use, unsigned hour)
{
    hour %= 8760;
    return _hotwaterProfiles[use][hour];
}

Kelvin Shared::soilTemperature(unsigned hour)
{
    hour = (hour/24) % 8760;
    return _soilTemperature[hour];
}

bool Shared::dateBetween(unsigned hour, Period period)
{
    if (period == wholeYear) return true;

    hour %= 8760;

    auto hour_begin = 24 * static_cast<unsigned>(std::gmtime(&period.first) ->tm_yday);
    auto hour_end   = 24 * static_cast<unsigned>(std::gmtime(&period.second)->tm_yday);

    if (hour_begin < hour_end)
        return (hour >= hour_begin && hour < hour_end);

    return (hour >= hour_begin || hour < hour_end);
}

auto Shared::energyCategories() -> std::set<std::string>
{
    std::set<std::string> result;

    for (auto& e : _energies)
        result.insert(e.category);

    return result;
}

std::string intToChars(int value, int stringLength, bool sign, bool digitSeparator)
{
    if (value == 0) return std::string(std::max(1, stringLength), '0');

    int length = 0;
    for (int l = abs(value) ; l != 0 ; l /= 10)
        ++length;

    if (digitSeparator) length += (length-1)/3;

    if (value < 0 || sign) ++length;
    std::string res(std::max(length, stringLength), '0');
    int pos = res.size() - 1;

    if (value < 0)
    {
        res[0] = '-';
        value = -value;
    }
    else if (sign) res[0] = '+';

    static constexpr char digits[] = "0123456789";

    int count = 0;
    while (value != 0)
    {
        if (digitSeparator && count == 3)
        {
            res[pos] = ' ';
            --pos;
            count = 0;
        }
        int r = value % 10;
        res[pos] = digits[r];
        value /= 10;
        --pos;
        ++count;
    }

    return res;
}


static const std::vector<Color> colors {
    {{119, 183,  83}, "Diagramme 1",  "forest green3"   },
    {{ 44, 114, 199}, "Diagramme 2",  "skyblue3"        },
    {{232,  82, 144}, "Diagramme 3",  "raspberry pink3" },
    {{242, 155, 104}, "Diagramme 4",  "hot orange3"     },
    {{  0, 179, 119}, "Diagramme 5",  "emerald green3"  },
    {{100,  74, 165}, "Diagramme 6",  "grapeviolet3"    },
    {{232,  87,  82}, "Diagramme 7",  "brick red3"      },
    {{243, 195,   0}, "Diagramme 8",  "sun yellow5"     },
    {{  0, 167, 179}, "Diagramme 9",  "sea blue3"       },
    {{177,  79, 154}, "Diagramme 10", "burgundy purple3"},
    {{143, 107,  50}, "Diagramme 11", "wood brown3"     },
    {{128, 179, 255}, "Scénario 1",   "blue2"           },
    {{255, 191, 128}, "Scénario 2",   "brown orange2"   },
    {{192, 128, 255}, "Scénario 3",   "purple2"         },
    {{255, 128, 128}, "Scénario 4",   "red2"            },
    {{ 55, 164,  44}, "Bois",         "forest green4"   },
    {{  0,  67, 138}, "Fossiles",     "skyblue5"        },
    {{255, 235,  85}, "Électricité",  "sun yellow3"     }
};


void Shared::startColorEnum()
{
    _currentColor = 0;
}

auto Shared::nextColor() -> Color
{
    if (_currentColor < colors.size())
    {
        return colors[_currentColor++];
    }
    return {QColor(), "none", "none"};
}

auto Shared::color(const std::string& name) -> QColor
{
    for (auto& c : colors)
        if (c.useName == name) return c.color;

    return QColor();
}


///
/// Estimation of Solar Elevation & Azimuth
/// Programmed by Kevin Godden
/// Original code and licence :
/// https://www.ridgesolutions.ie/index.php/2020/01/14/c-code-to-estimate-solar-azimuth-and-elevation-given-gps-position-and-time/
///

double julian_day(time_t utc_time_point)
{
    struct tm* tm = gmtime(&utc_time_point);

    double year  = tm->tm_year + 1900;
    double month = tm->tm_mon + 1;
    double day   = tm->tm_mday;
    double hour  = tm->tm_hour;
    double min   = tm->tm_min;
    double sec   = tm->tm_sec;

    if (month <= 2) {
        year -= 1;
        month += 12;
    }

    double jd = floor(365.25*(year + 4716.0)) + floor(30.6001*(month + 1.0)) + 2.0 -
        floor(year / 100.0) + floor(floor(year / 100.0) / 4.0) + day - 1524.5 +
        (hour + min / 60 + sec / 3600) / 24;

    return jd;
}

void SolarAzEl(time_t utc_time_point, double Lat, double Lon, double Alt, double* Az, double* El)
{
    double jd = julian_day(utc_time_point);

    double d = jd - 2451543.5;

    double w = 282.9404 + 4.70935e-5*d;                 // (longitude of perihelion degrees)
    double e = 0.016709 - 1.151e-9*d;                   // (eccentricity)
    double M = fmod(356.0470 + 0.9856002585*d, 360.0);  // (mean anomaly degrees)

    double L = w + M;                                   // (Sun's mean longitude degrees)

    double oblecl = 23.4393 - 3.563e-7*d;               // (Sun's obliquity of the ecliptic)

    // auxiliary angle
    double  E = M + (180 / M_PI)*e*sin(M*(M_PI / 180))*(1 + e*cos(M*(M_PI / 180)));

    // rectangular coordinates in the plane of the ecliptic(x axis toward perhilion)
    double x = cos(E*(M_PI / 180)) - e;
    double y = sin(E*(M_PI / 180))*sqrt(1 - pow(e, 2));

    // find the distance and true anomaly
    double r = sqrt(pow(x,2) + pow(y,2));
    double v = atan2(y, x)*(180 / M_PI);

    // find the longitude of the sun
    double lon = v + w;

    // compute the ecliptic rectangular coordinates
    double xeclip = r*cos(lon*(M_PI / 180));
    double yeclip = r*sin(lon*(M_PI / 180));
    double zeclip = 0.0;

    // rotate these coordinates to equitorial rectangular coordinates
    double xequat = xeclip;
    double yequat = yeclip*cos(oblecl*(M_PI / 180)) + zeclip * sin(oblecl*(M_PI / 180));
    double zequat = yeclip*sin(23.4406*(M_PI / 180)) + zeclip * cos(oblecl*(M_PI / 180));

    // convert equatorial rectangular coordinates to RA and Decl:
    r = sqrt(pow(xequat, 2) + pow(yequat, 2) + pow(zequat, 2)) - (Alt / 149598000);  // roll up the altitude correction
    double RA = atan2(yequat, xequat)*(180 / M_PI);

    double delta = asin(zequat / r)*(180 / M_PI);


    // Get UTC representation of time
    tm *ptm;
    ptm = gmtime(&utc_time_point);
    double UTH = ptm->tm_hour + ptm->tm_min / 60.0 + ptm->tm_sec / 3600.0;

    // Calculate local siderial time
    double GMST0 = fmod(L + 180, 360.0) / 15;
    double SIDTIME = GMST0 + UTH + Lon / 15;

    // Replace RA with hour angle HA
    double HA = (SIDTIME*15 - RA);

    // convert to rectangular coordinate system
    x = cos(HA*(M_PI / 180))*cos(delta*(M_PI / 180));
    y = sin(HA*(M_PI / 180))*cos(delta*(M_PI / 180));
    double z = sin(delta*(M_PI / 180));

    // rotate this along an axis going east - west.
    double xhor = x*cos((90 - Lat)*(M_PI / 180)) - z*sin((90 - Lat)*(M_PI / 180));
    double yhor = y;
    double zhor = x*sin((90 - Lat)*(M_PI / 180)) + z*cos((90 - Lat)*(M_PI / 180));

    // Find the h and AZ
    *Az = atan2(yhor, xhor)*(180 / M_PI) + 180;
    *El = asin(zhor)*(180 / M_PI);
}
