#ifndef ENERGY_H
#define ENERGY_H

#include <string>

#include "units.h"

class Energy
{
public:
    int         row             {};
    std::string name            {};
    Euro_per_MegawattHour price           {};
    double      vat             {};
    std::string category        {};
    double      priceGrowth     {};
    Kilogram_per_KilowattHour co2Factor       {};
    double      fossilFactor    {};
    double      renewableFactor {};
    double      nuclearFactor   {};
};

#endif // ENERGY_H
