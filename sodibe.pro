#-----------------------------
#
# Project created by QtCreator
#
#-----------------------------

QT       += core gui widgets charts svg

TARGET = sodibe
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

DEFINES = UNX GCC LINUX CPPU_ENV=gcc3

INCLUDEPATH += /usr/lib/libreoffice/sdk/include/

LIBS += -L/usr/lib/libreoffice/sdk/lib -L/usr/lib/libreoffice/program
LIBS += -luno_cppuhelpergcc3 -luno_cppu -luno_salhelpergcc3 -luno_sal

QMAKE_CXXFLAGS += -O2
QMAKE_LFLAGS += -Wl,--allow-shlib-undefined -Wl,-export-dynamic -Wl,-z,defs -Wl,--no-whole-archive

CONFIG += c++17

SOURCES += \
    main.cpp \
    MainWindow.cpp \
    ColorPicker.cpp \
    GraphicTable.cpp \
    LibreOffice.cpp \
    LibreOfficePrivate.cpp \
    Building.cpp \
    BuildingAdaptation.cpp \
    Scenario.cpp \
    Simulation.cpp \
    reports/Cmp_ChargesByBuilding.cpp \
    reports/Cmp_CumulativeCosts.cpp \
    reports/Cmp_DetailedChargesByScenario.cpp \
    reports/Cmp_EnvironmentalImpacts.cpp \
    reports/Cmp_Investments.cpp \
    reports/Initial_Consumptions.cpp \
    reports/Initial_Energies.cpp \
    reports/Report.cpp \
    reports/Scenario_ConsumptionPerEnergy.cpp \
    reports/Scenario_ConsumptionPerSourcePerBuilding.cpp \
    reports/Scn_DailyMaximums.cpp \
    reports/Scn_DetailedCharges.cpp \
    reports/Scenario_LDC.cpp \
    reports/Scenario_LocalInvestments.cpp \
    reports/Scenario_SummaryPerBuilding.cpp \
    Shared.cpp

HEADERS += \
    MainWindow.h \
    ColorPicker.h \
    GraphicTable.h \
    LibreOffice.h \
    LibreOfficePrivate.h \
    Building.h \
    BuildingAdaptation.h \
    Scenario.h \
    Simulation.h \
    Energy.h \
    reports/Cmp_CumulativeCosts.h \
    reports/Cmp_DetailedChargesByScenario.h \
    reports/Cmp_Investments.h \
    reports/Report.h \
    reports/Cmp_ChargesByBuilding.h \
    reports/Cmp_EnvironmentalImpacts.h \
    reports/Initial_Consumptions.h \
    reports/Initial_Energies.h \
    reports/Scenario_ConsumptionPerEnergy.h \
    reports/Scenario_ConsumptionPerSourcePerBuilding.h \
    reports/Scn_DailyMaximums.h \
    reports/Scn_DetailedCharges.h \
    reports/Scenario_LDC.h \
    reports/Scenario_LocalInvestments.h \
    reports/Scenario_SummaryPerBuilding.h \
    Shared.h \
    htmlCreator.h \
    units.h

FORMS += \
    MainWindow.ui \
    GraphicTable.ui

RESOURCES += \
    resources.qrc

DISTFILES +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
