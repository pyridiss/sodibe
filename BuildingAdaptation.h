#ifndef BUILDINGADAPTATION_H
#define BUILDINGADAPTATION_H

#include <string>
#include <unordered_map>

#include "Shared.h"
#include "units.h"

#include "Building.h"

class Scenario;

class BuildingAdaptation
{
public:
    enum System : uint8_t
    {
        LocalSystem,
        Network,
        ElectricHeater,
        None
    };

    bool       enabled                 {};

    System      heatingSystem              {None};
    System      winterDhwSystem            {None};
    System      summerDhwSystem            {None};
    bool        useNetworkHeatingPeriod    {};

    Euro       investment              {};
    Euro       maintenance             {};
    Euro       provision               {};


    Sim_TableOfHours<KilowattHour> heatFromNetwork;
    Sim_TableOfHours<KilowattHour> localConsumptionForHeating;
    Sim_TableOfHours<KilowattHour> localConsumptionForDhw;

    std::array<std::unordered_map<std::string, Euro>, 20> ownerBills;
    std::array<std::unordered_map<std::string, Euro>, 20> occupantBills;

    void setScenario(Scenario* sc);
    void setBuilding(Building* b);

    void setHeatingSystem  (const std::string& str);
    void setWinterDhwSystem(const std::string& str);
    void setSummerDhwSystem(const std::string& str);

    void setNewSystem(const std::string& energy, double inner, double system);

    void updateConsumption();
    void updateBill();

    Building* building() const {
        return _building;
    }

    auto localEnergy() const {
        return createNewBuilding().energy();
    }

    bool hasNewSystems() const;

    auto consumptions       () const
         -> std::unordered_map<std::string, KilowattHour>;

    auto networkSubscription() const
         -> Kilowatt;

    KilowattHour needs() const;

private:
    Building createNewBuilding() const;

private:
    Building* _building;
    Scenario* _scenario;
    Sim_TableOfHours<KilowattHour> localEnergyConsumption;
    Sim_TableOfHours<KilowattHour> electricityConsumption;

    std::string newSystem_energy           {};
    double      newSystem_innerEfficiency  {};
    double      newSystem_systemEfficiency {};
};

#endif // BUILDINGADAPTATION_H
