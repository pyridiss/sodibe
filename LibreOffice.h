#ifndef LIBREOFFICE_H
#define LIBREOFFICE_H

#include <any>
#include <functional>
#include <string>
#include <vector>

class Building;
class Energy;
class Scenario;
class Simulation;
struct TemperatureProfiles;

struct Document
{
    std::string url;
    std::string filename;
    std::any    component;

    bool operator==(const Document& right) const
    {
        return url == right.url;
    }
};

class Exception_DocumentClosed {};
class Exception_BadSpreadsheetVersion {};

class LibreOffice
{
public:
    static void loadLibreOffice();
    static void closeLibreOffice();

    static void                  setDocument(Document& doc);
    static std::vector<Document> documents  ();

    static void startBuildingsListener(std::function<void(void)> callback);
    static void startProfilesListener (std::function<void(void)> callback);
    static void startScenarioListener (std::function<void(void)> callback);
    static void startGeneralListener  (std::function<void(void)> callback);

    struct BuildingListener
    {
        static std::pair<int, int> columns();
    };

    struct ScenarioListener
    {
        static std::string sheet();
    };

    static std::vector<Building> loadAllBuildings       ();
    static Building              loadBuilding           (int column);
    static TemperatureProfiles   loadTemperatureProfiles();
    static void                  updateBuildingsConsumption(const Building& b);
    static void                  clearBuildingsCalculations(std::vector<int> exceptions);

    static void loadSimulation(Simulation& sim);

    static auto scenarios()
                -> std::vector<std::string>;

    static void loadScenario(Scenario &sc);
    static void updateScenarioCalculations(const Scenario& scenario);

    static std::vector<Energy> loadEnergies();

    static void cleanup();

    static std::string getFilenameFromUrl(std::string url);

private:
    static inline Document _current;
};

#endif // LIBREOFFICE_H
