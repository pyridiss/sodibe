#include "Initial_Energies.h"

#include "Energy.h"
#include "Shared.h"

#include "htmlCreator.h"

namespace Reports {

Initial_Energies_Model::Initial_Energies_Model()
{
    setHorizontalHeaderLabels({"Énergie", "Coût TTC au MWh", "Évolution moyenne", "Sources"});

    update();
}

void Initial_Energies_Model::update()
{
    removeRows(0, rowCount());

    for (const auto& e : Shared::getEnergies())
    {
        QList<QStandardItem*> items;
        items.push_back(new QStandardItem(e.name.c_str()));
        items.push_back(new QStandardItem(QString("%L1 € / MWh").arg(Euro_per_MegawattHour(e.price * (1 + e.vat)).get(), 0, 'f', 2)));
        items.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        items.push_back(new QStandardItem(QString("%L1 % / an").arg(100.0 * e.priceGrowth, 0, 'f', 2)));
        items.back()->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        items.push_back(new QStandardItem(QString("??")));
        appendRow(items);
    }

    setData(index(0, 0), false, Qt::UserRole + TABLE_HAS_TOTAL_LINE);
}

}  // namespace Reports
