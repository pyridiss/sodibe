#include "Scenario_ConsumptionPerSourcePerBuilding.h"

#include <QHeaderView>
#include <QPieSeries>
#include <QSettings>

#include "Scenario.h"

namespace Reports {

Scenario_ConsumptionPerSourcePerBuilding_Chart::Scenario_ConsumptionPerSourcePerBuilding_Chart(const Scenario& scenario)
  : _scenario(scenario)
{
    QString title;
    title += "<center>";
    title += _scenario.name().c_str();
    title += " : répartition des consommations par source et par bâtiment</center>";

    setTitle(title);
    setTitleFont(QFont(titleFont().defaultFamily(), 14));
    legend()->setVisible(false);

    redraw();
}

void Scenario_ConsumptionPerSourcePerBuilding_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    auto* series = new QPieSeries();
    series->setName("Bâtiments");
    series->setPieSize(0.8);
    series->setHoleSize(0.65);

    auto data = _scenario.consumptionPerSourcePerBuilding();

    double network {}, losses {}, local {};

    Shared::startColorEnum();

    auto addPeripheralSlice = [&](auto container, double& value){

        auto color = Shared::nextColor().color.darker(150);

        for (auto& i : container)
        {
            color = color.lighter(115);
            auto* slice = new QPieSlice(i.first.c_str(), i.second);
            slice->setLabelVisible(true);
            slice->setColor(color);
            series->append(slice);
            value += i.second;
        }
    };

    addPeripheralSlice(data.networkItems, network);
    addPeripheralSlice(data.losses, losses);
    addPeripheralSlice(data.localItems, local);

    addSeries(series);


    auto* seriesCenter = new QPieSeries();
    seriesCenter->setName("Sources");
    seriesCenter->setPieSize(0.65);

    QFont centerFont("Liberation Sans", 8);
    double sum = network + losses + local;

    Shared::startColorEnum();

    auto addCenterSlice = [&](QString name, double value){

        auto color = Shared::nextColor().color;

        auto* slice = new QPieSlice(name + QString(" - %L1 %").arg(100.0*value/sum, 0, 'f', 1), value);
        slice->setLabelVisible(true);
        slice->setColor(color.darker(150));
        slice->setLabelFont(centerFont);
        slice->setLabelColor(Qt::white);
        if (value/sum < 0.25)
            slice->setLabelPosition(QPieSlice::LabelInsideNormal);
        else
            slice->setLabelPosition(QPieSlice::LabelInsideHorizontal);
        seriesCenter->append(slice);
    };

    addCenterSlice("Conso. réseau",  network);
    addCenterSlice("Pertes",         losses);
    addCenterSlice("Conso. locales", local);

    addSeries(seriesCenter);
}

Scenario_ConsumptionPerSourcePerBuilding_Chart* Scenario_ConsumptionPerSourcePerBuilding_Chart::duplicate()
{
    auto* dup = new Scenario_ConsumptionPerSourcePerBuilding_Chart(_scenario);

    QSettings settings;
    settings.beginGroup("Scn_ConsumptionPerSourcePerBuilding_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Scenario_ConsumptionPerSourcePerBuilding_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Scn_ConsumptionPerSourcePerBuilding_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

Scenario_ConsumptionPerSourcePerBuilding_Model::Scenario_ConsumptionPerSourcePerBuilding_Model(const Scenario& scenario)
  : _scenario(scenario)
{
    setHorizontalHeaderLabels({"Poste de consommation", "Consommation", "Pourcentage"});

    update();
}

void Scenario_ConsumptionPerSourcePerBuilding_Model::update()
{
    removeRows(0, rowCount());

    auto data = _scenario.consumptionPerSourcePerBuilding();

    double total {};
    for (auto& i : data.networkItems) total += i.second;
    for (auto& i : data.losses)       total += i.second;
    for (auto& i : data.localItems)   total += i.second;

    auto append = [&](QString name, auto container){

        double t {};
        for (auto& i : container) t += i.second;

        QList<QStandardItem*> title;
        title.push_back(new QStandardItem(name));
        title.push_back(new QStandardItem(QString("%L1 kWh").arg(t, 0, 'f', 0)));
        title.push_back(new QStandardItem(QString("%L1 %").arg(100 * t / total, 0, 'f', 1)));
        title[1]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        title[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

        for (auto& i : title)
            i->setFont(QFont(i->font().family(), i->font().pointSize(), QFont::Bold, true));

        appendRow(title);

        for (auto& d : container)
        {
            QList<QStandardItem*> items;
            items.push_back(new QStandardItem(QString("  > ") + d.first.c_str()));
            items.push_back(new QStandardItem(QString("%L1 kWh").arg(d.second, 0, 'f', 0)));
            items.push_back(new QStandardItem(QString("%L1 %").arg(100.0 * d.second / t, 0, 'f', 1)));
            items[1]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            items[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            appendRow(items);
        }

    };

    append("Consommations réseau", data.networkItems);
    append("Pertes systèmes", data.losses);
    append("Consommations locales", data.localItems);


    QList<QStandardItem*> sum;
    sum.push_back(new QStandardItem("Totaux :"));
    sum.push_back(new QStandardItem(QString("%L1 kWh").arg(total, 0, 'f', 0)));
    sum.push_back(new QStandardItem("100,0 %"));
    sum[1]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    sum[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

    for (auto& i : sum)
    {
        i->setFont(QFont(i->font().family(), i->font().pointSize(), QFont::Bold));
    }

    appendRow(sum);
}

}  // namespace Reports
