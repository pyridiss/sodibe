#ifndef SCENARIO_CONSUMPTIONPERSOURCEPERBUILDING_H
#define SCENARIO_CONSUMPTIONPERSOURCEPERBUILDING_H

#include "Report.h"


class Scenario;

namespace Reports {

class Scenario_ConsumptionPerSourcePerBuilding_Chart : public Report_Chart
{
public:
    explicit Scenario_ConsumptionPerSourcePerBuilding_Chart(const Scenario& scenario);

    void redraw() override;
    Scenario_ConsumptionPerSourcePerBuilding_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const Scenario& _scenario;
};

class Scenario_ConsumptionPerSourcePerBuilding_Model : public Report_Model
{
public:
    explicit Scenario_ConsumptionPerSourcePerBuilding_Model(const Scenario& scenario);

    void update() override;

private:
    const Scenario& _scenario;
};

}  // namespace Reports

#endif  // SCENARIO_CONSUMPTIONPERSOURCEPERBUILDING_H
