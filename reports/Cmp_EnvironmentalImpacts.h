#ifndef CMP_ENVIRONMENTALIMPACTS_H
#define CMP_ENVIRONMENTALIMPACTS_H

#include "Report.h"

class Scenario;

namespace Reports {

class Cmp_EnvironmentalImpacts_Chart : public Report_Chart
{
public:
    Cmp_EnvironmentalImpacts_Chart(std::vector<Scenario*> scenarios);

    void redraw() override;
    Cmp_EnvironmentalImpacts_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const std::vector<Scenario*> _scenarios;
    QBarCategoryAxis *yAxis;
};

class Cmp_EnvironmentalImpacts_Model : public Report_Model
{
public:
    Cmp_EnvironmentalImpacts_Model(std::vector<Scenario*> scenarios);

    void update() override;

private:
    const std::vector<Scenario*> _scenarios;
};

}  // namespace Reports

#endif // CMP_ENVIRONMENTALIMPACTS_H
