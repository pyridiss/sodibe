#include "Initial_Consumptions.h"

#include "Building.h"
#include "Simulation.h"

namespace Reports {

Initial_Consumptions_Model::Initial_Consumptions_Model(const Simulation& simulation)
  : _simulation(simulation)
{
    setHorizontalHeaderLabels({"Bâtiment", "Surface", "Besoins de chauffage", "Besoins d'ECS",
                               "Puissance estimée", "Énergie actuelle", "Consommations actuelles"});

    update();
}

void Initial_Consumptions_Model::update()
{
    removeRows(0, rowCount());

    auto buildings = _simulation.getBuildings();

    Meter2       totalSurface {};
    MegawattHour totalHeatNeeds {}, totalDhwNeeds {}, totalConsumptions {};

    for (unsigned u = 0 ; u < buildings.size() ; ++u)
    {
        const Building& b = buildings[u];

        auto heat = b.heatNeeds(b.heatingPeriod());
        auto dhw  = b.dhwNeeds();

        auto heatNeeds    = MegawattHour(yearlyMeanSum(heat));
        auto dhwNeeds     = MegawattHour(yearlyMeanSum(dhw));
        auto consumptions = MegawattHour(b.currentHeatConsumption() + b.currentWinterDhwConsumption()
                                         + b.currentSummerDhwConsumption());

        totalSurface      += b.surface();
        totalHeatNeeds    += heatNeeds;
        totalDhwNeeds     += dhwNeeds;
        totalConsumptions += consumptions;

        auto i = static_cast<int>(u);
        setItem(i, 0, new QStandardItem(b.name().c_str()));
        setItem(i, 1, new QStandardItem(QString("%L1 m²") .arg(b.surface().get())));
        item(i, 1)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        setItem(i, 2, new QStandardItem(QString("%L1 MWh").arg(heatNeeds.get(),          0, 'f', 1)));
        item(i, 2)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        setItem(i, 3, new QStandardItem(QString("%L1 MWh").arg(dhwNeeds.get(),           0, 'f', 1)));
        item(i, 3)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        setItem(i, 4, new QStandardItem(QString("%L1 kW") .arg(b.estimatedPower().get(), 0, 'f', 0)));
        item(i, 4)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        setItem(i, 5, new QStandardItem(QString(b.heatingEnergy().c_str())));
        setItem(i, 6, new QStandardItem(QString("%L1 MWh").arg(consumptions.get(),       0, 'f', 1)));
        item(i, 6)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    }

    auto total = static_cast<int>(buildings.size());
    setItem(total, 0, new QStandardItem("Total"));
    setItem(total, 1, new QStandardItem(QString("%L1 m²") .arg(static_cast<int>(totalSurface.get()))));
    item(total, 1)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    setItem(total, 2, new QStandardItem(QString("%L1 MWh").arg(totalHeatNeeds.get(),    0, 'f', 1)));
    item(total, 2)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    setItem(total, 3, new QStandardItem(QString("%L1 MWh").arg(totalDhwNeeds.get(),     0, 'f', 1)));
    item(total, 3)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    setItem(total, 4, new QStandardItem("-"));
    item(total, 4)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    setItem(total, 5, new QStandardItem("-"));
    setItem(total, 6, new QStandardItem(QString("%L1 MWh").arg(totalConsumptions.get(), 0, 'f', 1)));
    item(total, 6)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

    for (auto i = 0 ; i < 7 ; ++i)
    {
        auto it = item(total, i);
        it->setFont(QFont(it->font().family(), it->font().pointSize(), QFont::Bold));
    }
}

}  // namespace Reports
