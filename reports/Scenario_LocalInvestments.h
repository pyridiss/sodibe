#ifndef SCENARIO_LOCALINVESTMENTS_H
#define SCENARIO_LOCALINVESTMENTS_H

#include "Report.h"


class Scenario;

namespace Reports {

class Scenario_LocalInvestments_Model : public Report_Model
{
public:
    Scenario_LocalInvestments_Model(const Scenario& scenario);

    void update() override;

private:
    const Scenario& _scenario;
};

}  // namespace Reports

#endif // SCENARIO_LOCALINVESTMENTS_H
