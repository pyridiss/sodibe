#include "Cmp_ChargesByBuilding.h"

#include <QBarCategoryAxis>
#include <QBarSet>
#include <QBarSeries>
#include <QSettings>
#include <QValueAxis>

#include "Building.h"
#include "Scenario.h"
#include "Shared.h"

#include "htmlCreator.h"

namespace Reports {

Cmp_ChargesByBuilding_Chart::Cmp_ChargesByBuilding_Chart(std::vector<Scenario*> scenarios, int year)
  : _scenarios(scenarios),
    _year(year)
{
    QString title;
    title += "<p style=\"line-height:2px;\"><center>";
    title += "Comparatif des factures par bâtiment<br /><small>Année ";
    title += QString("%L1").arg(year+1);
    title += "</small><br />";
    title += "<span style=\"font-size:8px\"><i>Pour les bâtiments loués, seules les factures du locataire apparaissent<i></span>";
    title += "</center></p>";

    setTitle(title);
    setTitleFont(QFont(titleFont().defaultFamily(), 14));
    legend()->setVisible(true);
    legend()->setReverseMarkers(true);
    legend()->setAlignment(Qt::AlignRight);

    xAxis = new QBarCategoryAxis();
    xAxis->setTitleText("Bâtiments");
    addAxis(xAxis, Qt::AlignBottom);

    yAxis = new QValueAxis();
    yAxis->setTitleText("Coût annuel");
    yAxis->setLabelFormat(QString("%.0f &euro;"));
    addAxis(yAxis, Qt::AlignLeft);

    redraw();
}

void Cmp_ChargesByBuilding_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    struct Data
    {
        std::string scenarioName;
        std::map<Building*, Euro> charges;
    };

    std::vector<Data> data;
    std::set<Building*> allBuildings;


    for (auto* scenario : _scenarios)
    {
        Data d;
        d.scenarioName = scenario->name();

        for (auto& [b, ch] : scenario->detailedChargesByBuilding(_year))
        {
            d.charges[b] = Euro(sumMap(ch));
            allBuildings.emplace(b);
        }
        data.push_back(d);
    }

    // Make sure that all buildings are in all scenarios

    for (auto& d : data)
    {
        for (auto* b : allBuildings)
        d.charges.try_emplace(b);
    }

    auto* series = new QBarSeries();
    series->setName("Scénarios");

    double max {0};

    for (auto& d : data)
    {
        auto* set = new QBarSet(d.scenarioName.c_str());

        for (auto& [building, value] : d.charges)
        {
            set->append(value.get());
            if (value.get() > max) max = value.get();
        }
        series->append(set);
    }

    addSeries(series);

    QStringList categories;
    for (auto& b : allBuildings)
        categories << b->name().c_str();

    xAxis->clear();
    xAxis->append(categories);
    series->attachAxis(xAxis);

    series->attachAxis(yAxis);

    yAxis->setMax(max);
    yAxis->applyNiceNumbers();
}

Cmp_ChargesByBuilding_Chart* Cmp_ChargesByBuilding_Chart::duplicate()
{
    auto* dup = new Cmp_ChargesByBuilding_Chart(_scenarios, _year);

    QSettings settings;
    settings.beginGroup("Cmp_ChargesByBuilding_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Cmp_ChargesByBuilding_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Cmp_ChargesByBuilding_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

} // Reports
