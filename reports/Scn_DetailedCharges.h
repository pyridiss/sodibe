#ifndef SCN_DETAILEDCHARGES_H
#define SCN_DETAILEDCHARGES_H

#include "Report.h"


class Scenario;

namespace Reports {

class Scn_DetailedCharges_Chart : public Report_Chart
{
public:
    explicit Scn_DetailedCharges_Chart(const Scenario& scenario, int year);

    void redraw() override;
    Scn_DetailedCharges_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const Scenario& _scenario;
    const int       _year;
    QBarCategoryAxis* xAxis;
    QValueAxis*       yAxis;

};

class Scn_DetailedCharges_Model : public Report_Model
{
public:
    explicit Scn_DetailedCharges_Model(const Scenario& scenario, int year);

    void update() override;

private:
    const Scenario& _scenario;
    const int       _year;
};

class Scn_GlobalCost_Model : public Report_Model
{
public:
    explicit Scn_GlobalCost_Model(const Scenario& scenario, int year);

    void update() override;

private:
    const Scenario& _scenario;
    const int       _year;
};

}  // namespace Reports

#endif // SCN_DETAILEDCHARGES_H
