#include "Cmp_Investments.h"

#include <QBarCategoryAxis>
#include <QBarSeries>
#include <QBarSet>
#include <QSettings>
#include <QStackedBarSeries>
#include <QValueAxis>

#include "Scenario.h"
#include "Shared.h"

namespace Reports {

Cmp_Investments_Chart::Cmp_Investments_Chart(std::vector<Scenario*> scenarios)
  : _scenarios(scenarios)
{
    setTitle("Investissements");
    setTitleFont(QFont(titleFont().defaultFamily(), 14));

    QStringList categories;
    for (auto* sc : _scenarios)
        categories << sc->name().c_str();

    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->setTitleText("Scénarios");
    axisX->append(categories);
    addAxis(axisX, Qt::AlignBottom);

    yAxis = new QValueAxis();
    yAxis->setTitleText("Montant d'investissement");
    yAxis->setLabelFormat("%.0f &euro;");
    addAxis(yAxis, Qt::AlignLeft);

    redraw();
}

void Cmp_Investments_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    std::set<std::string> actors;
    for (auto& sc : _scenarios)
    {
        if (!sc->projectOwner().empty())
            actors.insert(sc->projectOwner());

        for (auto& [b, c] : sc->localInvestments())
            if (!b->owner().empty()) actors.insert(b->owner());
    }

    QList<QStackedBarSeries*> series;
    for (auto& actor : actors)
    {
        auto * s = new QStackedBarSeries();
        s->setName(actor.c_str());
        series.push_back(s);
    }

    //                 ---Owner---                     --Scenario-  Invst, Subsd, Vat
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<Euro>>> data;

    for (auto* s : series)
    {
        for (auto* sc : _scenarios)
        {
            data[s->name().toStdString()][sc->name()].resize(3);

            if (s->name() == sc->projectOwner().c_str())
            {
                data[sc->projectOwner()][sc->name()][0] += sc->investment() - sc->subsidy() - sc->vatCompensation();
                data[sc->projectOwner()][sc->name()][1] += sc->subsidy();
                data[sc->projectOwner()][sc->name()][2] += sc->vatCompensation();
            }

            auto local = sc->localInvestments();
            for (auto& [b, c] : local)
            {
                if (s->name() == b->owner().c_str())
                    data[b->owner()][sc->name()][0] += c;
            }
        }
    }

    double max {};

    Shared::startColorEnum();

    for (auto* s : series)
    {
        auto* inv = new QBarSet(s->name());
        inv->setColor(Shared::nextColor().color);
        auto* sub = new QBarSet("Subventions");
        sub->setColor(inv->color().lighter(135));
        auto* vat = new QBarSet("FCTVA");
        vat->setColor(sub->color().lighter(135));

        for (auto* sc : _scenarios)
        {
            *inv << data[s->name().toStdString()][sc->name()][0].get();
            *sub << data[s->name().toStdString()][sc->name()][1].get();
            *vat << data[s->name().toStdString()][sc->name()][2].get();

            auto m = std::accumulate(data[s->name().toStdString()][sc->name()].begin(),
                                     data[s->name().toStdString()][sc->name()].end(),
                                     Euro(0)).get();
            max = std::max(max, m);
        }

        s->append(inv);
        s->append(sub);
        s->append(vat);
    }

    for (auto s : series)
        addSeries(s);

    for (auto* s : series)
    {
        s->attachAxis(axes(Qt::Horizontal).first());
        s->attachAxis(axes(Qt::Vertical).first());
    }

    yAxis->setMax(max);
}

Cmp_Investments_Chart* Cmp_Investments_Chart::duplicate()
{
    auto* dup = new Cmp_Investments_Chart(_scenarios);

    QSettings settings;
    settings.beginGroup("Cmp_Investments_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Cmp_Investments_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Cmp_Investments_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

}  // namespace Reports
