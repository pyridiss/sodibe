#ifndef CMP_CHARGESBYBUILDING_H
#define CMP_CHARGESBYBUILDING_H

#include "Report.h"

class Scenario;

namespace Reports {

class Cmp_ChargesByBuilding_Chart : public Report_Chart
{
public:
    Cmp_ChargesByBuilding_Chart(std::vector<Scenario*> scenarios, int year);

    void redraw() override;
    Cmp_ChargesByBuilding_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const std::vector<Scenario*> _scenarios;
    const int                    _year;
    QBarCategoryAxis* xAxis;
    QValueAxis*       yAxis;
};

} // Reports

#endif // CMP_CHARGESBYBUILDING_H
