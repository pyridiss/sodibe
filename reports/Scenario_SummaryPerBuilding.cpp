#include "Scenario_SummaryPerBuilding.h"

#include "Building.h"
#include "Scenario.h"

#include "htmlCreator.h"

namespace Reports {

Scenario_SummaryPerBuilding_Model::Scenario_SummaryPerBuilding_Model(const Scenario& scenario)
  : _scenario(scenario)
{
    setHorizontalHeaderLabels({"Bâtiment", "Solution de chauffage", "Énergie principale", "Consommation (toutes énergies confondues)",
                               "Investissement initial sur le bâtiment", "Facture annuelle pour l'occupant"});

    update();
}

void Scenario_SummaryPerBuilding_Model::update()
{
    removeRows(0, rowCount());

    MegawattHour totalConsumption {};
    Euro totalInvestment {}, totalCost {};

    for (auto& ch : _scenario.buildingsAdaptations()) if (ch.enabled)
    {
        QList<QStandardItem*> items;
        items.push_back(new QStandardItem(ch.building()->name().c_str()));
        const char* system;

        if (ch.heatingSystem == BuildingAdaptation::Network) system = "Réseau de chaleur";
        else if (ch.hasNewSystems()) system = "Nouveau système de chauffage";
        else                         system = "Système actuel";

        items.push_back(new QStandardItem(system));
        items.back()->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        if (ch.heatingSystem == BuildingAdaptation::Network) items.back()->setData(2, Qt::UserRole + TABLE_COLSPAN);

        items.push_back(new QStandardItem((ch.heatingSystem == BuildingAdaptation::Network) ? "-" : ch.localEnergy().c_str()));
        items.back()->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        auto consumption = MegawattHour(sumMap(ch.consumptions()));
        auto cost        = sumMap(ch.occupantBills[0]);

        totalConsumption += consumption;
        totalInvestment  += ch.investment;
        totalCost        += cost;

        items.push_back(new QStandardItem(QString("%L1 MWh").arg(consumption.get(), 0, 'f', 1)));
        items.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        items.push_back(new QStandardItem(QString("%L1 €").arg(ch.investment.get(), 0, 'f', 2)));
        items.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        items.push_back(new QStandardItem(QString("%L1 € / an").arg(cost.get(),     0, 'f', 2)));
        items.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

        appendRow(items);
    }

    QList<QStandardItem*> sum;
    sum.push_back(new QStandardItem("Total :"));
    sum.back()->setData(3, Qt::UserRole + TABLE_COLSPAN);
    sum.push_back(new QStandardItem("-"));
    sum.push_back(new QStandardItem("-"));
    sum.push_back(new QStandardItem(QString("%L1 MWh").arg(totalConsumption.get(), 0, 'f', 1)));
    sum.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    sum.push_back(new QStandardItem(QString("%L1 €").arg(totalInvestment.get(),    0, 'f', 2)));
    sum.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    sum.push_back(new QStandardItem(QString("%L1 € / an").arg(totalCost.get(),     0, 'f', 2)));
    sum.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

    for (auto& i : sum)
    {
        i->setFont(QFont(i->font().family(), i->font().pointSize(), QFont::Bold));
    }

    appendRow(sum);
}

}  // namespace Reports
