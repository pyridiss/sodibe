#include "Cmp_DetailedChargesByScenario.h"

#include "Scenario.h"

namespace Reports {

Cmp_DetailedChargesByScenario_Model::Cmp_DetailedChargesByScenario_Model(std::vector<Scenario*> scenarios)
  : _scenarios {std::move(scenarios)}
{
    update();
}

void Cmp_DetailedChargesByScenario_Model::update()
{
    setHorizontalHeaderItem(0, new QStandardItem("Poste de dépenses"));

    int column {1};

    for (auto& scn : _scenarios)
    {
        auto data = scn->detailedChargesByBuilding(0);

        setHorizontalHeaderItem(column, new QStandardItem(scn->name().c_str()));

        int row {0};

        for (auto& cat : Shared::energyCategories())
        {
            setItem(row, 0, new QStandardItem(cat.c_str()));

            double total {};
            for (auto& b : data)
            {
                for (auto& e : Shared::getEnergies())
                {
                    if (e.category != cat) continue;

                    total += b.second[e.name].get();
                }
            }
            setItem(row, column, new QStandardItem(QString("%L1 €").arg(total, 0, 'f', 2)));
            item(row, column)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ++row;
        }

        auto addOther = [&](const char* set)
        {
            setItem(row, 0, new QStandardItem(set));
            double total {};
            for (auto& b : data)
            {
                total += b.second[set].get();
            }
            setItem(row, column, new QStandardItem(QString("%L1 €").arg(total, 0, 'f', 2)));
            item(row, column)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ++row;
        };

        addOther("Entretien");
        addOther("Provisions pour réparations");
        addOther("Facture de chauffage du locataire");
        addOther("Facture de chauffage / Abonnement");
        addOther("Facture de chauffage / Consommation");

        setItem(row, 0, new QStandardItem("Total"));

        double total {};
        for (auto& b : data)
        {
            for (auto& e : b.second)
            {
                total += e.second.get();
            }
        }
        setItem(row, column, new QStandardItem(QString("%L1 €").arg(total, 0, 'f', 2)));
        item(row, column)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

        ++column;
    }
}

}  // namespace Reports
