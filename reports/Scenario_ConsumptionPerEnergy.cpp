#include "Scenario_ConsumptionPerEnergy.h"

#include "Scenario.h"

namespace Reports {
Scenario_ConsumptionPerEnergy_Model::Scenario_ConsumptionPerEnergy_Model(const Scenario& scenario)
  : _scenario(scenario)
{
    setHorizontalHeaderLabels({"Énergie", "Consommation", "Pourcentage"});

    update();
}

void Scenario_ConsumptionPerEnergy_Model::update()
{
    removeRows(0, rowCount());

    auto data = _scenario.globalConsumptions();

    KilowattHour total {};

    // Sort from the most used energy to the least used
    std::vector<std::pair<std::string, KilowattHour>> elems(data.begin(), data.end());
    std::sort(elems.begin(), elems.end(), [](auto l, auto r){return l.second > r.second;});

    for (auto& i : elems)
    {
        if (i.second.get() < 1) continue;
        if (i.first == "gaia")  continue;

        total += i.second;
    };

    for (auto& i : elems)
    {
        if (i.second.get() < 1) continue;
        if (i.first == "gaia")  continue;

        QList<QStandardItem*> line;
        line.push_back(new QStandardItem(i.first.c_str()));
        line.push_back(new QStandardItem(QString("%L1 kWh").arg(i.second.get(), 0, 'f', 0)));
        line.push_back(new QStandardItem(QString("%L1 %").arg(100 * i.second.get() / total.get(), 0, 'f', 1)));
        line[1]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        line[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

        appendRow(line);
    };

    QList<QStandardItem*> sum;
    sum.push_back(new QStandardItem("Totaux :"));
    sum.push_back(new QStandardItem(QString("%L1 kWh").arg(total.get(), 0, 'f', 0)));
    sum.push_back(new QStandardItem("100,0 %"));
    sum[1]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    sum[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

    for (auto& i : sum)
    {
        i->setFont(QFont(i->font().family(), i->font().pointSize(), QFont::Bold));
    }

    appendRow(sum);
}

}  // namespace Reports
