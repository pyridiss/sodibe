#ifndef REPORT_CURRENTCONSUMPTIONS_H
#define REPORT_CURRENTCONSUMPTIONS_H

#include <vector>

#include "Report.h"

class Simulation;

namespace Reports {

class Initial_Consumptions_Model : public Report_Model
{
public:
    explicit Initial_Consumptions_Model(const Simulation& simulation);

    void update() override;

private:
    const Simulation& _simulation;
};

}  // namespace Reports

#endif // REPORT_CURRENTCONSUMPTIONS_H
