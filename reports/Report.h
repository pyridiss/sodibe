#ifndef REPORT_H
#define REPORT_H

#include <QChart>
#include <QStandardItemModel>

QT_CHARTS_USE_NAMESPACE

class QSettings;

namespace QtCharts {
class QBarCategoryAxis;
class QValueAxis;
}  // namespace QtCharts


namespace Reports {

class Report_Chart : public QChart
{
public:
    virtual void          redraw()    = 0;
    virtual Report_Chart* duplicate() = 0;
    virtual void          saveSettings() = 0;

public:
    void loadCommonSettings(QSettings& settings, Report_Chart* target);
    void saveCommonSettings(QSettings& settings);
};

class Empty_Chart : public Report_Chart
{
public:
    void redraw() override
    {
    }

    Empty_Chart* duplicate() override
    {
        return new Empty_Chart;
    }

    void saveSettings() override
    {
    }
};


class Report_Model : public QStandardItemModel
{
public:
    virtual void update() = 0;
};

class Empty_Model : public Report_Model
{
public:
    void update() override
    {
    }
};

}  // namespace Reports

#endif // REPORT_H
