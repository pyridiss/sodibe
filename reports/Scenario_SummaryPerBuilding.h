#ifndef SCENARIO_SUMMARYPERBUILDING_H
#define SCENARIO_SUMMARYPERBUILDING_H

#include "Report.h"

class Scenario;

namespace Reports {

class Scenario_SummaryPerBuilding_Model : public Report_Model
{
public:
    explicit Scenario_SummaryPerBuilding_Model(const Scenario& scenario);

    void update() override;

private:
    const Scenario& _scenario;
};

}  // namespace Reports

#endif // SCENARIO_SUMMARYPERBUILDING_H
