#include "reports/Report.h"

#include <QSettings>

namespace Reports {

void Report_Chart::loadCommonSettings(QSettings& settings, Report_Chart* target)
{
    QFont newTitleFont(settings.value("titleFont",       titleFont().family())   .toString(),
                       settings.value("titleFontSize",   titleFont().pointSize()).toInt(),
                       settings.value("titleFontBold",   titleFont().weight())   .toInt(),
                       settings.value("titleFontItalic", titleFont().italic())   .toBool());
    target->setTitleFont(newTitleFont);
    target->setTitleBrush(settings.value("titleFontColor", titleBrush()).value<QBrush>());

    target->legend()->setVisible(settings.value("legendShow", true).toBool());
    target->legend()->setAlignment((Qt::AlignmentFlag)settings.value("legendPosition", (int)legend()->alignment()).toInt());
    target->legend()->setReverseMarkers(settings.value("legendReverseMarkers", false).toBool());

    QFont newLegendFont(settings.value("legendFont",       legend()->font().family())   .toString(),
                        settings.value("legendFontSize",   legend()->font().pointSize()).toInt(),
                        settings.value("legendFontBold",   legend()->font().weight())   .toInt(),
                        settings.value("legendFontItalic", legend()->font().italic())   .toBool());
    target->legend()->setFont(newLegendFont);
    target->legend()->setLabelColor(settings.value("legendFontColor", legend()->labelColor()).value<QColor>());

    target->setLocalizeNumbers(settings.value("miscLocalize", false).toBool());
    target->setBackgroundBrush(settings.value("miscBackgroundColor", backgroundBrush()).value<QBrush>());
    target->setPlotAreaBackgroundVisible(settings.value("miscPlotAreaVisible", false).toBool());
    target->setPlotAreaBackgroundBrush(settings.value("miscPlotAreaColor", plotAreaBackgroundBrush()).value<QBrush>());
}

void Report_Chart::saveCommonSettings(QSettings& settings)
{
    settings.setValue("titleFont", titleFont().family());
    settings.setValue("titleFontSize", titleFont().pointSize());
    settings.setValue("titleFontBold", titleFont().weight());
    settings.setValue("titleFontItalic", titleFont().italic());
    settings.setValue("titleFontColor", titleBrush());
    settings.setValue("legendShow", legend()->isVisible());
    settings.setValue("legendPosition", (int)legend()->alignment());
    settings.setValue("legendReverseMarkers", legend()->reverseMarkers());
    settings.setValue("legendFont", legend()->font().family());
    settings.setValue("legendFontSize", legend()->font().pointSize());
    settings.setValue("legendFontBold", legend()->font().weight());
    settings.setValue("legendFontItalic", legend()->font().italic());
    settings.setValue("legendFontColor", legend()->labelColor());
    settings.setValue("miscLocalize", localizeNumbers());
    settings.setValue("miscBackgroundColor", backgroundBrush());
    settings.setValue("miscPlotAreaVisible", isPlotAreaBackgroundVisible());
    settings.setValue("miscPlotAreaColor", plotAreaBackgroundBrush());
}

}  // namespace Reports
