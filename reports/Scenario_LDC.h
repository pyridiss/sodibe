#ifndef CHART_LDC_H
#define CHART_LDC_H

#include "Report.h"

class Scenario;

namespace Reports {

class Scenario_LDC_Chart : public Report_Chart
{
public:
    explicit Scenario_LDC_Chart(const Scenario& scenario, int precision);

    void redraw() override;
    Scenario_LDC_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const Scenario& _scenario;
    const int       _precision;
    QValueAxis*     xAxis;
    QValueAxis*     yAxis;
};

class Scenario_LDC_Model : public Report_Model
{
public:
    explicit Scenario_LDC_Model(const Scenario& scenario);

    void update() override;

private:
    const Scenario& _scenario;
};

}  // namespace Reports

#endif // CHART_LDC_H
