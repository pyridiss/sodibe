#ifndef SCN_DAILYMAXIMUMS_H
#define SCN_DAILYMAXIMUMS_H

#include "Report.h"

class Scenario;

namespace Reports {

class Scn_DailyMaximums_Chart : public Report_Chart
{
public:
    explicit Scn_DailyMaximums_Chart(const Scenario& scenario, unsigned year);

    void redraw() override;
    Scn_DailyMaximums_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const Scenario& _scenario;
    const unsigned  _year;
    QBarCategoryAxis* xAxis;
    QValueAxis*       yAxis;
};

}  // namespace Reports

#endif // SCN_DAILYMAXIMUMS_H
