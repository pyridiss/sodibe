#include "Scenario_LDC.h"

#include <QHeaderView>
#include <QLineSeries>
#include <QSettings>
#include <QValueAxis>

#include "Shared.h"

#include "Scenario.h"

namespace Reports {

Scenario_LDC_Chart::Scenario_LDC_Chart(const Scenario& scenario, int precision)
  : _scenario(scenario),
    _precision(precision)
{
    QString title = QString(scenario.name().c_str()) + " : monotone de puissance appelée sur le réseau";

    setTitle(title);
    setTitleFont(QFont(titleFont().defaultFamily(), 14));
    legend()->setReverseMarkers(true);

    xAxis = new QValueAxis;
    xAxis->setRange(0, 8760);
    xAxis->setTickCount(6+1);
    xAxis->setLabelFormat("%d h");
    xAxis->setTitleText("Nombre d'heures");
    addAxis(xAxis, Qt::AlignBottom);

    yAxis = new QValueAxis;
    yAxis->setTitleText("Puissance");
    addAxis(yAxis, Qt::AlignLeft);

    redraw();
}

void Scenario_LDC_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    auto data = _scenario.loadDurationCurve();

    std::array<QLineSeries*, 3> ldc;

    Shared::startColorEnum();

    for (unsigned u = 0 ; u < ldc.size() ; ++u)
    {
        ldc[u] = new QLineSeries;
        ldc[u]->setName(_scenario.systems()[u].energy.c_str());
        ldc[u]->setPen(QPen(Shared::nextColor().color, 2, Qt::SolidLine));
    }

    unsigned pr = static_cast<unsigned>(std::max(1, 100 - _precision * 10));
    std::vector<std::array<double, 3>> smoothData (data.size() / pr);

    for (unsigned i = 0 ; i < data.size() / pr ; ++i)
    {
        for (unsigned j = 0 ; j < pr ; ++j)
        {
            smoothData[i][0] += data[pr*i+j][0] / pr;
            smoothData[i][1] += data[pr*i+j][1] / pr;
            smoothData[i][2] += data[pr*i+j][2] / pr;
        }
    }

    auto first = data[0];
    smoothData.emplace(smoothData.begin(), first);

    auto last = data.back();
    smoothData.emplace_back(last);

    for (unsigned x = 0 ; x < smoothData.size() ; ++x)
    {
        double xCoordinate = 8760 * x / smoothData.size();
        ldc[0]->append(xCoordinate, (smoothData[x][0]));
        ldc[1]->append(xCoordinate, (smoothData[x][0]+smoothData[x][1]));
        ldc[2]->append(xCoordinate, (smoothData[x][0]+smoothData[x][1]+smoothData[x][2]));
    }


    double maxPower {0};

    for (auto& x : data)
        maxPower = std::max(maxPower, x[0]+x[1]+x[2]);

    if (maxPower > 100) maxPower = std::ceil(maxPower/100)*100;
    else maxPower = std::ceil(maxPower/10)*10;

    yAxis->setRange(0, maxPower);
    yAxis->setTickCount(10+1);
    yAxis->setLabelFormat("%d kW");

    for (auto line = ldc.rbegin() ; line != ldc.rend() ; ++line)
    {
        addSeries(*line);
        (*line)->attachAxis(xAxis);
        (*line)->attachAxis(yAxis);
    }
}

Scenario_LDC_Chart* Scenario_LDC_Chart::duplicate()
{
    auto* dup = new Scenario_LDC_Chart(_scenario, _precision);

    QSettings settings;
    settings.beginGroup("Scn_LDC_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Scenario_LDC_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Scn_LDC_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

Scenario_LDC_Model::Scenario_LDC_Model(const Scenario& scenario)
  : _scenario(scenario)
{
    setHorizontalHeaderLabels({"Énergie", "Livraison au réseau", "Taux de couverture", "Puissance max. appelée"});
    setVerticalHeaderLabels({"1", "2", "3"});

    update();
}

void Scenario_LDC_Model::update()
{
    removeRows(0, rowCount());

    auto data = _scenario.loadDurationCurve();

    std::array energy {0.0, 0.0, 0.0};
    std::array power  {0.0, 0.0, 0.0};

    Shared::startColorEnum();

    for (auto& x : data)
    {
        for (unsigned u = 0 ; u < 3 ; ++u)
        {
            energy[u] += 1.0/3.0 * x[u];
            power [u]  = std::max(power[u], x[u]);
        }
    }

    for (unsigned u = 0 ; u < 3 ; ++u)
    {
        int i = static_cast<int>(u);
        auto color = Shared::nextColor().color;

        setItem(i, 0, new QStandardItem(QString(_scenario.systems()[u].energy.c_str())));
        setItem(i, 1, new QStandardItem(QString("%L1 kWh").arg(energy[u], 0, 'f', 0)));
        setItem(i, 2, new QStandardItem(QString("%L1 %").arg(100.0*energy[u]/(energy[0]+energy[1]+energy[2]), 0, 'f', 1)));
        setItem(i, 3, new QStandardItem(QString("%L1 kW").arg(power[u], 0, 'f', 0)));

        for (int j = 0 ; j < 4 ; ++j)
        {
            if (j != 0) item(i, j)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            item(i, j)->setForeground(QBrush(color));
        }
    }

    QList<QStandardItem*> sum;
    sum.push_back(new QStandardItem("Total :"));
    sum.push_back(new QStandardItem(QString("%L1 kWh").arg(energy[0] + energy[1] + energy[2], 0, 'f', 0)));
    sum.push_back(new QStandardItem("100,0 %"));
    sum.push_back(new QStandardItem("-"));
    sum[1]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    sum[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
    sum[3]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

    for (auto& i : sum)
    {
        i->setFont(QFont(i->font().family(), i->font().pointSize(), QFont::Bold));
    }

    appendRow(sum);

}

}  // namespace Reports
