#ifndef CMP_DETAILEDCHARGESBYSCENARIO_MODEL_H
#define CMP_DETAILEDCHARGESBYSCENARIO_MODEL_H

#include "Report.h"

class Scenario;

namespace Reports {

class Cmp_DetailedChargesByScenario_Model : public Report_Model
{
public:
    Cmp_DetailedChargesByScenario_Model(std::vector<Scenario*> scenarios);

    void update() override;

private:
    const std::vector<Scenario*> _scenarios;
};

}  // namespace Report

#endif // CMP_DETAILEDCHARGESBYSCENARIO_MODEL_H
