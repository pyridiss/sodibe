#include "Scn_DetailedCharges.h"

#include <QBarCategoryAxis>
#include <QBarSet>
#include <QSettings>
#include <QStackedBarSeries>
#include <QValueAxis>

#include "Building.h"
#include "Scenario.h"

#include "htmlCreator.h"

namespace Reports {

Scn_DetailedCharges_Chart::Scn_DetailedCharges_Chart(const Scenario& scenario, int year)
  : _scenario(scenario),
    _year(year)
{
    QString title;
    title += "<p style=\"line-height:2px;\"><center>";
    title += scenario.name().c_str();
    title += " : détail des factures par bâtiment<br /><small>Année ";
    title += QString("%L1").arg(year+1);
    title += "</small><br />";
    title += "<span style=\"font-size:8px\"><i>Pour les bâtiments loués, seules les factures du locataire apparaissent<i></span>";
    title += "</center></p>";

    setTitle(title);
    setTitleFont(QFont(titleFont().defaultFamily(), 14));

    legend()->setVisible(true);
    legend()->setReverseMarkers(true);
    legend()->setAlignment(Qt::AlignRight);

    xAxis = new QBarCategoryAxis();
    xAxis->setTitleText("Bâtiment");
    addAxis(xAxis, Qt::AlignBottom);

    yAxis = new QValueAxis();
    yAxis->setTickCount(11);
    yAxis->setTitleText("Coût annuel");
    yAxis->setLabelFormat(QString("%.0f &euro;"));
    addAxis(yAxis, Qt::AlignLeft);

    redraw();
}

void Scn_DetailedCharges_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    auto data = _scenario.detailedChargesByBuilding(_year);

    QStackedBarSeries *series = new QStackedBarSeries();
    series->setName("Série principale");

    std::vector<QBarSet*> sets;

    for (auto& cat : Shared::energyCategories())
    {
        sets.push_back(new QBarSet(cat.c_str()));

        for (auto& b : data)
        {
            double total {};
            for (auto& e : Shared::getEnergies())
            {
                if (e.category != cat) continue;

                total += b.second[e.name].get();
            }
            sets.back()->append(total);
        }

        if (cat == "Combustible Bois")    sets.back()->setColor(Shared::color("Bois"));
        if (cat == "Combustible fossile") sets.back()->setColor(Shared::color("Fossiles"));
        if (cat == "Électricité")         sets.back()->setColor(Shared::color("Électricité"));
    }

    auto addOther = [&](const char* set)
    {
        sets.push_back(new QBarSet(set));
        for (auto& b : data)
            sets.back()->append(b.second[set].get());
    };

    addOther("Entretien");
    sets.back()->setColor(QColor(242, 155, 104));
    addOther("Provisions pour réparations");
    sets.back()->setColor(QColor(143, 107, 50));

    addOther("Facture de chauffage du locataire");
    sets.back()->setColor(QColor(191, 0, 0));

    addOther("Facture de chauffage / Abonnement");
    sets.back()->setColor(QColor(177, 76, 154));
    addOther("Facture de chauffage / Consommation");
    sets.back()->setColor(QColor(100, 74, 155));

    for (auto s : sets)
        series->append(s);

    addSeries(series);

    QStringList categories;
    for (auto& b : data)
        categories << b.first->name().c_str();

    xAxis->clear();
    xAxis->append(categories);

    double max {0};
    for (int i = 0 ; i < categories.count() ; ++i)
    {
        double currentValue{0};
        for (auto& s : sets)
            currentValue += s->at(i);
        if (currentValue > max) max = currentValue;
    }

    series->attachAxis(xAxis);
    series->attachAxis(yAxis);

    yAxis->setMax(max);
    yAxis->applyNiceNumbers();
}

Scn_DetailedCharges_Chart* Scn_DetailedCharges_Chart::duplicate()
{
    auto* dup = new Scn_DetailedCharges_Chart(_scenario, _year);

    QSettings settings;
    settings.beginGroup("Scn_DetailedCharges_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Scn_DetailedCharges_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Scn_DetailedCharges_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

Scn_DetailedCharges_Model::Scn_DetailedCharges_Model(const Scenario& scenario, int year)
  : _scenario(scenario),
    _year(year)
{
    update();
}

void Scn_DetailedCharges_Model::update()
{
    auto data = _scenario.detailedChargesByBuilding(_year);

    setHorizontalHeaderItem(0, new QStandardItem("Poste de dépenses"));

    int count {1};
    for (auto& i : data)
    {
        setHorizontalHeaderItem(count, new QStandardItem(i.first->name().c_str()));
        ++count;
    }

    int row {0};

    for (auto& cat : Shared::energyCategories())
    {
        setItem(row, 0, new QStandardItem(cat.c_str()));

        int col {1};
        for (auto& b : data)
        {
            double total {};
            for (auto& e : Shared::getEnergies())
            {
                if (e.category != cat) continue;

                total += b.second[e.name].get();
            }
            setItem(row, col, new QStandardItem(QString("%L1 €").arg(total, 0, 'f', 2)));
            item(row, col)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ++col;
        }
        ++row;
    }

    auto addOther = [&](const char* set)
    {
        setItem(row, 0, new QStandardItem(set));
        int col {1};
        for (auto& b : data)
        {
            setItem(row, col, new QStandardItem(QString("%L1 €").arg(b.second[set].get(), 0, 'f', 2)));
            item(row, col)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ++col;
        }
        ++row;
    };

    addOther("Entretien");
    addOther("Provisions pour réparations");
    addOther("Facture de chauffage du locataire");
    addOther("Facture de chauffage / Abonnement");
    addOther("Facture de chauffage / Consommation");

    setItem(row, 0, new QStandardItem("Total"));

    int col {1};
    for (auto& b : data)
    {
        double total {};

        for (auto& e : b.second)
        {
            total += e.second.get();
        }
        setItem(row, col, new QStandardItem(QString("%L1 €").arg(total, 0, 'f', 2)));
        item(row, col)->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        ++col;
    }
}

Scn_GlobalCost_Model::Scn_GlobalCost_Model(const Scenario& scenario, int year)
  : _scenario(scenario),
    _year(year)
{
    update();
}

void Scn_GlobalCost_Model::update()
{
    auto data = _scenario.globalCostByBuilding(_year);

    int count = 0;
    for (auto& i : data)
    {
        setHorizontalHeaderItem(count, new QStandardItem(i.first->name().c_str()));
        setItem(0, count, new QStandardItem(QString("%L1 €/MWh").arg(i.second.get(), 0, 'f', 2)));
        item(0, count)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        ++count;
    }

    setData(index(0, 0), false, Qt::UserRole + TABLE_HAS_TOTAL_LINE);
}

}  // namespace Reports
