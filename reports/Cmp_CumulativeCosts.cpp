#include "Cmp_CumulativeCosts.h"

#include <QValueAxis>
#include <QLineSeries>
#include <QSettings>

#include "Scenario.h"


namespace Reports{

Cmp_CumulativeCosts_Chart::Cmp_CumulativeCosts_Chart(std::vector<Scenario*> scenarios)
  : _scenarios(scenarios)
{
    setTitle("Cumul des dépenses actualisées");
    setTitleFont(QFont(titleFont().defaultFamily(), 14));

    xAxis = new QValueAxis();
    xAxis->setTitleText("Années d'exploitation");
    xAxis->setLabelFormat("%.0f");
    addAxis(xAxis, Qt::AlignBottom);

    yAxis = new QValueAxis();
    yAxis->setTitleText("Dépenses totales cumulées");
    yAxis->setLabelFormat("%.0f &euro;");
    addAxis(yAxis, Qt::AlignLeft);

    redraw();
}

void Cmp_CumulativeCosts_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    for (auto* sc : _scenarios)
    {
        auto data = sc->cumulativeCosts();

        auto* series = new QLineSeries();

        series->setName(sc->name().c_str());

        for (unsigned u = 0 ; u < data.size() ; ++u)
            series->append(u, data[u].get());

        addSeries(series);

        series->attachAxis(xAxis);
        series->attachAxis(yAxis);
    }

    yAxis->setMin(0);
}

Cmp_CumulativeCosts_Chart* Cmp_CumulativeCosts_Chart::duplicate()
{
    auto* dup =  new Cmp_CumulativeCosts_Chart(_scenarios);

    QSettings settings;
    settings.beginGroup("Cmp_CumulativeCosts_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Cmp_CumulativeCosts_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Cmp_CumulativeCosts_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

}  // namespace Reports
