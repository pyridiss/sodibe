#ifndef CMP_INVESTMENTS_H
#define CMP_INVESTMENTS_H

#include "Report.h"

class Scenario;

namespace Reports {

class Cmp_Investments_Chart : public Report_Chart
{
public:
    Cmp_Investments_Chart(std::vector<Scenario*> scenarios);

    void redraw() override;
    Cmp_Investments_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const std::vector<Scenario*> _scenarios;
    QValueAxis*                  yAxis;
};

}  // namespace Reports

#endif // CMP_INVESTMENTS_H
