#ifndef INITIAL_ENERGIES_H
#define INITIAL_ENERGIES_H

#include "Report.h"

namespace Reports {

class Initial_Energies_Model : public Report_Model
{
public:
    explicit Initial_Energies_Model();

    void update() override;
};

}  // namespace Reports

#endif // INITIAL_ENERGIES_H
