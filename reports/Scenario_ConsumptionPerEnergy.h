#ifndef SCENARIO_CONSUMPTIONPERENERGY_H
#define SCENARIO_CONSUMPTIONPERENERGY_H

#include "Report.h"


class Scenario;

namespace Reports {

class Scenario_ConsumptionPerEnergy_Model : public Report_Model
{
public:
    Scenario_ConsumptionPerEnergy_Model(const Scenario& scenario);

    void update() override;

private:
    const Scenario& _scenario;
};

}  // namespace Reports

#endif // SCENARIO_CONSUMPTIONPERENERGY_H
