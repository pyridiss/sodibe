#include "Cmp_EnvironmentalImpacts.h"

#include <QBarCategoryAxis>
#include <QBarSet>
#include <QHorizontalBarSeries>
#include <QSettings>
#include <QValueAxis>

#include "Scenario.h"
#include "Shared.h"

namespace Reports {

Cmp_EnvironmentalImpacts_Chart::Cmp_EnvironmentalImpacts_Chart(std::vector<Scenario*> scenarios)
  : _scenarios(scenarios)
{
    setTitle("Impacts environnementaux<br />&nbsp;");
    setTitleFont(QFont(titleFont().defaultFamily(), 14));

    QFont symbola(Shared::symbolaFont, 12);
    legend()->setVisible(true);
    legend()->setFont(symbola);
    legend()->setAlignment(Qt::AlignBottom);

    auto newSet = [&](const QString& name)
    {
        auto* axisX = new QValueAxis();
        axisX->setTitleText(name);
        axisX->setVisible(false);
        addAxis(axisX, Qt::AlignBottom);
    };

    newSet("Émissions de CO₂ (tonnes / an)");
    newSet("Équivalent kilomètres parcourus (km / an)");
    newSet("Part d'énergie non renouvelable (%)");
    newSet("Production de déchets nucléaires ultimes (kg / an)");


    yAxis = new QBarCategoryAxis();
    yAxis->setTitleText("Scénarios");
    yAxis->setTitleVisible(false);
    QPen pen(Qt::DashDotDotLine);
    pen.setWidth(3);
    yAxis->setGridLinePen(pen);
    auto font = yAxis->labelsFont();
    font.setBold(true);
    font.setPointSize(11);
    yAxis->setLabelsFont(font);
    addAxis(yAxis, Qt::AlignLeft);

    redraw();
}

void Cmp_EnvironmentalImpacts_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    enum Impacts
    {
        CO2,
        TravellingEquivalence,
        NonRenewables,
        NuclearWaste
    };

    std::array<std::vector<double>, 4> data;

    for (auto sc : _scenarios)
    {
        data[CO2]                  .push_back(sc->co2Emissions().get());
        data[TravellingEquivalence].push_back(sc->carTravellingEquivalence().get());
        data[NonRenewables]        .push_back(sc->nonRenewablesFraction() * 100);
        data[NuclearWaste]         .push_back(sc->nuclearWasteProduced().get());
    }

    QStringList categories;
    for (auto sc : _scenarios)
        categories.append(sc->name().c_str());

    yAxis->clear();
    yAxis->append(categories);

    auto xAxis = axes(Qt::Horizontal);
    int index = 0;

    Shared::startColorEnum();

    auto newSet = [&](const QString& name, const QString& format, std::vector<double> data)
    {
        double max {0};

        auto* set = new QBarSet(name);
        QHorizontalBarSeries *series = new QHorizontalBarSeries();
        series->setName(name);
        series->setLabelsVisible(true);
        series->setLabelsPosition(QAbstractBarSeries::LabelsInsideEnd);
        series->setLabelsFormat(format);

        for (auto d : data)
        {
            set->append(d);
            if (d > max) max = d;
        }

        QFont font(Shared::symbolaFont, 15);
        set->setLabelFont(font);
        set->setColor(Shared::nextColor().color);

        series->append(set);
        series->setBarWidth(0.7);
        addSeries(series);
        series->attachAxis(yAxis);

        series->attachAxis(xAxis.at(index));
        xAxis.at(index)->setMax(max);
        dynamic_cast<QValueAxis*>(xAxis.at(index))->applyNiceNumbers();
        ++index;
    };


    newSet(Shared::symbol_CLOUD            + QString(" Émissions de CO₂"),
           Shared::symbol_CLOUD            + QString(" @value t / an"),
           data[CO2]);
    newSet(Shared::symbol_AUTOMOBILE       + QString(" Équivalent kilomètres parcourus"),
           Shared::symbol_AUTOMOBILE       + QString(" @value km / an"),
           data[TravellingEquivalence]);
    newSet(Shared::symbol_OIL_DRUM         + QString(" Part d'énergie non renouvelable"),
           Shared::symbol_OIL_DRUM         + QString(" @value %"),
           data[NonRenewables]);
    newSet(Shared::symbol_RADIOACTIVE_SIGN + QString(" Production de déchets nucléaires ultimes"),
           Shared::symbol_RADIOACTIVE_SIGN + QString(" @value kg / an"),
           data[NuclearWaste]);
}

Cmp_EnvironmentalImpacts_Chart* Cmp_EnvironmentalImpacts_Chart::duplicate()
{
    auto* dup = new Cmp_EnvironmentalImpacts_Chart(_scenarios);

    QSettings settings;
    settings.beginGroup("Cmp_EnvironmentalImpacts_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Cmp_EnvironmentalImpacts_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Cmp_EnvironmentalImpacts_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

Cmp_EnvironmentalImpacts_Model::Cmp_EnvironmentalImpacts_Model(std::vector<Scenario*> scenarios)
  : _scenarios(scenarios)
{
    setHorizontalHeaderLabels({"Scénario", "Émissions de CO₂", "Équivalent kilomètres parcourus",
                               "Part d'énergie non renouvelable", "Production de déchets nucléaires ultimes"});

    update();
}

void Cmp_EnvironmentalImpacts_Model::update()
{
    removeRows(0, rowCount());

    for (auto* sc: _scenarios)
    {
        QList<QStandardItem*> line;
        line.push_back(new QStandardItem(sc->name().c_str()));
        line.push_back(new QStandardItem(QString("%L1 t / an") .arg(sc->co2Emissions().get(),             0, 'f', 1)));
        line.push_back(new QStandardItem(QString("%L1 km / an").arg(sc->carTravellingEquivalence().get(), 0, 'f', 0)));
        line.push_back(new QStandardItem(QString("%L1 %")      .arg(sc->nonRenewablesFraction() * 100,    0, 'f', 1)));
        line.push_back(new QStandardItem(QString("%L1 kg / an").arg(sc->nuclearWasteProduced().get(),     0, 'f', 1)));
        line[1]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        line[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        line[3]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        line[4]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

        appendRow(line);
    }
}

}  // namespace Reports
