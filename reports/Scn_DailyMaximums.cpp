#include "Scn_DailyMaximums.h"

#include <QBarCategoryAxis>
#include <QBarSet>
#include <QSettings>
#include <QStackedBarSeries>
#include <QValueAxis>

#include "Scenario.h"

namespace Reports {

Scn_DailyMaximums_Chart::Scn_DailyMaximums_Chart(const Scenario& scenario, unsigned year)
  : _scenario(scenario),
    _year(year)
{
    QString title = QString(scenario.name().c_str()) + " : minima et maxima appelés par jour sur le réseau";

    setTitle(title);
    setTitleFont(QFont(titleFont().defaultFamily(), 14));

    QStringList categories;

    for (int i = 0 ; i < 365 ; ++i)
        categories << QString::number(i);

    xAxis = new QBarCategoryAxis;
    xAxis->append(categories);
    xAxis->setGridLineVisible(false);
    xAxis->setLabelsVisible(false);
    xAxis->setTitleText("Jour de l'année");
    xAxis->setTitleVisible(false);
    addAxis(xAxis, Qt::AlignBottom);

    yAxis = new QValueAxis;
    yAxis->setTickCount(10+1);
    yAxis->setLabelFormat("%d kW");
    yAxis->setTitleText("Puissance");
    addAxis(yAxis, Qt::AlignLeft);

    redraw();
}

void Scn_DailyMaximums_Chart::redraw()
{
    for (auto s : series())
        removeSeries(s);

    auto data = _scenario.dailyMaximums();

    Shared::startColorEnum();

    auto* min = new QBarSet("Minimum");
    min->setColor(Shared::nextColor().color);
    auto* moy = new QBarSet("Moyenne");
    moy->setColor(Shared::nextColor().color);
    auto* max = new QBarSet("Maximum");
    max->setColor(Shared::nextColor().color);

    min->setPen(Qt::NoPen);
    moy->setPen(Qt::NoPen);
    max->setPen(Qt::NoPen);

    for (unsigned x = 365 * _year ; x < 365 * _year + 365 ; ++x)
    {
        *min << data[x][0];
        *moy << data[x][1] - data[x][0];
        *max << data[x][2] - data[x][1];
    }

    double maxPower {0};
    for (auto & x : data)
    {
        maxPower = std::max(maxPower, x[2]);
    }

    if (maxPower > 100) maxPower = std::ceil(maxPower/100)*100;

    auto* series = new QStackedBarSeries();
    series->setName("Puissance");

    series->append(min);
    series->append(moy);
    series->append(max);

    series->setBarWidth(1);

    addSeries(series);

    yAxis->setRange(0, maxPower);

    series->attachAxis(xAxis);
    series->attachAxis(yAxis);
}

Scn_DailyMaximums_Chart* Scn_DailyMaximums_Chart::duplicate()
{
    auto* dup = new Scn_DailyMaximums_Chart(_scenario, _year);

    QSettings settings;
    settings.beginGroup("Scn_DailyMaximums_Chart");

    loadCommonSettings(settings, dup);

    settings.endGroup();

    return dup;
}

void Scn_DailyMaximums_Chart::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Scn_DailyMaximums_Chart");

    saveCommonSettings(settings);

    settings.endGroup();
}

}  // namespace Reports
