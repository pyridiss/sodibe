#include "Scenario_LocalInvestments.h"

#include "Building.h"
#include "Scenario.h"

#include "htmlCreator.h"

namespace Reports {

Scenario_LocalInvestments_Model::Scenario_LocalInvestments_Model(const Scenario& scenario)
  : _scenario(scenario)
{
    setHorizontalHeaderLabels({"Bâtiment", "Travaux", "Montant à charge"});

    update();
}

void Scenario_LocalInvestments_Model::update()
{
    removeRows(0, rowCount());

    auto data = _scenario.localInvestments();

    struct BuildingLine
    {
        std::string name;
        Euro investment;
    };

    std::unordered_map<std::string, std::vector<BuildingLine>> lines;

    for (auto d : data)
    {
        lines[d.first->owner()].push_back({d.first->name(), d.second});
    }

    Euro wholeInvestment;

    for (auto l : lines)
    {
        Euro total;
        for (auto b : l.second)
            total += b.investment;

        wholeInvestment += total;

        QList<QStandardItem*> owner;
        owner.push_back(new QStandardItem(l.first.c_str()));
        owner.back()->setData(2, Qt::UserRole + TABLE_COLSPAN);
        owner.push_back(new QStandardItem("-"));
        owner.push_back(new QStandardItem(QString("%L1 €").arg(total.get(), 0, 'f', 0)));
        owner.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

        for (auto i : owner)
            i->setFont(QFont(i->font().family(), i->font().pointSize(), QFont::Bold, true));

        appendRow(owner);

        for (auto b : l.second)
        {
            QList<QStandardItem*> line;
            line.push_back(new QStandardItem(QString("  > ") + b.name.c_str()));
            line.push_back(new QStandardItem("??"));
            line.push_back(new QStandardItem(QString("%L1 €").arg(b.investment.get(), 0, 'f', 0)));
            line[2]->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            appendRow(line);
        }
    }

    QList<QStandardItem*> total;
    total.push_back(new QStandardItem("Totaux :"));
    total.back()->setData(2, Qt::UserRole + TABLE_COLSPAN);
    total.push_back(new QStandardItem("-"));
    total.push_back(new QStandardItem(QString("%L1 €").arg(wholeInvestment.get(), 0, 'f', 0)));
    total.back()->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

    for (auto i : total)
        i->setFont(QFont(i->font().family(), i->font().pointSize(), QFont::Bold));

    appendRow(total);
}

}  // namespace Reports
