#ifndef CMP_CUMULATIVECOSTS_H
#define CMP_CUMULATIVECOSTS_H

#include "Report.h"

class Scenario;

namespace Reports {

class Cmp_CumulativeCosts_Chart : public Report_Chart
{
public:
    Cmp_CumulativeCosts_Chart(std::vector<Scenario*> scenarios);

    void redraw() override;
    Cmp_CumulativeCosts_Chart* duplicate() override;
    virtual void saveSettings() override;

private:
    const std::vector<Scenario*> _scenarios;
    QValueAxis* xAxis;
    QValueAxis* yAxis;
};

}  // namespace Reports

#endif // CMP_CUMULATIVECOSTS_H
