#include "MainWindow.h"
#include <QApplication>

#include "Shared.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("ALE 08");
    a.setOrganizationDomain("ale08.org");
    a.setApplicationName("sodibe");

    Shared::load();

    QLocale locale (QLocale::French, QLocale::France);
    QLocale::setDefault(locale);

    MainWindow w;
    w.show();

    QObject::connect(&a, SIGNAL(aboutToQuit()), &w, SLOT(cleanup()));

    return a.exec();
}
